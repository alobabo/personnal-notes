# Rust language (Book)
- Basic types
  + literals
    * default for integers are _i32_, and floats are _f64_
  + __Arrays__
    * their signature are _[T; size]_ with T the type of the elements
    * size is constant and known at compile time
  + __Slices__, two words objects
    * one pointer to the data
    * the 2nd is the length of the slice
    * dynamic size
    * signature: _&[T]_
    * can be used to borrow a section of an array
  + __String__, __str__ and __char__
    * encoding for _str_ is UTF-8
    * encoding for a char is a scalar Unicode
    * therefore foreign characters usually take more than
    * _String_ can be owned whereas you generally only get reference _&str_
    to string literals _str_
    one byte in strings
    * __String__
      - wrapper around _Vec<u8>_, stored on the heap
      - _String_ is composed of 3 words
        1. pointer to first byte
        2. size of the string in bytes
        3. capacity of the string
      - classic functions: _push(char)_, _push_str(&str)_, _String::from(&str)_,
        _replace(&str, &str)_
    * __&str__
      - sequence of UTF-8 bytes
      - _&str_ is a slice
        1. pointer to first byte
        2. size (in byte) of the the slice
      - the size is unknown at compile time (due to unknown character size)
      and depedns on runtime information
      - therefore one can only handle it behind a pointer -> _&str_
      - _"toto"_ is of type _&'static str_
        + the string literal is hard-coded in the executable
      - escape with _\_
        > "\u{211D}"   // unicode code points
        > "\x52\x32"   // write bytes by their hexadecimal values
      - raw string literals wit _r"..."_
        > r"Escapes don't work here: \x3F \u{211D}"
        > r#"Quotes work here ""!"# 
        > r###"Quotes and # work here ""!"####
  + __Box<T>__
    * smart pointers
    * allow to wrap/box objects and store them in the heap
    * deallocation of the pointer and the heap allocated value is
      handled automatically when goind out of scope
    > Box::new("toto");
    * boxed values can be dereferenced with _*_ or _deref()_
  + __Vectors__
    * resizable arrays
    * represented by 3 fields: pointer to data, length, capacity
    * when capacity is surpased the vector is reallocated with larger capacity
    > let toto: Vec<i32> = (0..10).collect();
    > let toto = vec![1i32, 2, 3];
    * classic methods: _push()_, _pop()_, _len()_, _iter()_
		* ownership in vectors
			- we cannot move value out of vectors with syntax
				> let a = v[0];
			- the only way to move value out of a vector is to use _iter_mut()_
				since it guarantees that each element is only moved once
  + __HashMap<K, V>__
    * while vectors stores values by integer index, hashmaps store values 
      by keys
    * keys can be any type that implements the _Eq_ and _Hash_ traits
    * classic functions: _new()_, _insert(K, V)_, _get(&K)_
    * _tip_: you can have _K = String_ and query with _get_ and _&str_
  + implementation of __Set__
    * __HashSet__ which is a wrapper to _Hashmap<K, ()>_
    *  there is also __BTreeSet__
    * classic methods: _union(set, set)_, _difference(set, set)_, 
      _intersection(set, set)_, _symmetric_difference(set, set)_
  + __Rc__ (Reference Counting)
    * when multiple ownership is needed (graph, multiple edges might point
      to the same node)
    * keeps track of the number of references/owners of the value wrapped inside _Rc_
    * cloning an _Rc_ doesn't do a deep copy but creates another pointer to the
      wrapped value and increments the counter
    * methods of wrapped value can be used directly
    * clasic methods: _Rc::new(T)_, _Rc::strong_count(&Rc)_, _Rc::weak_count(&Rc)_
    * equality tests their inner value
		* _Rc_ is dropped when counter reaches 0 and 
		  can leak if counter never reaches 0
		* to avoid this situation we can also have __weak references__ _Weak<T>_
			- does not count in the _strong_count_ so weak ref can be live when 
				object has been dropped
			- weak references have no notion of ownership
			- to create a weak reference you use _Rc::downgrade(&T)_
			- to use a weak reference you use _Rc::upgrade_ that checks that
				object is still live
	+ __Arc__ (Atomic Reference Counting)
		* same as _Rc_ but counter increment decrement are atomic
	+ __RefCell<T>__
		* you can have both mutable and immutable ref to an object at compile time
		* the rule is still checked at compile time
		* you can mutate the value inside the _RefCell_ even when it's immutable
		* it enables __interior mutability__, when a variable/field is immutable
			for the users but mutable in the inner workings
		* functions: _borrow(self)_, _borrow_mut(self)_
		* by combining _Rc_ and _RefCell_ you can have multiple owners
			of mutable data
	+ _Rc vs Box vs Refcell_
		* _Rc_ enables multiples owners
		* _Box_ allow immutable or mutable at compile time
		* _Rc_ only enables immutable borrows
		* _RefCell_ allows both mutable and immutable at compile time

	
  + __thread__
    * native OS threads 
    * use the _spawn(closure)_ to spawn a thread with a moving closure
      > thread::spawn(move |x| { ... })
      > thread.join() -> Result
    * __channels__
      - unidirectionnal flow of information between a _Sender_ and a _Receiver_
      > mpsc::channel(): (Sender<I>, Receiver<I>)
      > sender.send(msg) -> Resuilt
      > receiver.recv() -> Result
  + __file__
    * __path__
      - either _posix::Path_ or _windows::Path_
      > Path::new(".")
      > path.join("toto").join("hah");
      > path.display() -> impl Display
    * __File__
      - open in read-only
      > File::open(&path) -> Result<File>
      - open in write-only
        > File::create(&path) -> Result<File>
        + _file.write(&str.as_bytes()) -> Result<usize>_
          return number off bytes written
        + _file.write_all(&str.as_bytes()) -> Result<()>_
          loop write until all the string have been sent

      - read a file
        > fs::read_to_string(&str) -> Result<String>
        > fs::read(&str) -> Result<Vec<u8>>
        > file.read_to_string(&mut string) -> Result<()>
        > read(&pat) -> Result<Vec8>
        +  line by line
          > read_lines(path: &str) -> Iterator<Option<String>>>
      - write a file
        > fs::write(path:&str, data: &str) -> Result<()>
      - __standard I/O__
        + _Stdio::piped()_ new pipe between child and parent process
          > Command::new(&cmd).stdout(Stdio::piped()).spawn()
        + _Stdio::inherit()_ connected to parent stdio
        + _Stdio::null()_ ignored
      - create a file with options
        + _OpenOptions::new().create(true).write(true).open(path);_
          create a file _path_ which is writable
  + __process__
      - create a command: 
        > Command::new(cmd:&str).arg(arg:&str) -> Command
        - no argument by default
        - inherit current proc env
        - inherit current proc working dir
      - start a child process 
        * _output()_, stdout and stderr are captured
        > command.output() -> Result<Output>
          + wait for it to finish
          + get status:
            > output.status.sucess() -> bool
          + read from stdout/stderr
            > String::from_utf8_lossy(&output.stdout)
        * _status()_ , wait for the child to finish and collect exit status
            > command.status() -> Result<ExitStatus>
          + stdin/stdout/stderr are inherited form parent
        * _spawn(&mut command) -> Result<Child>_
          + return a handle
          + stdin/stdout/stderr are inherited form parent
          + does not wait for the child to finish
      - wait a child
        _let mut child = Command::new("sleep").arg("4").spawn().unwrap();
        let _result = child.wait().unwrap();_
	+ __Option<T>__
		- enum with either _Some(T)_ or _None_
		- _take()_ allows to take ownership of a value in _Some_ leaving
			_None_ in the variable

- compound types/ algebraic types
  - struct (product type)
    + can be used in 3 different way
    1. Naming a kind of tuples (like Pair for two elements tuples)
    2. C structs or records 
    3. Unit structure with no fields (Zero Size Structure)
    + structs with named fields (records)
      * declaration
      > struct name { field1: type1, ... }
      * init
      > name { field1: arg1, ... }
    + structs with anonymous fields (tuples)
      * declaration
      > struct name (type1, ...);
      * init
      > name(arg1,...)
    + internally
      * each field of struct has the size of the biggest field
      * padding is used to fill the smaller fields
      * order of the fields are not guaranteed, may be optimized
		+ ownership
			* when moving out a value from a struct field we do what is
				called a _partial_move_, which makes the owner variable
				loses ownership of the whole struct
  - enum
    1. used to create a struct with multiple variants
    2. or can be used as C enum, where variants are just names to hold value
    + an instance of enum is represented by a struct with two fields
      * _tag_, an integer corresponding to the variant
      * _data_, data associated to the instance variant
        - _data_ is an union type which size is the biggest possible
          size of data for this enum
- constants/statics variables
  + _const_ for constants and represents unmutable values 
    * are inlined by the compiled
    * references to a constant may not point to the same address
  + _static_ usually for global variables and represent a memory address.
      * by default, static variables lifetime are equal to the program lifetime
      * stored in the read_only memory of the binary
      * you can bind the lifetime of a static to another variable lifetime
- variable bindings
  + we use variable bindings with the _let_ keyword
  + variable bindings can be mutable with _let mut_
  + variable shadowing is allowed
- references
  + 2 ways of getting a reference
  + specify in the let binding
    > let ref toto = tata;
  + borrow on the right side
    > let toto = &tata;
- types
  + casting
    * no implicit conversion but allowed explicit conversion with _as_
    * when casting into a smaller lietrals, the Least significant Bits are kept
    and the rest is truncated
    * signed integer used two complements representation
      - MSB is for sign
      - number is inverted 
      > 1 = 00000001 and -1 = 11111111
  + __aliasing__ of types can be done with _type_ keyword
  + type conversion
    * Any type can implement the trait _From_ and _Into_
      - convert a _str_ into _String_
      > String::from(my_str)
      - implement _From_
      > impl From<SrcType> for TargetType {
      >   fn from(e: SrcType) -> TargetType
    * _Into_ is the reciprocal of _From_
      + you get _Into_ for free if you implement _From_
      > let n: Number = Number::from(int)
      > let n2: Number = int.into()
    * also exists _TryFrom_ and _TryInto_ for fallible conversions
      + they must return _Result_ type
    * implementing the _fmt::Display_ trait gives _ToString_ for free
    * parsing _str_ into numbers
      - use the _parse()_ function
      - to specify the target type use either
        + type inference
        > let parsed: i32 = "5".parse().unwrap();
        + turbofish notation
        > let parsed = "5".parse::<i32>().unwrap();
        + _unwrap()_ is used for _Option_ type to unwrap 
        the value of type _T_ if _Some(T)_ 
- control-flow
  + classic if / else
    > if cond {} else {}
    * used as expressions
  + loop
    > loop { break; }
    * used as expressions
    * _break_ and _continue_ can be applied to outer loops with labels
    > 'outer: loop {
    >   'inner: loop {
    >       break 'outer;
    * _break_ can return a value
  + while 
    > while cond {}
  + for 
    > for var in iterator {}
    * three methods to turn collections into iterators
      - note that ownership does not apply to array which are unmutable 
      1. _iter()_, borrows each element of the collection
      2. _iter_mut()_, borrows element of the collection and 
      collection can be modified
      3. _into_iter()_, consumes the collection and ownership 
      has been moved in the loop
  + match can be used for pattern matching
    > match expr {}
    + integers and _str_ can matched
    + match can destructure items
      * tuples
      * enums
      * pointers, with _ref_ or _&_ 
      * structures
        > Foo { x: 2, y: i } =>
        > Foo { x, .. } => 
    + _guard_ can be added to filter the arms
      > (x, y) if x == y => println!("toto")
    + _if let_, allows to do non exhaustive pattern-matching
      * arms of the pattern-matching has to be of type unit
      > if let Some(i) = number { println!("{}", i) } 
      * useful for equality when you can't use _==_
    + _while let_ allows to have have a loop as long as the pattern matches
      > while let Some(i) = variable { ... }
- functions
  + function declaration
    > fn toto(arg: type) -> return_type { ... }
  + method
    + functions which are attached to objects
    + you declare methods like functions but within an implementation block
    > impl Object {
    >   fn toto ...
    > }
  + closures/lambda expr/lambda
    * functions with enclosing environment
    * closure declaration use _||_ around input variables 
      - example where input is _var_ and _x_ is part of the environment
      > |var: i32| -> i32 { var + x }
    * input and output types can be inferred
      > |var| var + x
    * by default closure prefers capturing env variable by:
      1. immutable borrow
      2. unique immutable borrow (exclusive to closure)
      3. mutable borrow
      4. by move 
    * the closure will stop the capture after its last call
    * it s possible to force the capture by move with _move_
      > move |var| var + 1;
      - it may be useful when we want the closure to outlive the 
      environment variables
    * closures have unnamed concrete type, therefore we specify using traits
      - my guess is that since closures include environment variables
        it is difficult to specify an exact type for a closure
      - closures are similar to struct but env variables
    * closure complete traits must be specified when used as function param
      - possible traits are 
        + _Fn_ may capture by reference
        + _FnMut_ may capture by mutable ref
        + _FnOnce_ may capture by balue
      - these traits all implement the _call_ operation with different arg types
      > fn fn_name<F> (arg: F) -> return_type 
      >   where F: Fn(arg_type) -> closure_rettype { ... }
      - _<F>_ is for generic type
        + when a closure is defined an anonymous structure is implicitly
        created to store the captured variables
        + it also implements teh necessary functionality of one of 
        the traits: _Fn_, _FnMut_ and _FnOnce_
      - unbound generic type _<F>_ is still ambiguous therefore the
      _where_ clause is required, bounding it to one of the closure trait
    * it is possible to provide a function as input if arg type is a closure
    * closures as output 
      - types of closure are unknwown so we have to use _impl Trait_
      - must be of traits _Fn_, _FnMut_ or _FnOnce_
      - you also need to add the _impl_ keyword to specify that it
      is an implementation of the trait
      > fn toto() -> impl Fn { ... }
      - _move_ must be used to capture variables by value otherwise the
      captured references would be dropped at the end of the function
  + higher-order functions
    * there exists a type _fn_ which contrary to closure is a type and not a trait
    * it implements the 3 closure traits
    * you may require the _fn_ type rather than a closure trait when interfacing
      with a language with function pointers but no closures (like C)
  + diverging functions 
    * functions that never return
    * return type is _!_
    > fn foo() -> ! { ... }
    * type _!_ cannot be instantiated
      - certainly an enum with empty variants
      - note that _()_ is different and has a single possible value
    * _!_ is useful because it can casted into any type
      - for example a pattern matching that may diverge can be cast as _i32_
      even for diverging arms
- Visibility and modules
  > mod mod_name { ... }
  + by default modules content is private
  + can be overrident with _pub_
  > pub mod mod_name { ... }
  > pub fn func(...) {...}
  + multiple variants for _pub_
    * solely public for a certain path
    > pub(in crate::mod_name)
    * solely public for the parent
    > pub(super) 
    * solely for the current crate
    > pub(crate)
  + Struct defined in modules also have to specify public fields 
  + overall modules paths are similar to a file system hierarchy
  + to bind a full path you can _use_
  > use crate::my_mod::nested 
  > use crate::my_mod::nested as toto
  + _self_ represents the current module scope
  > self::toto()
  + _super_ represents the parent module scope
  > super::toto()
  + any _.rs_ file is a module
    * to import another file _file.rs_ content use _mod_
    > mod file;
- crate
  + represent a compilation unit in Rust
  + necessary files to compile a binary or library
  + _mod_ keyword inlines the content of modules into the file
    * static linking
    * a single file gets compiled
  + _rustc_ is the rust compiler
    * to produce a binary _main_
    > rustc main.rs
- library
  + to compile the lib _libtoto.rlib_
  > rustc --crate-type=lib toto.rs
  + to import an extern library in rust code
  > extern crate toto;
  + to compile with library
  > rustc main.rs --extern toto=libtoto.rlib
- cargo
  + official Rust package management tool
  + the cargo book: https://doc.rust-lang.org/cargo/
  + dependency management and integration with crates.io (official Rust crates)
  + Unit testing
  + Benchmarks
  + new project
  > cargo new foo
  + new lib
  > cargo new --lib foo
  + _cargo new_ also sets up a git init command
  + hierarchy
    > foo
    > |-> Cargo.toml
    > |-> src
    >     |-> main.rs
    >     |-> bin
    * _Cargo.toml_ is config file for the project
      - _[package]_ details about the project
      - _[dependencies]_ required crates to compile the project
        + can be from local/online/crates.io repo
    * _Cargo.lock_
      - image of the build dependency after building
      - give the compiler more details such as git commit version ...
    * _main.rs_ is the root of the project
    * _srs/bin_ directory is necessary if you want to produce multiple binaries
  + __testing__
    * we can add a directory _tests_ at the root of the project containing
    rust files containing tests
    * to __run tests__
    > cargo test
      - cargo run tests concurrently which can cause race condition
      - to disable concurrency
      > cargo test -- --test-threads=1
      - to show output of all the tests (failed or not)
      > cargo test -- --show-output
      - run only certain test
        + use full name
        > cargo test testa
        + use part of multiple tests name
          > cargo test add
          * will launch all test which names starts with _add_
    * writing tests
      - add the _test_ attribute to a function on the line before _fn_
      > #[test]
      - when running tests with _cargo test_, Rust builds a test runner binary
      that execute the annotated test functions and reports the results of the tests
      - __tests macros__
        + _panic!("msg")_ fails with message
        + _assert!(var)_ checks that _var_ is true
          * calls _panic!_ if _var_ is false
        + assert equality
          * _assert_eq!(v1, v2)_ checks that _v1_ and _v2_ are equal
          * _assert_ne!(v1, v2)_ checks that _v1_ and _v2_ are not equal
          * for equality and non-equality _==_ and _!=_ are used
          * this may requires to implement or derive the _PartielEq_ trait
        + custom failure messages
          > assert!(is_this_var_true, 
          >         "format message with {}", 
          >         placeholder_value);
      - checking for panic
        + may be useful when we expect handle errors as we expect it
        + use the attribute _should_panic_
          > #[should_panic]
      - ignore some tests with the _ignore_ attribute under the _test_ one
        > #[test]
        > #[ignore]
        > fn ...
      - write tests that use _Result<T, E>_
        + instead of panicking when failing the test we can
        also return a result type
        + Rust panic is the equivalent of exceptions but it cannot contains
        structured information
        + Rust usually handles errors with _Result_
          * it forces you to deal with error
          * you can immediately see in the code where can failure happen
      - test organization
        + __unit tests__
          * why make unit tests
            - test units of code independently 
            - allow to pinpoint errors
          * where
            - in the _src_ directory
            - in the same file as the tested code
            - in a _tests_ module
              + add the attribute _cfg(test)_ before the module declaration
              to be only compiled when using _cargo test_
              > #[cfg(test)]
              > mod tests { ... }
              + only required for the tests written in the _src_ directory
              + _cfg_ stands for configuration, and means that it should
              only be included when the _test_ option is specified during
              compilation
        + __integration tests__
          * entirely external to your library
          * test through the library API
          * where
            - in the _tests_ directory
            - in _*.rs_ files
          * integration tests are meant for libraries
            - binaries are meant to be run on their own and its function 
            cannot be imported in the _tests_ directory
            - Rust projects usually have a straightforward _src/main.rs_ file
            that calls logic that in the _src/lib.rs_ file
  + __build__
    * two main profiles _release_ and _dev_
      > cargo build   //dev by default
      > cargo build --release 
    * customize profile in _Cargo.toml_
      + default 
      > [profile.dev]
      > opt-level = 0
      >
      > [profile.release]
      > opt-level = 3
  + __doc__
    * to write doc use _///_ just before the item you want to comment
      > /// Eat cookies
      > /// # Examples
      > /// let five = 5;
      > pub fn toto() -> () { 
      >   ...
      > }
    * you can use markdown syntax
    * run _cargo doc_ to generate the doc in _target/doc/module_name/_
    * or _cargo doc --open_ to generate and open it
    * _#_ to create html sections
      - Examples
      - Panics, for function that can panic
      - Errors, possible errors
      - Safety, if the function use unsafe blocks
    * _`toto`_ for rust items
    * _cargo test_ run the code examples in your documentation as tests!
    * _//!_ to add documentation to the module containing the doc
      + usually for general doc about the crate (in _src/lib.rs_)

- Attributes
  + metadata that can be applied to modules, crates or items
  + syntax is _#[attr]_ or _#![attr]_
  + presence of bang _!_ indicates if is applicable to the whole crate
  + examples:
    * _#[cfg(...)]_, marks conditional compilation of code
        - _#[cfg(target_os = "linux")]_ only compiles if target is a linux OS
        - custom conditions for _cfg_ can be written but it must
        be specified to the compiler _rustc_
    * set crates parameters
      - not relevant when using _cargo_
      - _#![crate_type = "lib"]_
      - _#![crate_name = "toto"]_
    * disabling lints or warnings
      - _#![allow(unused_variables)]_
      - _#![allow(dead_code)]_
    * enable compiler features
    * _#[test]_, mark functions as unit tests
- __Generics__
  + between angle brackets _<...>_, before first usage
  + use camel case _<Ttt>_
  + usually _<T>_ or _<T, U, X, ...>_ for multiple generic type
  + functions
    * declaration
    > fn toto<T>(arg1: T) { ... }
    * usage with and without inference
    > toto(3)
    > toto::<i32>(3)
  + implementation of generic type can be specific or still generic
    * _<T>_ must precede the type
    > impl<T> Type<T>
  + traits can also be generic
    * to refer to the type implementing the trait you can use _Self_
  + __bounds__ can placed on generic types
    * type _T_ can be generic but must implement certain traits
    > fn printer<T: Display> (s: T) { ... }
    * with _where_
    > fn printer<T> (s: T) where T: Display { ... }
    * multiple bounds can be specified with _+_
    > fn toto<T: Display + PartialEq>(t: T) { ... }
    > fn toto<T>(t: T) where T: Display + PartialEq { ... }
  + _where_ clause can be more expressive
      > impl<T> PrintOption for T where Option<T>: Debug { ... }
    * here the bounds are not directly applied on _T_
  + __associated types__
    * a trait with a generic type _A_ may have its _A_ defined
        depending on the type implementing the trait
    * associated types are useful when you want to specify the generic
        type only once: when you're implementing the trait
    * in this situation you don't want to re-specify _A_ every time 
        you use the implemented trait
    * you are allowed to create associated types in the traits which
        are specified when implementing it
        > trait Trait { 
        >   type A;
        > }
    * difference with generic is that you can't implement a trait with
      associated types multiple times on the same type
  + __Phantom types__ can be used as a marker to differentiate between 
      types with the same underlying structure
    * for example you may have two structs _Error(&str)_ and
        _Msg(&str)_ that have the same underlying structure
    * therefore you are able to do _Error("to") == Msg("to")_
    * phantom types can be used if you want to differentiate between these types
        > struct Error<A>(&str, PhantomData<A>);
        > struct Msg<A>(&str, PhantomData<A>);
        > let e: Error(&str, i32) = ... ;
        > let m: Msg(&str, i64) = ... ;
        > // e and m cannot be compared
    * This is even more useful if you want to differentiate between two
        instances of the same generic type
    * Example: you want to have a _Length_ datatypes but you want to differentiate
        if the units are in inch or millimeters
  + Generic internals:
    * rust compiler performs __monomorphization__
    * for each usage of a generic function/trait rust will produce a non-generic
      function/trait corresponding to the concrete type that was used
    * the resulting code from monorphization is doing _static dispatch_
    * _dynamic dispatch_ in opposition will produce code that will figure
      out which code to execute at runtime
- __Scoping__
    + Rust enforces RAII (Resource Acquisition Is Initialization)
      * whenever a resource goes out of scope, its destructor is called
          and its resources are freed
      * this avoids resource leak bugs due to mistakes in memory management
    + destructor in Rust is provided through the _Drop_ trait
      * contains the function _drop(&mut self)_
      * implemented by default
      * only implement if you require your own destructor logic (like logging)
      * you can't call drop manually otherwise you'd have a double free
      	with the automatic drop
      * _std::mem::drop_ can be called for manual dropping
    + __variables are responsible of freeing their own resources__
      * hence __resources can only have 1 owner__
      * this also prevents double free
    + when passing an argument by value or during assignment,
        _ownership_ of resources is transfered, and is called _move_
    + a resource is not moved if it implements the _Copy_ trait
      * _Copy_ and _Clone_
        - _Copy_ is implicit, _Clone_ explicit_ with _clone()_ function
        - _Copy_ cannot be reimplemented it's always a simple bit-wise copy
        - both traits are derivable
          > #[derive(Clone, Copy]
        - _Clone_ is a supertrait of _Copy_, every type implementing _Copy_
        also needs to implements _Clone_
      * list of _Copy_ implementors
        - generally data which are stored solely in the stack
        - literals ( _bool_, _char_, number, ...)
        - arrays/tuples/closures when their components implements _Copy_
    + mutability of data can be changed during a _move_
    + instead of _moving_ values you can also pass values by reference _&T_
    and it's called __borrowing__
      * the borrow checker verifies that references always point to
      valid objects
      * _&T_ borrows data via immutable reference which can only reads the data
        - here _a_ is of type _&i32_ and _b_ of type _i32_ 
          > let (ref a, b) = (3, 5)
        - allows to pass value by reference when destructuring
        - possible to have mutable reference
          > let (ref mut a, b) = (3, 5)
    + __lifetimes__
      * abstractions used by the borrow checked to check for correct borrows
      * __lifetime is defined from the place it is created to its last use__
      * lifetimes are named regions of code that a reference must be valid for
      * while scopes and lifetimes are deeply linked, they are different
        - by default an item ends when reaching the end of its scope
        - we can also manually destruct items

      * __explicit lifetime annotations__
        - lifetime annotations describe the relationship of the lifetimes
          of multiple references to each other
        - __lifetime annotations are used to specify the relation between the 
        lifetime of parameters and return values__
        - syntax is similar to generics
          + to declare lifetime
            * _foo_ has a lifetime parameter _'a_
              > foo<'a>
            * multiple lifetime parameters, lifetime of _foo_ should not
              exceed that of either _'a_ or _'b_
                > foo<'a, 'b>
            * reference to _i32_ must outlive instance of _Toto_
              > struct Toto<'a>(&'a i32);
              - instances of _Toto_ must live within the lifetime of
                their _i32_ field
            * instance of _Y_ must live within _'a_ and _'b_, field of _Y_ of
            type _X_ has lifetime _'a_ and must live within _'b_
              > struct Y<'a, 'b>(&'a X<'b>); 
        - explicit notations are instructions for the compiler
          + they are mandatory for the references (it may be omitted due to
              elision)
          + for example 
            * _foo_ requires more notations to specify how long will
            live the return value
              > fn foo(x: &u32, y: &u32) -> &u32;
            * here we specify that the returned reference need to live
            as long as the reference of _x_
              > fn foo<'a, 'b>(x: &'a u32, y: &'b u32) -> &'a u32;
        - applicable to functions/methods/structures/traits
        - like generic types, lifetimes can be __bounded__ too
          + all references in _T_ must outlive lifetime 'a
            > T: 'a
          + Type _T_ must implement _Trait_ and reference must outlive _'a_
            > T: Trait + 'a
          + with _where_
            > fn foo<'a, T>( ... ) where T: Trait + 'a { ... }
        - __coercion__
          + _'a_ is at least as long as _'b_
            > <'a: 'b>
        - __elision__
          + historically in beta versions of Rust you had to specify lifetime
            for every reference
          + nowadays lifetime references are only required in ambiguous situations
          + the borrow-checker was improved with 3 elision rules to recognize 
            some deterministic patterns for lifetime annotations
            1. each parameter of a function that is a reference gets its 
              own lifetime param
              * a function with 2 arguments will have two lifetime params _<'a, 'b>_
            2. if there is a single input lifetime parameter then it is assigned
              to all output lifetime parameters
              > fn foo<'a>(x: &'a i32) -> &'a i32 
            3. if there are multiple input lifetime params but one is either
              _&self_ or _&mut self_
              * it is a right rule in 99% of the cases, but in situation where
                bad programing practice are used this is harmful
      * __static__ lifetime
        - longest possible lifetime
        - equal to the lifetime of the program
        - two ways to create static vars
          1. constant with static declaration
            > static TOTO = 3;
          2. string literal of type _&'static str_
- __Traits__
  + collection of methods defined for an unknown type _Self_
  + traits can provide default implementations of functions
  + __derive__
    * compiler can "derive" basic implementations of certain traits
      - Comparison traits: _Eq_, _PartialEq_, _PartialOrd_
      - _Clone_, create a _T_ from _&T_
      - _Copy_, gives a _copy_ semantics instead of the _move_ one
        + implemented for types where making a bitwise copy creates a valid 
          instance without invalidating the original instance
        + ok for stack allocated values
        + for _String_ this does not work coz it contains a pointer to the heap
          that could create undefined behaviour with wrong deallocation
      - _Hash_, compute a hash from _&T_
      - _Default_, create an empty instance of a data type
      - _Debug_, to format a value using the _{:?}_ formatter
      > #[derive(Debug, Eq)]
  + in the case where a type implements multiple traits with the same method
    name you have to specify which method you want to call
    > <Type as Trait1>::get(&toto);
    > <Type as Trait2>::get(&toto);
    > Trait1::get(&toto);
    > Trait2::get(&toto);
  + in Rust, _type_ and _traits_ are separated because __Rust compiler
  needs to know how much memory each items requires__
    * _type_ describes the construction of an instance therefore
    it also describes its memory size
    * _trait_ describes the functionality of an instance but gives
    no infromation on its internal structure
    * hence you have to use concrete type for functions return value
    * a common workaround is to return _Box<dyn Trait>_
      - a _box_ is a smartpointer to heap memory
      - _dyn_ is required to explicitly indicate that it's a pointer to 
      dynamic memory (the heap)
  + __overloading__
    * one can override certain existing functions by overriding the corresponding
    trait
    * to override the _+_ operator you can implement the following trait
      > impl ops::Add<SndArgType> for FirstArgType {
      >   type Output = ResultType
      >   fn add(self, _rhs: SndArgType) -> ResultType { ... }
      > }
    * _rhs_ stands for _right hand side_
  + common traits
    + __Drop trait__
      * has the method _drop()_ that frees resources and ends an item lifetime
      * automatically called at the end of an object scope
    + __Iterator trait__
      * used to implement iterators 
      * only requires to implement the _next()_ functions
      * _for_ construct turns collections into iterators using _into_iter()_
      * _take(n: u32)_ is defined from _next()_ and reduces an iterator from its
      _n_ first terms
      * _drop(n: u32)_ drop the first _n_ terms
      * Note: _into_iter()_ is used to transform collections into iterators
      * Functions which take an _Iterator_ and return another _Iterator_ are
        called 'iterator adapters' 
        - common iterator adapters are _map_, _take_ and _filter_
      * it is possible to use _impl Trait_ for signatures when you don't want
      to specify a specific type for arguments or return types
        > fn toto() -> impl Trait { ... }
        - it's an upgrade over the workaround _Box<Trait>_
    + __Deref trait__
      * contains a single method
				> type Target: ?Sized
        > fn deref(&self) -> &Self::Target;
      * implementing the _Deref_ trait allows us to customize the dereference
        operator _*_
      * _Box_ implements _Deref_
			* under the hood _*x_ is tranformed to _*(Deref::deref(&x)_
			* for mutable reference there exists the trait _DerefMut_ 
      * __deref coercion__
				- if T implements Deref<Target=U> and x is an instance of T
        - &T if forced to be converted to &U
				- it happens automatically for method calls or function parameters when 
          the type does not match
				- T implements all methods of U
        - it also possible to coerce _&mut T_ into _&U_ and convert mutable ref 
          into an immutable one
    + __PartialEq__ and __Eq__
      * _PartialEq_ for partial equivalence relations, for types that do not
        have full equivalence relation
        - for example floats equivalence is not reflexive ( _NaN_ != _NaN_)
          + examples of _NaN_ are _(n/0)_ or _√(-n)_ 
        - requires symmetry and transitivity from the partial equivalence relation
      * _Eq_ is derived from _PartialEq_
        - requires reflexivity on top of transitivity and symmetry
  + __Trait objects__
    * a trait object points to both an instance of a type implementing our trait
    but also a talbe used to look up trait methods on that type at runtime
    * useful when the type implementing the trait cannot be known at compile time
    * notation:
      > dyn Trait
      > example: Box<dyn Draw>
      > example: &dyn Draw
    * contrary to generic type _T_ with trait bounds where _T_ must corresponds
      to a unique concrete type at a time
      - for example a vector with generic type _Vec<T: Draw>_ will have its elements
       with all the same type _T_ that implements _Draw_
      - whereas a vector of type _Vec<dyn Draw>_ may have elements of different
        types as long as they implement _Draw_
    * __Sized trait__
      - specify which type has its size known at compile time
        * _u8_ is one byte
        * _Vec<T>_ is either 12 or 24 bytes (32 or 64 bits platforms)
          and independent of _T_
        * _&T_ are sized too (8 or 16 bytes), independent of _T_
      - you don't need to specify _dyn_ with this trait
      - under the hood it is used for generic function
        > fn generic<T>(t: T) { ... }
        > fn generic<T: Sized>(t: T) { ... }
        + by default generic functions only work with type that have a known size
        at compilation time
      - to relax the fact that generics only works with compile-time known type
        > fn generic<T: ?Sized>(t: &T) { ... }
      - objects which are not _sized_ are called __DST__ or _Dynamically Sized Types_
        * slices _[T]_, unknown number of _T_ contiguous in memory
        * _Trait_, because the size depends on the concrete type
          + usually you "size" unsized values with a pointer
          > &[T]
          > Box<Trait> 
    * __object safety__ for trait objects
      - trait objects must be object safe because Rust no longer knows the
        the concrete type implementing the trait
      - behaviours which aren't object safe
        + __sized Self__
          * the trait inherit from _Sized_
          * this cannot be possible since by definiiton trait objects are of
            undefined types which size is unknown at compile time
          * therefore the internal implementation (by the compiler) 
            of trait _Foo_ by type _Foo_ cannot be _Sized_
        + __self by value__
          * it is not possible to call a method of a trait object which takes
            ownership of self
          * indeed, the size of _self_ is unknown and it is illegal to have 
            unsized parameters
          * definition is allowed but calling such method is not allowed
        + __static method__
          * methods without _&self_ are not allowed
          * default implemententation of _Foo_ by _Foo_ just make a call to 
            the _vtable_ of the _&self_ parameter, which cannot be done for static
            methods
        + __references to &Self__
          * it can happen as a parameter type or as a return type
          * in both cases in means that type of the parameter or of the return value
            should be the same as _&self_
          * this cannot be guaranteed at compile time therefore it is not allowed
          * this is problematic because code may be misused since we are not
            certain that parameters have the same type
        + __generic method__
          * generic method are monomorphised therefore there exists one
            for each concrete type calling them
          * in the case of trait object it is not possible to know at compile 
            time which type is used therefore we cannot chose which function 
            to call from the vtable ( _foo1_u8_, _foo1_String_, _foo1_unit_, ...)
          * furthermore the compiler would need to implement _foo1_ for every 
            possible type which would be impossible
          * a workaround is to make the method use a trait object instead of 
            a generic
            > fn foo<A>(&self, c: A) ...;
            > fn foo(&self, c
            > fn send_embed<F: FnOnce(String) -> String>(&self, u64, &str, f: F) -> Option<u64> where Self: Sized + Sync + Send;
            > fn send_embed(&self, u64, &str, f: &Fn(String) -> String) -> Option<u64> where Self: Sync + Send;

    * __internal representation__
      - defined in _std::raw_
      > pub struct TraitObject {
      >   pub data: *mut(),
      >   pub vtable: *mut (),
      > }
      - _data_ addresses the data of the concrete type _T_
      - _vtable_ points to the implementation of the trait _Foo_ for _T_
        > struct FooVtable {
        >   destructor: fn(*mut()),
        >   size: usize,
        >   align: usize,
        >   method: fn(*const()) -> String,
        > }
        + this is the virtual table of trait _Foo_
        + there exists one instance of _FooVtable_ for the types implementing
          the trait _Foo_
        + _method_ is a method required by _Foo_, and contains a function
          pointer to the implementation of the corresponding concrete type
      - a trait object of _Foo_ contains
        + pointer to instance of the concrete type (let say _u32_)
        + pointer to the instance of type _FooVtable_ for the type _u32_
          * the vtable _FooVtable_ is a structure where every method
          of _Foo_ is a field storing a function pointer to the implementation
          of _Foo_ for the corresponding concrete type ( _u32_ here)
        + explicitly a call to _toto_ from a traitobject is
          > traitobject.vtable.toto(traitobject.data);
          * for simplicity we discarded unsafe block and dereference
      - the trait object on top of having _data_ and _vtable_ field also implements
        the methods of trait _Foo_ as a type _Foo_
        > impl Foo for Foo {
        >   fn foo1 (&self, ...) {
        >     (self.vtable.foo1)(self.data); 
        >   }
        > }
    * __supertraits__
      - Rust does not have inheritance a trait can a superset of anotheer trait
      - implementing _SuperFoo_ requires you to implement _Foo_
        > trait SuperFoo: Foo { ... }
        > impl Foo for Bar { ... }
        > impl SuperFoo for Bar { ... }
      - a trait may have multiple supertrait
        > trait FooBar: Foo + Bar { ... }
- __macro_rules!__
  + allows metaprogramming in Rust
  + contrary to C macros, Rust macros are expanded into AST
  + usage of macros
    1. __DRY (Don't Repeat Yourself)__, similar code in multiple locations
      * for example, creating new operators such as _=+_
    2. Domain-specific languages
    3. Variadic interfaces such as _println!_
  + syntax of macros
    * simplest macro
      - name of the macro if _foo_
      - _()_ means that the macro takes no arguments
      > macro_rules! foo {
      >   () => { ... };
      > }
    * 3 ideas: designators, overloading and repetition
    * __Designators__
      - macro arguments are prefixed by _$_
      > ($foo: type_of_foo) => { ... }
      - possible types are meta types 
        + _ident_ for variable/function names
        + _expr_, _block_, _literal_, _pat_ ...
    * __Overload__
      - macros can received different patterns of arguments
      - in this situation, it is simalar to a _match_ block
      - arguments don't need to be separated by a comma, any
        templates can be used
      > macro_rules! toto {
      >   ($left:expr; and $right:expr) => { ... };
      >   ($left:expr; or  $right:expr) => { ... };
      > }
      > toto!(1+3; and 56);
      > toto!(1+3; or 56);
    * __Repeat__
      - macros can use _+_ to signal that an argument may repeat one or more times
      - macros can use _*_ to signal that an arrgument may repeat zero or more times 
      > macro_rules! toto {
      >   ($x:expr) => { ... };
      >   ($x:expr, $($y:expr),+) => { ... };
      > }
      > toto!(1u32);
      > toto!(1u32, 3, 5*3);
      - arguments are used recursively as a list of values
      - usage in the macro calls the macro recursively
- __Error handling__
  + in Rust errors are separated into two categories: 
    unrecoverable and recoverable errors
  + most languages don't distinguish between the two and wrap them under _exceptions_
  + __unrecoverable errors__
    * errors that may put your program in a bad state
      - an unexpected situation
      - following code rely on not being in a bad state
      - user provides values that does not make sense to the API
    * in these situations it may be preferred to stop the program instead of
      proceeding on
    * the __panic!__ macro serves such purpose
      1. print a failure message
      2. unwind and clean the stack
      3. exit the process
      > panic!("Panic error haaaaaaaa!");
      + to get a backtrace you can set the environment variable RUST_BACKTRACE to 1
        > $ RUST_BACKTRACE=1 cargo run
    * for prototyping and testing, when you haven't tackle the error handling
      you may use either _panic!_ or _unimplemented!_
  + __recoverable errors__
    * most errors are predictable and you may prefer handle the problem rather 
      than stopping the program
    * for potential recoverable error you wrap a computation in the 
      __Result<T,E>__ enum
      - two variants: _Ok(T)_ and _Err(E)_
    * to handle _Result<T, E>_ type you can
      - pattern match on the enum
      - use _unwrap_ or _expect_ for less verbose syntax
        + both trigger the unwrapped value with _Ok_ and throws a _panic!_ with _Err_
        + often used when you have more information than the compiler and you
          know that it is not possible to get the _Err_ variant
        + _expect(&str)_ enhance _unwrap()_ by customizing the _panic!_ message
    * match different errors
      - the type of _E_ in _Err(E)_ is generaly the enum _std::io::ErrorKind_
        which list different types of errors
      - you may match on _ErrorKind_ for different error handlings
    * _propagating errors_
      - sometimes it is more appropriate for the calling code to handle errors
      - in these situations you will return and propagate the _Result_ obtained
      - __?__ may be used as a shortcut to propagate errors
        + used as a prefix to a _Result_ value
          > f.read_to_string(&mut s)?
        + if value is _Ok_ then it returns the unwrapped value and
          the function continue
        + if value is _Err_ then program returns from the function with
          the _Err_ value
        + _try?_ is the ancestor of _?_
      - __map(closure)__ and __and_then(closure)__ 
        can also be used to work on _Result_ without unwrapping it
        > let a = Ok("a");
        > let b: Result<String, ErrorKind> = a.map(|v| v.to_owned());
        - _map(FnOnce(T) -> T)_
        - _and_then(FnOnce(T) -> Result<T, E>)_
    * _Result_ can be the return type of the _main_ function
        > fn main() -> Result<(), ParseIntError> { ... }
- unsafe Rust
  + unsafe blocks are necessary when you want to write safe 
    code that the compiler is unable to check
  + 4 behaviours are allowed in _unsafe blocks_
    1. dereferencing raw pointers
    2. call an unsafe function/mehod
    3. access or a modify a mutable static variable
    4. imlement an unsafe trait
  + __raw pointers__
    * useful for _FFI_ Foreign Functions Interfaces
    * opposed to _Box_ or _&_
    * _*const T_ and _*mut T_
      - are not guaranteed to point to valid memory or to be non-NULL
      - no automatic cleanup
      - are just plain data and have no ownership semantic
      - no lifetimes
      - no guarantees about aliasing or mutability
    * creating raw pointers is safe but dereferencing is not
      > let x = 5;
      > let raw = &x as *const i32;
      > let points_at = unsafe { *raw };
    * conversion from _&T_ to _*const T_ is safe, the opposite is not
      - in fact, conversion of _&T_ to _*const T_ is done implictly in safe code
  + __unsafe function/method__
    * functions that require additional condition to execute safely
      > unsafe fn toto() { ... }  // definition
      > unsafe { toto(); }         // call
    * condition may be ensured in Rust (with assertions)
    * _Foreign Function Intervae_ see below
- __Foreign Function Interface___
  + wirte code to use with an external li
    > extern "C" { abs ... }
    > unsafe abs();
    * _extern_ helps the cook to choose the correct ABi for the language
  + write readable code external users when using our lib
      > #[no_mangle]
      > pub extern "C" fn toto() { ... }
      + n omangle with theam demj
- type checker
  + Rust only need to know signatures of functions/methods to type-check a program
  + __orphan rule__
    * to implement a trait for a type, _rustc_ requires to have at least the
      trait or the type to be local to our crate
      - this is required to avoid the "diamond problem"
        + where a class A is derived from B and C which both have a common parent D
        + an instance of A calls the method _foo_ derived from either B or C
        + which method implementation should be called?
      - also disabling the orphan rule may allow one to override the behaviour
        of a struct within another crate
    * this is required to respect the __ODR__ (One Definition Rule)
      - which implementation to chose if we have type 
        _Show for (YourType, MyType)_ with:
        > impl<T> Show for (T, MyType) { ... }
        > impl<T> Show for (YourType, T) { ... }
      - two crates could both implement an external trait for external types,
        which one should we choose
    * a workaround to avoid the orphan rule is to use the __newtype pattern__
      - the idea is to wrap the external type into a local struct
        * if we want to implement the trait _Display_ for _Vec_
          > struct Wrapper(Vec<String>);
          > impl fmt::Display for Wrapper { ... }
      - this pattern has no performance penalty since it is elided at compile time
      - the main drawback is that we need to implement of _Vec<T>_ in our 
        _Wrapper_ to use it as intended
        * an idea to solve this drawback is to implement the _Deref_ trait
        * implementing the _Deref_ trait allows us to customize the dereference
          operator _*_
### Considering Rust
#What is Rust?
- Buzz words
	+ Systems programming
	+ Fast, reliable and productive -- pick 3
	+ Fearless concurrency
	+ Community-driven and open-source
- Technical Bits
	+ Compiled
	+ Strong static typing
	+ Imperative with functional aspects
	+ No Gc or runtime
	+ elaborate type system
# Comparisons to other languages
- Comparison criteria
	+ speed
	+ memory usage
	+ safety
	+ concurrency
	+ build system
	+ dependency management
	+ documentation
	+ community
# Features of Rust
+ modern language/nice to use
	* efficient generics, monomorphisation
	* algebraic data types + pattern matching
	  exhausistive match is checked statically
	* modern tooling
	  Compilers knows about test and documentation
	* really nice compiler errors
- Safety by construction
	+ checking pointers by construction at compile-time
		* borrow-checker:
		1. value have one owners
		2. pointers don't live past owner changes or drops
		* immutable value by default
	+ Thread-safety from types
		* Rc and Arc (Atomic Reference Counting)
		* one unique mutable reference or muliple immutable references
	+ No hidden states
		* you don't have null pointers
		* usage of Option or Result type
- Low-level control
	+ no GC or runtime
	  can issue system calls, 
	  can run on systems without an OS,
	  free FFI calls to other languages
	+ control allocation and dispatch
	  you can choose how you allocate memory (heap, stack, which malloc ...)
	+ can write and wrap low-level code
	  with unsafe blocks, for invariants the compiler cannot check
	  write asm code in unsafe block
- Compatibility
	+ zero-overhead FFI
	  trivial to access any function following C ABI
	  export Rust function through C ABI (callable from C)
	  work with every language compatible with the C ABI
	  C, C++, Python, Ruby ...
	  There is CBindGen, create C header file or give a Rust API from C
	+ incrementally replacement of modules of a big project to Rust 
	+ great WebAssembly support
	  one of the best integration to WASM
	+ Works with traditional tools
	  Since Rust produces binaries that are indistinguishable from a C++ binary
	  perf, gdb/lldb, valgrind, llvm sanitizers works
- Tooling
	+ dependency management
	  builtin in Cargo (Cargo.toml)
	  Cargo automatically download and build it for you
	  With support for versions, knows which versions have sematical equivalence
	  Knows which versions are compatible with each other
	  Custom build support (Make, Ninja ...)
	+ standard tools (formatter)
	  cargo fmt, code formatter
	  cargo doc, doc generator
	  cargo clippy, linter
	  rls/rust-analyzer, compiler front-end for IDE integration
	+ Excellent support for macros
	  fully-fledged program with syntax to interact with AST
	  input and output are valid Rust
	  hygienic, identifiers of the macro does not contaminate the rest of Rust code
	  There is a library that automatically Serialize/Deserialize for any
	  format written with macros
- Asynchronous code
  Threadings without the need to go to the kernel
	+ language support for writing asynchronous code
	+ choose your own runtime
# Data representation
- type size is always a multiple of its alignment
- composite data struct
	+ there is no guarantees on how a data structure will be mapped
	+ all field will have its type size a multiple of its alignment
	+ layout/field order is often optimized to minimize memory usage
- DSTs ( _Dynamically Sized Types_ )
	+ two major DSTs
		* trait objects: 
			> dyn MyTrait
			- runtime reflection layout in form of a vtable
      > pub struct TraitObject {
      >   pub data: *mut(),
      >   pub vtable: *mut (),
      > }
		* slices
			> [T], str 
			- pointer + nb of elements it points to
- ZSTs ( _Zero Sized Types_ )
	+ Struct with no fields, emply tuple, 0-length array
	+ Rust compiler knows that it stores 0 data and consider any of
		these load/store as noop
	+ can be useful with generic types
		* for example implementing a HashSet<Key> using the struct HashMap<Key, ()>
- Empty types
	+ types that cannot even be instantiated
	+ enum with no variants
		> enum Void {}
	+ can be used for unreachable case
		* for example type _Result<T, Void>_ when we can't have the _Err()_ type
- Alternative representations
	+ specify a data layout (for example a layout identical to the C language)
	+ _#[repr(C)]_
		* DST pointers does not exist in C so not FFI-safe
		* Enum with fields does not exist, but valid bridgin exists
		* any non-nullable pointer type have the same layout and ABI
		* Tuple are the same except they are not named
		* Fieldless enums complicated see rusnomicon page 18
	
# Drawbacks
- Learning curve
	+ borrow-checher
	  new way of reasonning about your program
	+ no object-oriented programming
- Ecosystem
	+ young and few maintainer
	  Even if it's still changing
	  Integration with entreprise libraries are lacking (oracle databases ...)
	  One of the best regex library
	  Community and documentation is good to make it up
- No runtime
	+ no runtime reflection
	  cannot inspect objects at runtime
	+ no runtime-aided debugging
	  only gdb, lldb but no rich information
- Compile time
  	slow but improving
	use llvm, so the pipeline is quite long
	getting better at doing incremental compilation
	no pre-built libraries in Rust, you need to compile from source
	this is due to generics, you need to compile the method for your type
- Vendor support
	+ huge C++ libraries are a pain
	  Sometimes linking or wrapping complex libraries is complicated
	+ Some tooling only supports C++
- Windows
	+ Full compiler/std support
	+ limited library support
	  The community is mainly open-source
	  Linux and MacOS focused

# Long-term viability 
- Most loved language 4 years running
- adoption by large companies
- great interoperability, easy incremental adoption
- increased company involvement in Rust 
- ~10 yearly conferences around the world

# Project structure 
- _mod foo;_ this keyword import module named foo found in the project
	+ follow the order:
		1. search module _foo_ in the file 
		2. search file named _foo.rs_ in the directory 
		3. search file named _mod.rs_ in the directory _foo_ in the directory
			* file _mod.rs_ is responsible for calling files (let's say _foo/bar.rs_)
				of its directory with:
				> pub mod bar;
- src
	+ _main.rs_ if there is an executable 
	+ _lib.rs_ for a library
	+ _foo.rs_ if t

# Wrapper Types 
- &T and &mut T
	+ was created to have memory safety with zero cost abstraction 
	+ check lifetime to avoid double free, call to null ...
	+ policy of "one mutable or many immutable" to avoid data race 
- Box<T>
	+ allow to own data which gives us pointer 
	+ mainly to avoid Sized data struct giving a pointer to heap data
	+ low cost abstraction for dynamic allocation
- *const T and *mut T
	+ raw pointers 
	+ dereferencing them must be done in unsafe code
- Rc<T>
	+ Reference counter 
	+ Allow to have multiple owner of a piece of data
	+ Useful when you don't know precisely the lifetime of your data
	+ Data is wiped when the _strong_ ref counter reach 0 
	+ there is a weak ref counter as well
	+ has a cost at runtime (Increment/decrement counters)
	+ can have memory leak in cyclic reference
- Cell<T>
	+ 0-cost abstraction 
	+ give interior immutability (mutable data stored in immutable wrapper)
	+ when used you have to be aware that the data may have been modified 
	+ can only be used when inner data implement the _Copy_ trait
		* this is enough for memory safety, since _copy_ object haved fixed size
		* shared mutability most of the time cause "logic errors" and not "memory safety"
			erros which is our prime goal 
		* memory safety errors with shared mutability can occur when changing the 
			type or size of inner data 
			- changing variant of an enum 
			- changing length of a vector, may be relocated in the memory
		* if inner data has _Copy_ trait memory safety errors won't happen 
- RefCell<T>
	+ give interior immutability to any type
	+ enforce the _many read or single write_ at runtime instead of compile time
	+ when you create safe code that the borrow checker cannot evaluate
	+ possess a mutable and immutable ref counter
- Send and Synchronous types 
	+ marker traits (no associated items or methods)
	+ a type is _Send_ if it is safe to send it to another thread
	+ a type is _Sync_ if it is safe to share between threads
	+ T is is _Sync_ iff &T is _Send_
	+ marker trait (no associated items or methods)
		* traits are automatically derived
		* almost all types are Send and Sync
		* not Send or Sync
			- raw pointers are neither Sync or Send
			- UnsafeCell is not Sync
			- Rc is neither Sync or Send
	+ Send types
		* can be moved or copied to a different thread
		* UnsafeCell<T>
	+ Sync types
		+ can be referenced by multiple threads 
		+ Arc<T>
			* Atomic Ref Couter 
			* Similar to Rc but ref counting done automatically
			* more costly for performance
		+ Mutex<T> 
			* thread needs to acquire a lock to access the data (mutably)
			* release the lock when done with the data 
			* prone to deadlock, atomic op can be costly
		+ RWLock<T>
			* same as mutex but more permissive
			* allow multiple read locks but a single write locks
			* no read and write simultaneously
			* prone to deadlock, atomic op can be costly
- _Borrow<T> vs AsRef<T>_
  + let's say we have a type `Toto` which is a wrapper to type `T`, shall 
    we implement trait `Borrow` or `AsRef`?
  + both traits means that `Toto` can provide a reference of type `T`
  + when implementing other traits, for example `Debug`, implementing 
    `Borrow<T>` means that `T::debug()` behaves exactly as `Toto::debug()`
  + there are no implicit equivalence in trait implementations when implementing
    `AsRef`
  
# RustBelt 
- project about formal verification of Rust 
- developed a model of Rust: λ_Rust
	* inspired by MIR
	* represent most of Rust core language
	* defined a semantic model of λ_Rust type system
		+ using syntactic type soundness would not tell us anything about 
			blocks of unsafe or interacting with them
		+ use the semantic approach to define what a type is through logical relations
		+ what terms inhabit a type through the observable behaviour instead of
			a fixed set of inference rules
		+ different from syntactic typing rules
		+ ex: for a function to be well-typed only its input-output behaviour is 
			relevant
			* the body of the function can be unsafe as long as it does not violate 
				the type system guarantees
- Safety proof results of λ_Rust:
	__as long as unsafe code in a well-typed λ_rust program is confined to libraries
		that satisfy their verification conditions, the program is safe to execute__
	1. _The fundamental theorem of logical relations_
		+ typing rules of λ_Rust are sound when interpreted semantically
			* semantically well-typed as long you follow the typing rules
		+ for each typing rule a lemma
			* semantic interpretations of the premises imply the semantic interpretation
				of the conclusion
	2. _Adequacy_
		+ if a closed program is semantically well-typed according to λ_Rust 
			then there will be no safety violations when executed
	3. _library-specific verification condition_
		+ semantic interpretation of a library yields safety contracts 
			on API construction
		+ if the inputs to methods of the API conform to their specified types 
			then so do the outputs 
		+ basically if you call API in safe code then the API usage will be safe

# Stacked Borrows
- extension of type system giving additional information to the compilers
	for optimizations
- operational dynamic semantics for pointer aliasing in Rust
	+ introduce clearly defined conditions under which Rust exhibits undefined
		behaviour due to an alising violation (in unsafe)
	+ dynamic version of the borrow checker
		* the borrow checker just rules out unsafe code and do not give more info
		* rules for raw pointers (ignored by borrow checkers)
- Semantics which have undefined behaviour allowing the compiler some
	freedom for optimization
	+ "desired" programs should still be well-defined
- Validation of Stacked Borrows: 
	+ don't want too much undefined behaviour
		* extended Miri with an implementation of Stacked Borrows
			- interpreter for MIR 
	+ want to have "enough" undefined behaviour 
		* proof sketeches showing situations where we wave "desired" UB
		* proof are mechanized with formalization of Stacked Borrows



# Wasm
- Crates for Rust and wasm 
  + JS and DOM
    + wasm-bindgen: facilitates interactions between Rust and Js 
      * import Js into Rust 
      * export Rust to Js 
    + wasm-bindgen-futures 
      * bridge connecning Js promises and Rust futures
      * allows interacting with DOM events and I/O
    + js-sys 
      * imports all JS global types and methods 
    + web-sys 
      * imports all web api 
      * DOM, timeout, web gl, web audio
  + Errors reports and logs
    * console_error_panic_hook 
      - debug panics on wasm32-unknown-unknwown 
    * console_log 
      - backend for the log crate
  + Dynamic allocation 
    * wee-allow 
      - small .wasm allocator implementation 
      - useful when code size is a greater concern than allocation performance
  + Parsing and generating .wasm binaries 
    * parity-wasm : serialize/deserialize/building wasm binaries
    * wasmparser: parse wafm binary files
  + Interpreting and compiling WebAssembly 
    * wasmi: interpreter for wasm 
    * cranelift-wasm: compile wasm to native machine code
- Tools for Rust and Wasm 
  + wasm-pack 
    * helps to build and publish rust-generated wasm to be glueued with js package
- Things that does not work with wasm 
  + C and System Library dependencies
    * crates that bind to a system lib 
    * C libraries since wasm does not have a stable ABI for cross-com
  + File I/O 
  + Spawning threads

## build targets for wasm32
- emscripten: to create full self-conteined application
  + uses emscripten to provide the standard lib
  + TCP/socket/files is provided
- unknown
  + usually used for a lib (pure computation called from js)
  + bare bone approach
  + most sys call will panic
  + smaller modules since it does not contain all the emulation
  + wee_alloc is used for heap allocated data structures 
  + wasm-bindgen provides interop with js 
- wasi 
  + WebAssebmly System Interface 
  + API designed by wasmtime providing access to several os features
  + to run wasi programs you use either wasmtime or browser polyfill

## Wasm runtimes 
- Wasm runtime features
  1. Enable programs to run with any programing language 
  2. Enable portable binaries to run on any OS supported by wasmer
  3. Act as a secure brige for wasm modules to interact with 
      native OS function via ABI (ex: WASI or Emscripten)
- __Wasmer__: best compatibility support with programing languages with fast speed 
  + features
    * pluggability: comatible with various compilation framework 
    * speed/safety: native speed in a sandboxed setting
    * universality: works on any platform/chipset
    * support: big community support
  + crates published by wasmer
    * compilers
      - singlepass: fast compilation, slow runtime
      - cranelift: normal compilation, normal runtime
      - llvm: slow compilation, fast runtime
    * engines
      - universal: load and link the binary file
      - dylib: generates and links shared object file from PIC wasm code
      - staticlib: to produce static object library 
    * integrations 
      - wasmer-wasi: implementation of the wasi standard
      - wasmer-emscripten: implementation of Emscripten ABI
      - these two crates are built on top of the wasmer crate
        * same relation as system calls (wasmer) and libc (wasmer-wasi)
      
- __Wasmtime__: fast, compact and good configurability
  + compact: small standalone runtime 
  + easily modified: tweak wasmtime to suit your needs
  + speedy: high resolution runtime 
  + wasi-compatible
  + support: big community support
- __Lucet__: specialized solution for running untrusted wasm app in larger app
  + fast 
  + wasi-friendly 
  + support many languages
  + AOT compilation support
- __Wamr__  (Web Assembly Micro Runtime): small footprint
  + made of 3 components 
    * VM core with JIT/AOT and interpreter 
    * application framework to build wasm apps
    * dynamic/remote app management from cloud or host env
  + fast (AOT)
  + compliance to W3C wasm mvp 
  + small binaries
  + requires small memory resources

# Tokio 
- Runtime for async Rust 
  + multi-threaded runtime 
  + async version of the standard lib 
  + large ecosystem of lib
