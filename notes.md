##### Note #####
# OS
- Today
  + __Unix__, answer the SUS (Single Unix Specification)
    * System V is one of the first commercial version of Unix OS,
      it has derived into: IBM AIX, HP-UX, Oracle Solaris
    * macOS
  + __Unix-like__, no specified definition
    * os which behave similarly to a Unix system
    * these OS are usually partially compatible to the POSIX standard
    * can be written as *nix, Uni*x, *n?x
    * List
      - Linux/GNU
        + GNU stands for _Gnu Not Unix_
        + the goal was to have a Unix compliant free OS
        + the original kernel was Gnu Herd but was replaced by Linux since
          it has never been released
        + Linux is the kernel and GNU is a collection of free software
          to use with the OS
      - BSD
        + was derived from the inital UNIX open source code
  + Windows
    * Dos
    * Windows NT (starting from Windows XP)
- package manager
	+ dpkg (Debian Package)
		* present in all debian based os
# Architectures
- Computer architectures
  - Von Neumann architecture
    + used by classic computers
    + contains:
      * A processing unit that contains an arithmetic logic unit and processor registers
      * A control unit that contains an instruction register and program counter
      * Memory that stores data and instructions
      * External mass storage
      * Input and output mechanisms
    + usually used by abstract machine:
      * Java Virtual Machine
      * webassembly
  - Harvard architecture
    + centralized around a control unit which is linked to
      * ALU and registers
      * Memory for data
      * Memory for instructions
      * I/O mechanisms
    + biggest difference with Von Neumann is that memories for data and instructions
    are separated
      → instruction address and data address both starts at 0
      → CPU can fetch both an instruction and data at the same time
    + popular with
      * microcontroller where the entire computer holds on a chip
      * Digital signal processor, where speed is paramount
  - Modified Harvard architecture
    + hybrid between Von Neumann and pure Harvard
    + contain a single address-space but also have additional separate
    faster cache memory for data and instructions
    + This is the architecture used in modern computers
# Virtualization
- 3 types: __Emulation__, __Paravirtualization__, __Container-based virtualization__
- __Paravirtualization__
	+ Hardware|Hypervisor > Host OS > Guest OSes
	+ uses Type1 Hypervisor: runs directly on hardware
	+ bypass the host OS to services directly the VM
	+ Host OS, virtualized hardware, real hardware collaborate to run VMs
	+ examples: Xen, KVM, ...
- __Emulation__
	+ Hardware > Host OS > Hypervisor > Guest OSes
	+ also known as full virtualization
	+ runs the VM kernel entirely in software
	+ use Type2 hypervisor: full software hypervisor
	+ examples: VirtualBox, QEMU, VMware Player ...
- __Container-based virtualization__
	+ Hardware > Host OS > Containers
	+ enabled by Linux __namespaces__ and __cgroups__
	+ __namespaces__
		* isolated container that has no visibility or access to outside world
		* processes appear to run on normal Linux although they are sharing
			kernel and other objects with processes of other namespaces
		* f.e: you can be root in your namespace and not in the system
	+ __cgroups__
		* used to manage ressources of a group of processes
		* used to limit memory and CPU consumption of containers
	+ containers have near-native performance which makes them
		really efficient virtualization tools
	+ containers state are much smaller compared to VM image
	+ close to virtual machines but all containers run on the same
		kernel (contrary to VMs)
	+ VMs vs containers
			* containers can't run different OSs
			* _containers run distros (of the same kernel), VMs run OSes_
			* containers are faster than VMs
			* isolation of VMs are stronger
	+ example: _Docker_
   
## Docker
* _Docker_ is a framework to manage contained apps
  + apps are isolated into containers
    * based on linux namespaces (cgroups and linux kernel)
    * parts of resources (disk, cpu, Ram) are allocated to namespaces,
    and processes running in these namespaces cannot see/access
    ressources external to its namespace
    * processes cannot see/talk to processes of another namespace
  + Docker allows for immutable servers
    * servers which are not updated but replaced entirely
    * on Windows and Mac, Docker uses a Linux VM to run the containers
  + uses Hyper-V on windows (the native hypervisor)
  + uses Hypervisor.framework on Mac
* commands
  + _docker + cmd_
  + _pull_: fetch an image from the Docker registry
    * _images_ can be used to list available images on our system
  + _run_: run a Docker container on provided image
    * run a command in a new container and close
    * _run -it_ attaches us to an interactive tty in the container 
      - to run more than one command
    * _run -d_ detach the container from the terminal
      - _stop_ to stop a detached container
    * _run -P_ assign exposed container ports to random ports of the system
  + _ps_: show containers that are currently running
    * usually we used _ps -a_ to also list containers that were running
  + _rm_: delete container 
    * to free disk space
    * _docker rm $(docker ps -a -q -f status=exited)_ to deleted exited containers
  + _rmi_: delete image
  + _build_: create an image from a Dockerfile
  + _create_: create a container from an image
  + _port [container]_: show container ports and their associated system ports 
  + _push_: push an image on a registry
  + _network_: manage network 
    * between containers 
    * between containers and host
* terminology 
  + images: blueprints of our app and basis of containers
    - also called snapshots: represent an app and its env at a specific point in time
    - immutable file containing source code, lib, dependencies...
    - used as a template to build a container
  + containers: crated from Docker images and run the app
    - running image which is also writable
    - virtualized runtime env where users can isolate app from the system
  + Docker daemon: background service that build/run/distribute docker containers
  + Docker client: cli tool that allows interactions with the daemon
    * there are also other clients such as Kitematic which provides a GUI
  + Docker hum: a registry of docker images
* Dockerfile
  + _FROM_: specify the base image
  + _WORKDIR_: set a working directory for the app
  + _COPY_: files to be copied in the app
  + _RUN_: run a command at build time
    * get written into your docker image as a new layer
  + _EXPOSE_: expose a port
  + _CMD_: run a command when a container starts
* Docker tools 
  + Docker Machine: create docker hosts on computer/cloud providers/data center
  + Docker Compose: to define and run multiple container docker apps 
  + Docker swarm: native clustering solution for docker
  + Kubernetes: system for deployment, scaling and management of containerized apps

# Languages
- __Fortran__(1954)
    + FORmula TRANslator
    + started with punchcard
    + for scientific computation
    + one of the first language with variable names, loops, I/O, functions
- __Cobol__(1959)
    + COmmon Business Oriented Language
    + aimed at business (successful)
    + close to english and easy to use
    + Features
        * Macros
        * Records
        * Long names
    + Variables have fixed position in memory
        * fast and easy to compile
        * no dynamic alloc (i.e. no data structure)
        * no recursive func
- __Lisp__(1958)
    + LISt Processing
    + Features
        * Symbolic expr instead of numbers
        * List list list
        * Func Func Func / recursion / composition
        * garbage collector
    + Lisp programs are S-expr (list), so Lisp programs can manipulate
    other Lisp prog
    + relatively slow
    + slowly relaced by Scheme (1975)
        * smaller than Lisp
        * popular in academia

- __Algol__(1958)
    + ALGOrithmic Language
    + for scientific computation
    + not popular but led to Pascal, C, Java
    + first machine-independent language
    + use BNF to describe language syntax
    + FORTRAN was more popular

# Rust
- See notes in rust.md
# OCaml
  - language orienté fonctionnel
    + pattern matching
    + par défaut les structures/variables sont immuables
    + Type algébrique
    + first class function
      + fonction polymorphe
      + fonction d'ordre supérieure
    + persistance, en général les fonctions ne modifie pas
    les données passées en argument elles vont en récréer
      * persistance = ovservationnellement immuable
      * plus facile à lire/comprendre -> correction plus facile à justifier
      * facilite le backtracking vu que chaque état est une nvelle
      structure de données
    + prefer usage of let bindings rather than assignment
      * let bindings are not assignments but identifier in a new scope
  - existence de boucles
  - structure de données de base: Array/List/Tuple/Record
  - les variables pouvant êtres mutés sont annotés avec ref
    > let x = ref 3;;
    + les variables mutables sont des records avec des champs mutables
  - Utilisation de modules et foncteurs
    + les foncteurs sont des modules paramétrables avec des modules
    + ex: table de hachage
      + paramétrées par un module définissant
        1. type des hash
        2. fonction de hashage
        3. fonction d'égalité
    + OCaml propose déja:
      * Hashtbl.Make, Set.Make (type ordonné), Map.Make
  - Pour choisir ce qui est publique/privé dans un module on peut aussi
  spécifier la signature du module qui correspond à son interface
  - OCaml encourage




# Posix
- Portable Operating System Interface
  + Standard to have compatibility between OS
  + API used for Unix-like systems
# Unix
- Contrôle d'accès
  + Accès aux données par __getent__
    * contrôlé par nsswith.conf
    * peut-être par les fichiers, ldap ...
  + Authentification et PAM
    * _Pluggable Authentification Module_
    * framework pour l'authentification
      - login, su, sshd, gdm
    * Peut utiliser différents modules
      - kerberos, unix, ldap ...
    * gère différents points
      - auth, pour l'authentification
      - password, pour la gestion des mots de passe
      - account, pour la gestion des comptes autres que l'authentification
        + durée de vie, heures de connexion ...
      - session, les actions avant et après authentification
    * règles PAM
      1. le type: auth, password, account, session
      2. le niveau: required, requisite, sufficient, optional
      3. le nom du module: pam_unix, pam_ldap ...
      4. options spécifiques au module
  + DAC vs MAC vs RBAC
    - Discretary Access Control
      * le proprio de la ressource gère ses autorisations
      * assure avaibility
    - Mandatory Access Control
      * un système centralisé gère les permissions des sujets sur les ressources
      * assure confidentiality
        + utilisé dans l'armée où l'on requiert des permissions très fines
    - Role Based Access Control
      * les accès aux ressources sont divisés selon différents rôles
      * assure integrity
        + exemple d'une entreprise où il y a un pôle business et développement
        + les pôles peuvent lire les ressources mais seuls certains peuvent les modifier
  + ACL Posix
    - permet un contrôle plus fin du système de permissions par défaut (DAC)
    - définit des permissions pour les utilisateurs ou les groupes
    - plus flexible, plus expressif
    - ACL minimales, étendues par défaut
      + minimales correspond au système de permission UNIX par défaut
      + étendues on peut faire des ACL pour des users ou groupes nommés
      + défaut, pour les répertoires, s'applique à la création de fichiers
  + Capacités POSIX
    - UNIX classiques, c'est out ou rien pour les appels systèmes:
    Soit root soit setuid root pour pouvoir faire des opérations privilégiés:w
    - les capacités permet d'octroyer des droits précis pour un processus
      + droit de chown, kill, configure network ...
    - les capacités sont chargées par le noyau à l'exécution

- __bash__
    * __sed__
        + sed '/coco/d' file, remove all lines with coo
        + sed '/coco/!d' file, remove all lines which does not contain coco
        + sed 's/foo/bar/g' fiel, replace all foo by bar
        + sed 'regexp' -i file, replace the content of the file with stdout
    * __grep__
        + grep "word" files
        + grep "word" -R ., recursive grep
        + grep "word" *.c -l, find in all c file "word" and only output matching file names
        + grep "word" -v, find lines NOT matching
    * __file__
        + examinate file type, gives also 32/64bits info
    * __patch__
        + apply a patch created from a diff
        + patch foo.txt < foo.patch (>), apply the patch foo.patch to foo.txt
        + patch -p1 < foo.patch (>), apply the patch to a directory, -p1 omit the first level of the path
    * __diff__
        + show difference between 2 files
        + diff -u before.c after.c > change.patch, create a patch fro before.c to after.c
        + diff -rupN original/ new/ > original.patch, create a patch for the whole directory
        + diff <(command) <(command2) (>), this <() (>) puts the result of the command in a
        tempory file
    * __tar__
        + to make archives
        + tar -xvf foo.tar, extract archive tar
        + tar -czvf foo.tar.gz foo.c foo.h, create a compressed archive foo.tar.gz
        + -x for extract
        + -v for verbose
        + -f for archive/file name else there is a default
        + -c for create
        + -z for compress/uncompress
    * __string operations__
        + $(dirname $foo), script relative location
        + $(basename $foo), script name
        + ${foo%.*}, name without extension
    * __export__
        + put a variable in the shell environment
        + shell environment is herited by subprocesses
    * __readlink__
        + give true path of a link
        + -f, follow links recursively but last has to exist
    * __ln__
        + ln -s target link_name
        + -r, link to relative_path to target
    * __type__
        + return information on files or executables
        + -t return type of the word, alias, builtin, (user) function, file
        + -p return absolute path if the arg is a file
        + -a return all info
    * __nm__
        + list symbols found in object file/executables
        + give the type of the symbols as well
        + usually lowercase corresponds to local symbols whereas uppercase match global symbols
        + for shared lib use the -D/--dynamix option otherwise you look at debug symbols
    * __disown__
        + remove a job from active table
        + _disown -h %1_
          → nohup the background job %1
        + _disown -a_
          → nohup all the jobs
    * __xargs__
        + transform output into input
        + input usually from a pipe
        + _ls | xargs rm_
- elf
    + a.out is not an elf file, old format
    + a.out does not support dynamic lib
    + fPIC compilation
        * is used for shared lib, but slower
        * pc_thunk function, stores location of the GOT table in %ebx
        * %ebx will be reserved and can't be used for calculations
        * GOT, is a table of pointers, one for each globvar used in the shared lib
        * for functions we use a procedure linkage table (PLT) to reference all the functions
        * PLT is just a jump table of the functions available in shared libs
    + section
        * .interp, name of the dynamic loader
        * .hash, .dynsym, .dynstr, tables used by dynamic linker
        * .plt, jump table for functions of the shared lib (procedure linkage table)
        Use lazy symbol binding, before being used it only points to the dynamic loader
        Save time for the dynamic loader
        * .dynamic, notes for the dynamic loader
        * type
            - PROGBITS, section to load
            - NOBITS, .bss only 0
            - NOTE,   additional info
            - STRTAB, string table
            - SYMTAB, symbol table for static linking, include debug symbols used by GDB
            - DSYMTAB, symbols used for dynamic linking
- kernel
    + gdt (Global Descriptor Table), ldt (Local Descriptor Table)
        * Gdt used to describe segments information, task stake, ldt, call gates, pages
        * ldt is fine grained and specific gdt
- Debug
    + strace
        * strace executables,   show every syscall
        * strace -e trace=open, follow only syscall open
    + ltrace
        * like strace but shows calls to shared lib
    + gdb
        * points
            - __breakpoints__, stops at a specific location
            - __watchpoints__, stops when the value of an expression changes
            - __catchpoints__, stops when a special event occurs (exception, signal...)
        * __info__ give info on the state of the program
        * __show__ give info on the state of gdb
        * __finish__ allow to step out of a function
        * __until__ is like next except it stops only if current pc is farther than the one when
        until was invoked. Useful to go through loops
        * __nexti__ like stepi but does not step in func call
        * __target remote host:port__ for remote debug
        * __symbol-file__ when missing symbol it is possible to indicate it
- make
    + $(lastword $(list of words)), get lastword
    + $(dir file_name), get path to file_name
    + $(MAKEFILE_LIST), list of files included by make
    + $(foreach var,list,text)
- IPC
    * __file__, a record stored on disk accessible by multiple processes
    * __memory-mapped file__, a file mapped to RAM
    * __shared memory__, shared block of ram

    * __signal/asynchronous sys trap__, a message sent to another process, not for data but
    remotely command the target process
    * __message queue__, a data stream similar to sockets
        + usually implemented by the os
        + preverves msg boundaries
        + allow multiple processes to read and write to the msg queue
    * __msg passing__, allows multiple programs to communicate using msg queues on
    non-OS managed channels

    * __socket__, a data stream sent over network interface
        + byte-oriented
        + rarely preserve message boundaries
        + requires formatting to preserve msg boundaries
    * __unix domain socket__, similar to sockets but communication occurs in the kernel
        + use the filesystem as their address space
        + processes refer to domain socket as an inode
        + multiple sockets can converse with a single socket
    * __pipe__, unilateral data channel
        + data transferred is buffered until the targeted proccess read it
        + use stdout/stdin
    * __named pipe__, like pipe but using other inode than stdout and stdin
+ UID
    - Real, account owner of the process and should never change
    - Effective, id used to start process, it can be elevated to run files with escalated privilege
    - Saved, when a user with high privilege (root) want to run as a user with less privilege it
    changes its effective UID and stores the privileged uid in the saved one

# Réseau
- what is a network
	+ a network is used to connect multiple devices together: printer, computer ...
			* can be connected through wires or wirelessly
			* with a switch/wifi box/directly
	+ through the network the devices are able to share data/information
	+ to communicate the devices must share the same language
	+ these languages are called protocols: HTTP, SMTP, TCP...
- network type
	+ no clear definitions
	+ SOHO, _Small Office, Home Office_
	+ LAN, _Local Area Network_
	+ WAN, _Wide Area Network_
- Wired network
	+ types: copper or fiber
		* copper is cheaper, for short distances use eletric signals, can
			be affected by interference
		* fiber is more expensive, made of glass and use light, cannot be interfered,
			used for long distances
		* use __ethernet__ protocol
				- composed of:
					- types of cabling and their speed (describe physical links)
					- how the data should be formatted ( _media access control_ rules)
				- if a computer want to send a message to a server through ethernet
					1. format the msg following media access control rules
					2. prepare the msg according to physical rules that it has to follow
						(which cable with which speed)
					3. server decodes the physical signal
					4. server decodes the MAC format
				- designed by IEEE
					- 802.*** = LAN tech
						+ 802.3.** = Ethernet
				- Ethernet most common copper cable is called Unshielded Twisted Pair (UTP)
					- currently made of 4 pairs of cable
					- each pair are twisted to avoid creating magnetic fields and
						interfere with other pairs
					- some pairs are used to transmit (TX) or receive (RX), new categories
						used pairs for both receive and transmit
					- cables are standardised by categories (number of pairs, how
						thin/thick, how twisted ...)
						+ affect speed of transmission
						+ how far can you send data
					- RG45 is the connecter at the end points
				- Ethernet fiber cable:
					- pulsing lights
					- often used between networking devices (servers, routers, switch)
					- rarely used for computers
					- _full duplex_: cable can be used to send data both ways simultaneously
					- _half duplex_: cable can be used to send data both ways one at a time
					- if there is one core or two cores it can be half or full duplex
					- Single mode fible (SMF)
						+ laser light, >2km, more expensive
						+ from ISP to your building
					- Multi Mode Fible (MMF)
						+ LED, ~500m, cheaper
						+ within the same building
					- _maximum bend radius_: how much you can bend the cable
						before the signal gets degraded
					- LC and RC connectors
- Wireless / Wifi (not ethernet but similar)
	+ uses access points (the box) ~ switch for wireless networks
	+ access points can be wired in the network
	+ uses 802.11 standard
- Network address
	* each device on a network has:
		- at least one MAC address
			+ one MAC address per network card
			+ this MAC address cannot be changed and is unique
			+ used solely in LAN segments
		- an IP address
			+ chosen by the network administrators and can be changed
			+ can be used in LAN but also wider network
		- multiple ports
			+ addresses within a device for multiple applications
- __OSI model__ has 7 layers
	+ memo tech: Please Do not Throw Sausage Pizza Away
	+ Upper layers: 5-6-7: work large pieces of informations, can be considered
		a single _software layer_
	+ Lower layers: 1-2-3-4: how to bring the information (address, packets ...)
		* broken in chunks of data
		* chunks can be sent by multiple devices (multiplexing)
	1. Physical
		- manages physical network components
		- encoding data into physical signals
		- which wires is used?
	2. Data Link
		- creates a logical link between devices
		- data is likely to go through multiple devices
		- the local address (MAC) will change but not the network address
		- ethernet
	3. Network
		- adds network addresses
		- adds routing
	4. Transport
		- how do we transport data between endpoints: UDP/TCP
		- port numbers (application address)
	5. Session
		- building a session between applications
		- request to remote services, SCP...
		- PAP (authentication), RPC, conferencing for synchronization
	6. Presentation
		- format that can be understood
		- data conversion and formatting between communicating devices
		- file types, image/video/text, character encoding
	7. Application
		- communication between applications
		- for example between websites
		- FTP, HTTP, SSH ...
	- protocols don't follow the OSI model exactly
		+ for example HTTP can be considered 5-6-7
			* app data, character encoding, no session
- __IPV4__
	+ 4 bytes separated by dots
	+ two addresses in one
		1. host address
		2. network address
	+ classless networks and subnets (starting from 1993)
		- you always get a 4 bytes __address__
		- and a 4 bytes __mask__
			+ mask always starts with a serie of 1 followed by a serie of 0
			+ it describes which part of the address is for the network
				and which is for the host
			+ CIDR notation for network 172.16.0.0/24
				* mask is composed of 24 bits of value 1
	+ reserved addresses
		- broadcast IP: last IP in a network
			* example: 172.16.2.255/24
			* it can also be: 255.255.255.255, you don't need to know the network address
			* routers never broadcast messages
		- network address: when the host bits are set to 0
	+ public and private addresses
		- some IP spaces are reserved for private use
			* 10.0.0.0/8
			* 172.16.0.0/12
			* 192.168.0.0/16
	+ _multicast traffic_
	+ each network should have a _default gateway_
- sysctl and sysctl.conf
  + change kernel parameters at runtime
  + files where kernel parameters aer stored
- __TCP/IP model__
	+ this model is more popular among used protocols
	+ Developed by US DOD, that's why it is more practical
	+ TCP/UDP, Ethernet use this model
	+ all protocols using this model are described in Request For Comments (RFC)
	+ the application/presentation/session of OSI is gathered into one application layer
	+ split into two
		* Application and processes upper part (Application + Transport layer)
			- application layer create processes that listen to ports
			- transport layer handles communication from ports to processes
		* Data Transfer lower part
			- network layer bring data from host to another host through the internet
			- data link and physical layers to brind data from a device to another device
	1. application, describes how the application uses the network
		+ example HTTP: how web browsers communicate to servers
	2. transport, creates and maintain conversations between applications
	3. network, make sure that data from one host finds its way to another host
	4. data link, is responsible for delivering data within a subnet
	5. physical, physically transmitting and receiving data: optical, electrical, radio
- __TCP and UDP__
	+ they get data between applications
	+ both have headers and port numbers
		* well-known ports: 0-1023
		* random ports: 1024-65535
		* ports are useful for multiplexing, allowing multiple apps to access
			the network at the same time
		* sockets are used by network apps and are composed of
			+ local ip
			+ local port
			+ protocol used
		* any conversation can be differentiated by the 5-tuple
			+ local ip
			+ remote ip
			+ local port
			+ remote port
			+ protocol
	+ __TCP__
		- connection oriented, build and track a connection between hosts
			+ _3-way handshake_ to initiate a connection
				1. SYN flag in the header (synchronize)
				2. SYN/ACK flags are up (ack for acknowledge)
				3. ACK
			+ to close a connection the right way
				1. FIN/ACK
				2. ACK
				3. FIN/ACK
				4. ACK
			+ to close a connection abruptly, happens when there is an error
				1. RST
		- error recovery
			+ ask to resend packets since a sequence number with ACK <start_nb>
			+ ask to resend a specific pack with SACK (selective ACK)
		- windowing, maximum amount of data sent before requiring an ACK
			from the other party
			+ may be resize depending of the quality of a connexion
			+ window size can be bigger when really few segments are drop
		- ordered data delivery
	+ __UDP__
		- lightweight
		- connectionless
		- does not care about errors
- __switching__
		- mac addresses are unique
				xx:xx:xx:yy:yy:yy
				+ x for vendor specific
				+ y abitrarily given to each device
			- broadcast address
				ff:ff:ff:ff:ff:ff
			- multicast addresses
		- __Ethernet__
			- header: preamble, destination, src, type, data (IPV4/6)
				+ preamble is a fixed pattern to know the beginning of a frame
			- data
			- trailer: checksum
		- __hub__
			+ layer 1 device
			+ all devices in collision domain
			+ half duplex
			+ data replicated to all ports
			+ limited growth
		- __bridge__
			+ small collision domains
			+ less traffic flooding
			+ more efficient
			+ scalable
			+ learn which mac address is on which interface
			+ flooding, learning, forwarding, filtering, aging (table entries
				are removed past a certain time)
		- __switch__
			+ combine good points of hub and bridge
			+ full duplex
			+ forwarding scheme
				+ store and forward scheme
					* buffer frames until the whole message has arrived
						before forwarding
				+ cut through
					* forward all frames
				+ fragment free
					* buffer a fixed amount of frames before forwarding

- network bash commands
	- __ip__ command
		+ in charge of networking, routing, tunnels
		+ ip address,  for networking
			+ can fix ip address for a network adapter
		+ ip route
			+ show possible routes for networking
			+ can setup a default gateway
			+ established automatically by arp
		+ ip neigh
			+ to show neighbours in the network
		+ ip link, for network adapter
	- __iptables__
		+ firewall, router, are involved when forwarding from a network adapter to another
		+ possess multiple tables such as nat, filter or mangle
		+ possess multiple chains such as INPUT, OUTPUT, PREROUTING, POSTROUTING, FORWARD
			+ chains corresponds to a position in the packet treatment by iptable
			+ chains are composed of rules that can be modified
	- __dig/nslookup__
		+ query a dns server about an adress/ip
	- __telnet__
		+ establish a tcp connection
	- __nc__
		+ netcat, part of the nmap project
		+ nc -u, for udp
		+ nc -t, for tcp
		+ nc -l, to bind a socket to listen for udp and tcp
		+ nc -e, executes a command
			* nc -e /bin/bash for a reverse shell
		+ -z just report the status
	-  __ss__
		+ socket statistics
		+ sockets are used for tcp connection for example
	- __nmap__
		+ network mapper
		+ option -s for scan,
		+ -sV for scan of services version
		+ -sT TCP, -sU IDP, -sO IP scan
		+ -sn or -sP for no port scan
		+ -sS stealth scan
		+ -A, all: os, service version, trace route ...
		+ -T1 to -T5, speed/stealthyness
		+ can also run scripts
			* --scripts=ssl-enum-version for example check cyphers supported by target

# Python3
+ dir() function inspect the attributes of an object
+ module
    * PYTHONPATH environment variable where python interpreter looks for its modules
    * sys.path  list of locations where the interpreter search functions
        - first element is location where the interpreter was summoned
        - equal to PYTHONPATH but loaded dynamically
    * sys.path.insert(nb, dir_location) with nb the index where dir_location
    will be place in sys.path
        - avoid using 0, because it should be reserved for current file
    * sys.path.append(dir_location) append at the end of sys.path
    * import toto, import the file toto.py found in sys.path
    * from toto import *, import the file toto.py found in sys.path and the
     namespace does not need to be mentioned
    * from toto import jo
        - toto.py exists in sys.path then jo should an existing identifier of toto.py
        - toto is a directory with a jo.py file in it
+ named tuples
    * immutable tuples indexed by string with class syntax
    * Foo = namedTuple("Foo", ["field1", "field2"]
    * newFoo = Foo(att1, att2)
+ file op
    * os.path.join("path", "to", "target")
    * os.path.isfile(file)
    * open(filepath, "r/w/r+/a")
    * file.close()
+ iterator
  * _iterable_
    - list, tuple, string
    - has an iter() method that return an iterator
  * _iterator_ are obtained from iterable
    - simple python objects with 2 methods
    - __iter__()
    - __next__()
      + return next element of iterator
      + or raise StopIteration
    - for loop are iterators which are nexted until they raise StopIteration
  * generator
    - function returning an iterator that can only be looped once
    - define a function with __yield__ instead of __return__
    - when calling the generator
      + the function executes until it hits a yield
      + the execution is paused and the closure is returned
      + later we can next() our iterator to restart the execution until next yield
    - basically it's a a sequential closure
+ decorator
  > @log_fct                def action():
  > def action():      =>     do_action()
  >   do_action()           action = log_fct(action)
  * can be seen as macros, change the program code itself
  * used when you want to add extra generic code to functions
    - logs, when you want all your function to create logs you can use a logging decorator
    - user authentification, when function are accessible only when connected
    you can decorate your functions with a function checking authentification
  * an issue is that it may be hard for debugging
    - in the example above, when calling action() the __name__ attribute will be log_fct
    - to deal with this you can use @wraps
        + @wraps is a decorator itself that is in charge of
        copying function name, docstring, arguments...
- under the hood
  + python immutable data have a single instance in the memory
    * all variables containing the int 5 points to the same memory location storing 5
  + python list just contain pointers to other memory locations
  + everything is object in python
    + class are objects
    + int are objects
    + every python object has a python header where the type, size and value is stored
  + metaclass
    + class are instances of metaclass
    + type() is a metaclass and also a function to generate new class
    + type(name, superclass, attributes) creates a new class
    + type is an instance of itself, this is possible due to C++ implem under the hood
  + only objects with the attribute __dict__ can be modified to hold more attributes
  + the function setattr can be used to add attributes to any objects
# C
- tips
    + pointer addition, be careful
    > struct toto *p = malloc(sizeof(struct toto));
    > p + 4 => p + 4*sizeof(struct toto)
    + "foo" "bar" => "foobar"
    + function pointer
        * typedef int (*func_type)(int, int);
        * func_type coco(int i) { ...
        * int (*coco(int i)) (int, int) { ...
- c macro
    + include guards
        * #ifndef TOTO
        * #define TOTO
        * #endif
    + with var
        * #define INC(a) ((a)+1)
    + pasting tokens
        * #define BUILD_FIELD(field) my_struct.inner_struct.##field
        * BUILD_FIELD(field1) =>  mystruct.inner_struct.field1
    + string-izing tokens
        * #define PRINT_TOKEN(token) printf(#token "is %d", token)
        * PRINT_TOKEN(foo) => printf("<foo>" " is %d" <foo>) => printf("foo is %d", foo)
    + #define assert(exp, msg) __assert(#exp, #msg, __FILE__, __LINE__)
        > void __assert(char* exp, char* msg, char* file, char* line) {
        >   printf(...);
        > }
- c startup
    + _start()_
    + _libc_start_main_
        * _libc_csu_init_
            - _init_
                + frame_dummy
                + __do_global_ctors_aux__
                    * constructors
        * main
        * exit
- constructor or destructor function in c
    > void before(void) __attribute__((constructor));
    > void after(void) __attribute__((destructor));
    + contructor got their address stored in section init_array
        * always contains the function frame_dummy first
    + destructor got their address stored in section fini_array
- symbol visibility
    + usually we need to share var and func between files of a module and not
    between programs and libs, otherwise provides functions
    + __static__ keyword
        * renders a var/func private
        * however it does not allow sharing between files of a module
    + visibility attribute
        > int my_var __atribute__ ((visibility ("hidden")));
        * default, visible everywhere
        * hidden, does not appear in the dynamic symbol table but left in symbol table
        for static linking purposes
        * protected, visible but cannot be overridden
        * internal, does not appear in any symbol table
    + export list
        * explicitly tells the linker which symbols are to be exported
    + __extern__ keyword
        * usage of extern for variables does not instantiate the variable
        * that's why we use _extern_ in header files since multiple programs might
        include the header, which avoid having multiple definitions
        * for functions _extern_ is added implicitly and just oppose _static_
- Under specification
    - C standard gives freedom to compiler for speed and portability
    - Unspecified behaviour
        + Compilers has to chose and document between multiple choice
        + like order of evaluation in expressions
    - Implementation defined behaviour
        + Nothing is specified but compiler has to documents its choice
        + Endianness or size
    - Undefined behaviour
        + No requirements are imposed, compilers can do anything they want
        + deferencing null, signed integer overflow ...
- libc
  + __fork()__
    * creates a child process
    * return 0 for child process and PID of child for original process
    * the only things that are shared between the processes are:
      - file descriptors
      - msg queue descriptors
      - directory streams
    * the original can use __wait()__ to wait until the child process ends
  + __execve()__
    * executes the program referred by the _pathname_ args
    * the process runs with a new stack, heap, data, code
    * does not return on success
  + __exit()__
    * terminates a process with _status_ exit value
  + __mmap()__
    * map a new area of memory in the virtual memory of the calling process
    * usually used for file mapping
    * we can define the protection level to this memory (read, exec, written ...)
    * we can define the access to this memory to other processes (shared, anonymous)

  + __mprotect()__
    * change protections for a memory pages containing part of the specified addresses range
    * protections are: none, read, write, exec...
  + __system()__
    * executes a shell command
    * it's a combination of _fork()_ and _execl("/bin/sh", "sh", "-c", command, NULL)_

# Concurrent programming
- __Concurrency models__:
	define how threads collaborate to complete a task
- Concurrent vs Distributed
	+ concurrent: different threads collaborate
	+ distributed: different processes collaborate
- Concurrent vs Async
	+ concurrent: works is split among multiple working entity
	+ async: work is done by a single working entity but when
			faced with a blocking operation such as IO, start another
			work until hit by a callback
- Popular concurrency models
	+ OS threads
	+ Event-driven programming
	+ Coroutines
	+ The actor model
- Shared state tools
	+ Blocking mechanisms
		* lock, mutex
	+ Persistent data structures
		* when a thread modifies a data structure, other threads
			keep a reference to the unmodified one
		* usually tend to not perform well due to using non-efficient data structure
	+ __non-blocking concurrency__
		* blocking algorithms block a worker until it can perform its job
		* non-blocking algorithms notify the worker that job cannot be completed
			safely which allow worker to focus on another task
		* examples of non-blocking tools
			- volative variables
			- optimistic locking, compare and swap approach
				+ threads get a copy a copy of the data and can modify it
				+ when writing the modified version in the memory you have to check
					if the original data had been modified meawhile
				+ if it was modified the thread get a new copy of the data
				+ it's called optimistic because it assumes that no thread modified
					the shared memory in the meantime
				+ if the shared memory was modified, you do wasted work
			- non swappable data structures
				+ certain data structure may be too big to copy to threads for modifications
				+ a thread can _share its intended modifications_
				+ if no other thread want to make modifications at the same location
					a lock is installed during the exec
		* pros and cons
			- hard to implement
			+ threads can choose what to do when resources are not available
			+ no deadlocks
			+ no thread suspension (suspending and reactivating threads is costly)

- __Parallel Workers__
	+ one delegator and multiple workers
	+ each worker fulfills its task from top to bottom
	+ Pros and cons:
		+ easy to understand
		+ just add more worker for more parallelization
		- you have to handle shared data which may get complex
		- _stateless workers_, workers must re-read shared state every time it needs it
			to be up to date (can be slow)
		- job execution is nondeterministic
- __Assembly Line/Event Driven Systems__
	+ Resembles an assembly line in a factory
		* each worker handles a part of the full job
		* when finished, workers forward the job to the next worker
	+ _shared nothing concurrency model_, workers do not share state together
	+ usually designed to use non-blocking IO
		* IO operations are slow
		* a worker does as much at is can until it has to start an IO,
			then gives control over the job
		* when the IO finishes, the next worker continues working on the job,
			until it has to start an IO
	+ there can be multiple "virtual" assembly lines that can be mixed
		together as well
	+ also called _event driven_ because workers reacts to event occuring
		in the system
	+ __Actor model__
		* example of event driven models where workers are called _actors_ and
			send message to each other asynchronously
	+ __Channel model__
		* example of event driven models where workers to not communicate
			directly but use channels
		* workers can listen to a channel without the sender being aware of
			who is listening
	+ Pros and cons:
		+ no shared state
		+ stateful workers
		+ better _hardware conformity_, code is actually singlethreaded
			* code naturally benefits from how the underlying hardware
		+ job ordering is possible
			* you can rebuild the state of the system from logs
		- code is splitted among multiple classes in your project (multiple worker)
			* harder to see what code is being executed for a job
			* may arise to a situation of _callback hell_ where it is hard
				to track what the code is doing
- __Functional Parallelism__
	+ functional programing without a global state
	+ all the data is carried over from function to function
	+ function executions feel like atomic operations
	+ each function call are independent from each other which enable easy
		parallelism
	+ Pros and cons
		- functions have to be well organized for it to be more efficient than
			single thread

# Crypto
- Different objectives
	- ideal channel
		+ a channel directly between sender/receiver that no one can access
		+ cryptography tries to emulate this channel
	- __Confidentiality__
		+ minimize the possibility of guessing your message
		+ _Encryption/Decryption_ is used for privacy
			* converts data in unreadable form which can only
				be read by the receiver
			* to encrypt and decrypt sender and receiver must possess a _key_
	- __Authentication__
		+ make sure that a message comes the sender claimed in the message
		+ _Message Authentication Code_ (MAC) is used
			* when sending a message _M_ a tag _t_ is also sent where _t_ is
				computed with _M_ and a key which should be specific to the sender
			* receiver gets _(M,t)_ and can check that decryption of _t_ gives M
	- __Integrity__
		+ ensure that a message has not been latered on the communication path
	- __Non Repudiation__
		+ prevent a party to disclaim being the author of an action
	- __Secure pseudorandom number generator__
- Cryptographic primitives
	+ Hash
	+ __Symmetric encryption__
			*	A and B share a secret key _k_
			* __encryption symmetric key__ -> Privacy
					- send _cypher(M,k)_
			* used for: Confidentiality
			* exemples
				- AES, Blowfish, DES, 3DES
			* recommendation by NIST is AES-256
			* usually used
	+ __Asymmetric encryption__
			* A got a secret key _sk_, B got public key _pk_ (anyone can have _pk_)
			* __encryption, public/secret key__ -> Privacy + Authentication
					- B sends _cypher(M, pk)_, A decrypts with _decrypt(cypher(M, pk), sk)_
			* used for: confidentiality + authentication
			* Certificate
					- usually delivered by a third-party to ensure identity
						of a private key owner
					- certificate authority
						+ Let's Encrypt
						+ IdenTrust
						+ Sectigo
					- X.509 is a certificate format
						+ public key
						+ identity (hostname, organization, individual)
						+ signature of a certificate authority (or self-signed)
						+ address
	* __Digital signature__
		- A sends _(M, cypher(M, sk))_, B checks that _M == decrypt(cypher(M, sk), pk)_
		- Authentication + Non-Repudiation + Integrity
	* __MAC__
		- send _(M, H = hash(M,k))_, B checks that _hash(M,k) == H_
		- used for: Authentication
- Algorithms
	- AES
		- Number of rounds depends on the key length
			+ 10 rounds for 128-bit key
			+ 12 rounds for 192-bit key
			+ 14 rounds for 256-bit key
		- plaintext is divided into blocks of 128 bits
		- the state of the cypher is described by a 4x4 matrix each containing
		a byte (4x4x8 = 128)
		- operations description
			+ RoundKey, 128 bit round key derived from the cipher
			key using Rijndael's key schedule
			+ AddRoundKey, xor with the round key
			+ SubBytes, byte substitution with an S-box (table lookup)
			+ ShiftRows, each row is shifted by an amount equal to the index of the row
			+ MixColumns, columns are multiplied with a fixed matrix
		- Rounds
			+ First round: AddRoundKey
			+ Intermediate rounds
				1. SubBytes
				2. Shiftrows
				3. MixColumns
				4. AddRoundKey
			+ Final round
				1. SubBytes
				2. Shiftrows
				3. AddRoundKey
		+ Sbox provides confusion
		→ each bit of the cipher should depend on several parts of the key
		+ Shiftrows + MixColumns, provides diffusion
		→ a single bit modification to the plaintext should change the cipher greatly
		(half of the cipher bits should change)
- __Electronic voting__
	+ Requirements
		* __End-to-End Encryption__
			- voter intention is protected from beginning to end
			- starting from the voting client to the tally
			- vote privacy post-election: secure anonymization 
		* __Invidual Verifiability__
			- after submitting a vote, voter receives evidence that the 
				vote has been cast and recorded as intended
			- the proof gives the voter high confidence that the vote was not
				manipulated by a compromised voting client
			- detecting compromised vote must be 99,9% or higher
		* __Universal Verifiability__
			- correctness of the election result can be tested by independ verifiers
			- verification includes: 
				+ checks that only votes cast by eligible voters have been tallied 
				+ eligible voters only voted once 
				+ every vote by eligible voter have been tallied as recorded
		* __Distribution of Trust__
			- several independent control components participate
				+ sharing private decryption key 
				+ performing individual anonymization steps 
			- single control components are not fully trusted, but we trust a group 
				+ at least one of them will prevent/detect malicious behaviour
			- prevent single point of failures
- __Public key/Asymmetric schemes__ 
	- glossary:
		+ trapdoor function: one-way function, hard to reverse
	- __RSA__
		+ trapdoor: multiplication of prime numbers 
			* easy to calculate c = msg^(k_public)
			* hard to get msg from c with k_priv
		+ protocol
			1. Pick two prime numbers (a, b)
			2. Set modulo, m = a * b
			3. pick a public key _kp_
			4. Calculate private key _ks_ with a and b 
			5. for any number we have the relation: _(a^kp)^ks mod(m)= a_
	- __Diffie-Helman__
		+ trapdoor: dicrete logarithm problem
	- __Elliptic Curve Cryptography (ECC)__
		+ actually close to RSA but in a different domain
		+ domain is an elliptic curve 
			* instead of natural numbers you have points (x,y) which
				verifies relation _y^2 = x^3 + ax + b_
			* instead of multiplication the . operations refers to line intersection
				- A . B = C
				- A and B and C are points of the elliptic curve
				- drawing a line trhough A and B instersect the curve at point C
		+ protocol for ECC discrete algorithm
			1. pick a prime number m as maximum 
			2. pick an elliptic curve 
			3. pck a public point a on the curve
			4. we get cypher keys with the operation 
				> k_pub = a^k_priv mod(m)
				- we repeat the . operation k_priv time on msg
- __Commitment scheme__
	+ commitment allows one to proove one knows a value
		* the value may be revealed later to verify the commitment
		* or can be kept secret using a zero knowledge proof
	+ 2 phases
		1. commit phase: value is chosen and specified
		2. reveal phase: value is revealed and checked
	+ applications 
		* coin flipping 
			1. A sends a commitment to B to a value tail or head
			2. B flips the coin and reports the result
			3. A reveals what she commited to
			4. A and B compare revelation with actual result
		* Verifiable secret sharing
			- what is it?
				- each party receive a share of a secret value
					+ when enough parties cooperate they can use their share to 
						reconstruct the secret
				- at the root of many protocols for secure computation
			- link with commitment scheme 
				+ to be sure that shares received are correct the distribution 
					of a secret is accompanied by commitments to the shares
				+ each party can check if its share is correct 
	+ Merkle tree 
		* you can check that you have a correct block of data by recomputing
			the merkle tree 
	+ KZG polynomial commitment scheme 
		* vector commitment (you can check commitment among a list of value)
		* you can check that your data is part of a commitment X at index I
- __Zero Knowledge proofs__
	+ Prove that you know something to a party without revealing the knowledge
	+ 2 types: interactive and non-interactive:
		* _interactive_: multiple exchange between parties to do the proof 
			- trust that you know the secret value deepens with the number of exchanges
			- it's like an interrogation, the more questions you ask the more you're 
				confident the other know the secret
			- _Downside_
				+ limited in transferability 
				+ you need to redo everything to transmit the proof for a new party
		* _non-interactive_
			- ex: zk-snarks (zero-knowledge Succing Non-interactive Arguments of Knowledge)
			- well-suited to blockchains since you transmit the proof to many ppl
	+ isssues: 
		- can't prove with 100% certainty that you have the secret 
		- computation hungry
- __Shamir Secret Sharing__
	+ (k, n)- threshold scheme
		* split a secret into n parts
		* you need at least k parts to reconstruct the secret
	+ Based on Lagrange interpolation theorem
		* you need k points to uniquely determine a polynomial of degree k-1
		* we have this polynomial of degree k-1
			> f(x) = a0 + a1x + a2 x^2 + ... + ak-1 x^(k-1)
			- a0 is the secret 
			- shares of the participants are points of the polynomial: (i, f(i))
			- with k shares you can reconstruct the polynomial and get secret a0
- __Threshold signatures scheme__
	+ you want to sign a message with at least t from n share
	+ phases 
		* Key generation 
		* Key distribution 
		* Signature distributed protocol
		* Verification
	+ _Key generation_
		* without leader
		* usually based on Shamir Secret Sharing 
	+ _Key distribution_
		* how do we make sure anyone receives a correct share?
		* 2 ways of distribution: interactive and non-interactive 
			- with interactive there are multiple rounds for parties for 
			verification and complaint resolution
				* you can verify your share against a public value from the generator
			- non-interactive: publicly verifiable public key encription
				* produce a cypher from your share that other parties can validate
	+ _Signature protocol_
		* without leader
		* each party 
	+ _Verification algorithm_
- __elliptic curve pairings__
	+ at the core of threshold signatres, 0-knowledge proofs...
- __Trapdoor function__
	- __Discrete logarithm__
		+ For _a_ and _b_ in group G the discrete logatirhtm _x_ of _a_ to base _b_ is
			the answer to the following problem: _b^x = a_
				* such _x_ may not exist
				* we usually do discrete logarithm in a finite group in order to 
					have a termination condition (there can be at most size_of_group tries)
		+ dicrete logrithm problem 
			* the discrete logarithm is hard to compute given a, b and G (in a large gropu)
			* given b, x and G a is easy to compute
	- __Multiplication over Factorization of prime numbers__
	- __Elliptic Curve Cryptography (ECC)__
		+ public key encryption based on mathematical elliptic curves 
			* finding the discrete logarithm of a random elliptic curve 
				element with respect to a known base point is infeasible
			* Elliptic Curve Discrete Logarithm Problem (ECDLP)
		+ smaller, faster and more efficient crypto keys
			* 256-bit key in ECC gives as much security as 3072-bit key in RSA
			* ~10% less storage space than RS
		+ recommended algorithms based on ECC:
			* __ECDH__: Elliptic Curve Diffie Hellman for key exchange
			* __ECDSA__: Elliptic Curve Digital Signature Algorithm for digital signature
			* __EdDSA__: Edwards-curve Digital Signature Algorithm, based on 
				Schnorr signature and uses twisted Edwards curves
		- visual version:
			* algorithm
				1. on a curve: choose a starting point A
				2. draw the tangent from A
				3. the line will cross the curve once at -C 
				4. reflect the crosspoint across the X-axis from -C to C
				5. repeat from steps 3 (n times) drawing a line from C to A
				6. we call the last produced point E
			* public key: (starting point A, ending point E)
			* private key: number of hops from A to E (value n)
			* it's easy to retrace the path from A to E knowing the number
				of hops required
				- there is an exponentiation trick to find the ending point 
					quickly if you know the number of hops
			* just knowing A and E it is hard/long to find the necessary
				number hops
				- n is very large, ~ 2²⁵⁶
				- it would get too long to compute each one by one and count
		- mathematical 
			* __based on algebraic structure of elliptic curves over finite fields__
				- _algebraic structure_: group, ring, lattice ... 
					+ a nonempty set A 
					+ collection of operations on A with finite arity 
					+ finite set of identities known as axioms
				- _field_: 
					+ set on which add/subs/mutl/div are defined 
						* operations behave as the corresponding on rational and
							real numbers
					+ algebraic structure widely used in algebra 
					+ most known: field of rational, field of real, field of complex
					+ _finite field or Galois field_
						* field with a finite number of elements
						* most common are given by: _n mod p_ with p a prime number
						* order _q_: 
							- number of elements in the finite field 
							- exists only if _q = p^k_ with p a prime and k an integer
							- in field of order _p^k_, adding any element p times result
								in 0
							- the characteristic of the field is p
				- elliptic curves: 
					+ plane agebraic curve
						* (x,y) solutions of _y² = x³ + ax + b_

# Pentest
- Hacking:
  1. Passive recon
  2. Active recon, scanning
  3. Vulnerability exploit
  4. Backdoor
  5. Cover tracks
1. Passive recon
  - whois, information on domain name
  - haveibeenpwned, using mail of dns administrator or targets
  - bluto is tool that does everything
      + DNS: recon, bruteforcer, zone transfer wildcard
      + email and staff enumeration and compromission
      + metadata hharvesting
2. Scanning
  - Mainly nmap to see open ports
3. Exploits
  - Metasploit is a real nice framework that contain scripts and payload for
  exploits
4. Backdoor
  -
- Metasploit
  + framework for pentest
  + Armitage is gui of metasploit
  + 6 types of modules:
    * exploit, take advantage of a system vulnerability and install a payload
    * payload, code that will give you access to the system (reverse-shell or meterpreter)
      - singles, one action and quite small (keylogging)
      - stagers, create communcation betweeen atk and target
      - stages, large payload give good control on targets
    * auxiliary, give unique type of attacks
      - scanners, ddos, sniffer, spoof...
    * encoders, used to reencode payload and exploit to get past security systems
      - allow to escape: intrusion detection, antivirus
    * nops
      - used to exploit buffer overflow
      - multiple type of nops depending on architectures, size of nops ...
    * post
      - after the system has been exploited
      - allows you to leave backdoors, keyloggers, spying on webcam...
  + main commands:
    * help
    * use, to load a module
    * when module is loaded
      - info, generic info on the module
      - show args, specific information on _args_ of the module
        + targets, options, payloads ...
      - set value for options
        + _set rhost 192.168.56.2_
      - exploit/run to start the module
    * back to unload a module
    * search to look for a module
      - with options
      - examples, _search type:exploit platform:linux_
    * setg, like set but to set option globally
- Memory resident attacker technique
	+ __Shellcode injection__
		1. open a target process 
		2. allocated memory in the process 
		3. write shellcode payload to the allocated memeory
		4. create a thread in the process to execute the shellcode
	+ __Reflective dll injection__
		* create a DLL that maps itself into memory instead of using Windows loader
		* same as shellcode injection but the shellcode is replaced with 
			a self-mapping dll
	+ __Memory module__
		* module which maps a target DLL into memory from a memory buffer 
			rather than a file
		* reimplements the LoadLibrary function
		* can be used to map a malware into memory
	+ __Process hollowing__
		1. create a suspended process 
		2. unmap/hollow the original exec from the process 
		3. allocate and write new payload to the process 
		4. redirect execution of the original thread to new payload
		5. resume thread execution
	+ __Module overwriting__
		* techniques above execute non-image backed code are therefore easy to detect 
		* map an unused module into a process and overwrite the module
			with its own payload
	+ __Gargoyle__
		* evade detection by storing malware in non-executable memory 
		1. put code in read-only page protections 
		2. periodically wakes using an async procedure call 
		3. executes a ROP chain to mark the payload as executable 
		4. jump to it and execute the payload
		5. mark the payload as read-only again 
		6. go back to sleep


# Malwares
- Meltdown
  + use CPU race condition between privilege check and instrucion execution
  + a single malicious process can read part of virtual memory which should
  require privilege (usually kernel)
  + attack
    1. Setup an array _dummy_ where value _v_ we want to access
    will correspond to an index of the array
    2. attacker tries to read _v_ at an address  which is not part of its virtual memory
    3. while the privilege checking is under process
      * read the value _v_
      * store a value at _dummy[v]_
    4. when privilege checking fails, everything is turned back except the cache
    5. get the value of _v_ using timing attacks and looping through
    the array _dummy_
  + protection is easy, unmap kernel memory in the virtual space in user mode
- Spectre
  + use speculative execution to execute unallowed instructions
    * for example bypassing bound checking condition and reading out of bounds value
  + a malicious process need to setup the attacker in order to make the
  target leak secret values
  + attack In the case of an array bound check,
    1. Setup the cache in order to control cache miss or cache hit in the way you want
    2. target program hit the array bound check,
    control the memory so that there is a cache miss for address _a_
    3. While it fetch the value, speculative execution
        * will read the value at _a_ which is out of the array bounds
        * use this value for an index of another array _dummy_ for example
    4. when privilege checking fails, everything is turned back except the cache
    5. get the value using timing attacks and looping through the array _dummy_
  + spectre has a variant using indirect speculation execution
    * when cpu tries to guess which function will be called
    * extremely current with virtual table (function pointers with polymorphism)
    * manage to execute code which should not be accessible
  + real solution are hard to find, only mitigations are found
    * analysis to see sequence of instructions that can be exploited
- Difference Spectre vs Meltdown
  + both use speculative execution or race condition
  + meltdown can read kernel memory, whereas spectre make the process leaks its memory
  + spectre will haunt us and be hard to patch

# Corrélation d'alertes
- deux méthodologies
  + _implicite_, détecte des comportements différrents de ceux attendues
    * le comportement standard est généré par apprentissage
    + peut détecter de nouvelles attaques
  + _explicite_, création de scénarios en corrélant les différentes alertes
    * moins de faux positifs
    * nécessité de mettre à jour une BDD de signatures

- trois objectifs
  1. Réduction du volume d'information
  2. Aug
# Compilation
+ lexer/parser
  - langages réguliers peuvent être décrits par
    * expressions régulières
    * un alphabet avec les trois opérations rationelles
      + union, (concaténation) _a.b_
      + produit (dijonction) _a|b_
      + étoile de kleen _a*_
    * un automate à état fini
  - pour faire un lexer
    * définir les termes du language
      + entier -> [0-9]*
      + if -> i.f
    * décrire un automate non déterministique lettre par lettre correspondant
    (plus intuitif à créer)
    * transformer le ndfa en dfa pour avoir une implémentation du lexer
  - parser
    * il n'est pas possible de reconnaître les expressions bien parentésées avec
    un automate fini (voir lemme de l'étoile)
      + un automate fini ne peut reconnaître un language de la forme _a^n.b^n_
    * on ajoute la récursivité qui est non supportée par les expressions rationnelles
    * obtention de grammaires non-contextuelles
      + une grammaire non-contextuelle ou context free est composée de:
        + symboles non-terminaux
        + symboles terminaux
        + d'un axiome, usuellement nommé _S_, il correspond au symbole remplacé dans les
        règles de production
        + d'un ensemble fini de règles de productions/dérivations
            > S → aSb
            > S → ε
            * cette grammaire p-ê utilisé pour avoir des expressions bien parenthésées
      + une grammaire contextuelle peut avoir un contexte associée à une règle de production
        > c + S → c + aSb
        > S → ε
        - un exemple de grammaire contextuelle ou c fait partie du contexte
        - dans une context-free grammar les règles e production contiennent seulement l'axiome
        _S_ dans la partie gauche
    * top-down and bottom-up parsing
      + top-down parsing
        - consider the input word as the axiom S and then
        use the production rules until getting only terminal symbols
        - simple to produce
        - cannot be used as it is if there are left-recursive production rules
          > S → Sa
          * these rules can be rewritten to remove left recursivity
        - cannot handle ambiguous grammars without backtracking
        - use leftmost derivation (use rules on leftmost S) (because we read from left-to-right)
      + bottom-up parsing
        - start from the leaves and uses the production rules backwards (reduction) to obtain
        a single axiom symbol S
        - more powerful than top-down parsers
        - more complicated to produce
        - are rightmost derivation
        - use shift-reduce
          * either shift: add a new symbol to the parse stack (left to right)
          * either reduce: reduce the top of the stack in conjunction with the lookahead
          symbol (next symbol)
          * the parser next action is determined by top of the parse stack
          and the lookahead symbol
          * better than backtracking since it scales linearly with the lenght of the input
    * ll and lr stands for
      - ll: Left-to-right, Leftmost derivation
          * top-down parsers for a subset of context-free languages
          * ll(k) parsers use k tokens of lookahead
          * ll(k) grammar can be parsed by ll(k)
          * ll(1) parser are easy to create
      - lr: Left-to-right, Rightmost derivation
          * lr uses a variant of shift-reduce
          * rather than using only the top of the parsing stack it uses the whole stack
          * use 4 actions: error, shift, reduce, stop
          * LR parser next action is  determined

+ optimizations
    - Dead store elimination
        * eliminate store which are not read afterwards
    - Code motion
        * Move independent code for better cache miss, better path prediciton or lcoality
    - Common subexpression elimination
        * If an expression is used multiple times, store the expr result in a temporary var and
        replace next occurences with the temp var
    - Constant propagation
        * replace var by its value
    - Strength reduction
        * replace classic arithmetic with bitwise op, reduce processor usage and time
+ compilation properties
    - __correctness__
        * preserves the behaviour from the source language to the target language
        * if the source program has a semantic at source level, this semantic will be respected when compiled
    - __full abstraction__
        * having a mapping of any behaviour between target and source program, involves context
        * no low level attackers can do more harm to a compiled program than a high level attacker to the source program
        * _limitations_
            1. cannot be applied to unsafe languages
                - undefined behaviours are nondeterministic hence we cannot preserve this nondeterminism in the target program
                - removing it from the source language will reduce performance which is the reason of using unsafe languages
            2. attacker model is too open
                - context should be compatible with the protected program
                - should respect privilege of the compromised program
            3. makes an assumption on which part of the program is trusted or untrusted
                - how do you know which components will be compromised
        * correctness is a form of full abstraction, you cannot have a model with full abstraction of every criteria, else it is the concrete implementation
    - __secure compartmentalizing compilation__
        * separate a program in multiple components
        * __fully defined__, a component C is fully defined in respect to interfaces Bis if
        for any components Bs respecting interface Bis, C behaviour cannot go undefined/wrong
        1. for every partitioning between compromised and uncompromised component Cs are fully defined in respect to Bis
        2. If the a variant of uncompromised components Cs and Ds show distinguishable behaviours at _low level_ for compromised components Bs,
        then there exists compromised components As which see distinguishable behaviours between Cs and Ds at _high level_
+ JIT (Just In Time)
    - example of SpiderMonkey (Mozilla JS engine), incremental compiler
        1. Parser and lexer to AST
        2. Interpretation of the bytecode, gather info for further compilation
        3. Baseline compilation from bytecode, which has balance between compile-time and code efficiency, can be executed and gather info during exec
        4. Ion compilation, fully optimized code from MIR code (extension of SSA), long compile time, can be executed
    - allows to have both benefits of interpreter and compilers
    - But still have a "warmup", only code executed multple times gets to be optimized
+ AOT (Ahead of Time Compilation)
    - like JIT but leaves the first steps to standard compilation
    - transform source code into an intermediate representation before the compilation
    - interpret and recompile the IR during runtime
    - necessary for asm.js which uses a subset of JS to have close to native code perf
    - issue compared to JIT or interpreter that it compiles and optimizaes all the code even dead or cold code
+ Linking
    - Multiple symbols resolution
        * Symbol interposition
            + with dynamic linking, the linker will chose the first definition that it finds,
            + depends on linking order
            + ex: libc.so is generally linked last
            + link order can be seen with __ldd__
        * Weak objects
            + for static linking
            + if two symbols are found and one is marked as __weak__, then it choses
            the other one
            + when using nm, weak symbols are tagged _w_ or _W_
    - Shared libs
        * are loaded at runtime before anything else
        * we need to specify the location of shared lib at compile time
        to check that all necessary symbols are defined
        * use LD_LIBRARY_PATH to find the shared libs at runtime
        * else use rpath/runpath
            + rpath is searched __before__ LD_LIBRARY_PATH
            + runpath is searched __after__ LD_LIBRARY_PATH
            + store the library location info in _.dynamic_ section of the elf file
        * when an exec is ran with escated privilege (setuid, setgid...)
            + search path is altered and not using LD_LIBRARY_PATH anymore
            + this is to avoid overriding systems shared lib
        * __stripped shared libs__, version where additional information for debugging
        have been removed, such as
            + symbol tables which tells where variables or internal functions
            are located
            + uneeded static symbols
            + can be used to reduce visibility to internal functions or vars
            + can affect both symbol tables (static and dynamic)

# Static analysys
- Slicing
    * Computation of a subset of a program which gathers every instructions under a criterion
    * exemples of criterion: instructions that affects the value of x, syscall op ...
# Information-flow
- Non-Interference
  + 2-safety property
  + can be approximated into a 1-safety property
  + Two executions with the same public inputs will result with
      the same public output
  - Comparing different strategies
    + Naive
      * skip the execution of code/branch dependent on secrets
      * this strategy may reveal private data due to intricate implicit flows
    + Non-Sensitive Upgrade
      * halt executions when updating public value in secret context
      * The issue here is that we get stuck
    + Permissive-Upgrade
      * rather that stopping the execution say the public value updated in
          private context has an unknown value
      * accepts more program than NSU
      * you still get stuck if an unknown value is reused
    + Secure Multi-Execution
      * execute program once for each security level
      * use true value for input of the security level and
          default value for other inputs
    + Faceted values
      * do a single execution but simulate multi-execution
      * allows to avoid redundancy of SME
- Declassification
  + NI is often too strict, we need to leak some bits of information
  + therefore we allow the confidentiality of some data to be downgraded
  + when using declassification, NI is lost
  + __robust declassification__
    * active attacker which can inject code is no more powerful
    than a passive attacker who observe the result
    * declassification can only be performed by code which was not
    influenced by untrusted data
    * hence confidentiality rests on integrity
  + __Endorsement__ is the dual of declassification for integrity
    * treating information as more trustworthy as the info that has influenced it
- Shannon entropy
  - used to determine the change for an attacker to discover a secret
  - determine the quantitative info-flow using Shannon entropy
    * H = Σ - pi log(pi)      (logarithm in base 2)
    * H is maximum with an uniform distribution
    * H is equal 0 if we have an event with a probability of 100%
  - intuitively Shannon entropy represents the necessary number of bits that one
  needs to receive to guess the symbol sent by the source
    * if source always output the same symbol we have H = 0
    * if it's uniform on an alphabet of 2^n symbols then encoding on bits
    is of size _n_ and entropy is then also _n_
  - Leakage/Mutual information of a program is the difference between the
  entropy on a secret before and after observing outputs of the program
    * L = S(π) - S(π|Outputs)
- Rényi's min entropy
  + chance that an attacker guess the secret at the first try
  + if _v_ is the secret with the highest probability then min entropy is
    * H∞ = -log(v)

# PL
- Abstract Data Types  (exists α. A)
    + Subset of a type A
        + ex:
        + Integers which are pairs
        + Integers which are used as identifiers (must be lower than identifiers counter)
    + Can be composed of a generator for the ADT and a checker for the ADT
- Algeabric Data types
		+ two classes of algeabric data types
			1. Sum types: tuples, records 
			2. Product types: enum, disjoint unions
- Generalized Algeabric Data Types (GADT)
    + You differentiate subtypes in you Algeabric DT
    + Useful if you want a function which returns a type depending
    on the type of a  param
    + For example evaluating an expression wchich have either bool or nat

# Type system
- Types are used to do verifications on programs
- Different type systems ensures different properties
    + For example Java types ensure that java programs cannot go wrong
    + Dependent types and stronger can be used to prove functional correctness of a program
- Syntactic type soundness
    + type system which rules are based on the syntax of the language
    + you give types to components of the language depending on the syntax
    + if the type system is sound you usually get "program does not get 
			stuck/ does not go wrong"
        * Proved  with the two principles with an operational semantic
					1. Progress: if  the state is wf then either it's final or it can make 
						 another step
					2. Preservation: a step from a wf state gives another wf state
    + However such type systems does not guarantees data abstraction and
        safe encapsulation of unsafe features
        * data types internal states could be corrupt by other safe components,
            this cannot be detected
        * safe encapsulation of unsafe features
            - unsafe features like for Rust or IO for Haskell
            - Ideally only libraries should use unsafe features and through a sound API
                it should not corrupt a core program using only safe types
            - It is not the case
- Semantic type soundness
    + Cover the caveat of syntactic type soundness
    + More general that syntactic type soundness
    + Instead of "e is of type A" and prove safety you want "e behaves safely when used as type A"
    + This applies to libraries that you need to behaves safely under the API restrictions
    + Soundness is derives from
        * Adequacy: if "e is a closed expression" and is syntactically well typed then it is
            safe to execute
        * Compatibility: semantic type rules are compatible with syntactic type rules
            - syntactic type rules should still be true if we take the semantic meaning
    + Now Rust libraries should prove semantic type soundness to show that they
        encapsulates unsafe features safely
    + Iris framework built over Coq can be used for semantic type soundness
        - was used for Rust, Rustbelt
        - Haskell, weak memory models, compiler correctness
        - HO Hoare logic including separation logic
- __Curry-Howard correspondence__
  + Correspondence between simply typed λ-calculus and intuitionist logic
      * type           - proposition
      * terms          - demonstration
      * term reduction - coupures (like A->B and B->C becomes A->C)
      * type reduction - deduction rules
  
# Side-channel
- power analysis
  * _Simple Power Analysis_
    attack which allows one to guess the info flow of an execution
    - directly get a power consumption trace of an execution
    - branch and special operands may have peculiar power consumption signature
    - it is possible to deduce the sequence of instructions that was
    executed and retrieve secrets
    - Defense
      + branchless
      + hard-wire hardware implementation of crypto algorithms have small variations
      of power consumptions
  * _Differential Power Analysis_
    statistical attack allowing one to guess secret values by analysing the correlation
    power consumption and public outputs
    - these are generally weak and can be confused with noise
    - algorithm
      + we try to differentiate the instances where a secret bit is at 1 or 0
      + the issue is that the noise is too big to make the difference as it is
      + our goal is to accentuate the differences and remove noise
      1. we run a crypto operation _n_ times
      2. among the _n_ times we make a guess on which instances
      were at 0 and which were at 1
      3. we make an average power consumption trace for both sets of traces
      4. we calculate the differential trace between both average (minus op)
      5. if our sorting guess is
        * wrong, the differential trace should be flat
        * right, noise should cancel out and differences appear as a peak
      + Hence we are able to differentiate traces with secret at 1 or 0
    - Defense
      + __hiding__
        By decreasing the signal to noise ratio (SNR)
        + reduce signal size
          * balancing hamming weight
            - if the sample rate is good we can differentiate between
            actual operation and balancing one
          * choosing less costly operation
          * shielding the device
          * however DPA can still be used if we have an ∞ observations
        + introduce noise
          * randomize power consumption by changing exec order or generating noise directly
          * there are hardware with constant process that consume power randomly (noise)
      + __masking__
        randomize intermediate values of crypto computations to avoid dependencies
        between them and power consumption
        * applied on an algorithmic level and is architecture independent
        * transform every secret variable _x_ into n shares
        * t_0, ..., t_(n-1) are random values
        * t_n is chosen such that t_0 * ... * t_n = x, with * a specific law
        * make DPA exponentially harder in n
        * afterwards computations are made independently on each share
        * security properties
          - t-threshold probing model
            + t+1 shares
            + secure if any set of t intermediate variables is independent from the secret
            + sufficient for software model
            - vulnerable to hardware glitches
              * dependency between variables due to hardware
              * info does not propagate simultaneously between wires
              of a calculation
          - Threshold Implementations
            * _correctness_, sum of output shares is equal to applying
            the function without masking
            * _incompleteness_, each output share is computed by at most t input share
            * _uniformity_, output sharing must be uniformly distributed if the input sharing is
  *  _Correlation Power Analysis_
    + Similar to DPA
    + however the attacker knows the leakage model of the chip/architecture targeted
    + from an hypothetic key the attacker can derive the hypothetical power consumption of
    the computations at multiple moments
    + compared to DPA which compare various entry at a  single point, CPA compare various
    entry with multiple power values during the computations
    + enables a greater precision

# Formal verification
+ __sémantique opérationelle__
    * on s'intéresse au fonctionnement de la machine concrète et abstraite pour chaque pas d'execution
    * pour chaque pas on vérifie que l'observable qu'on a choisit correspond entre le modèle abstrait et concret
    * on est concentré sur le fonctionnement de la machine
+ sémantique par trace, raffinement
    * on ne s'intéresse pas aux fonctionnement de la machine
    * on vérifie que la machine concrète produit un ensemble d'observables contenues par l'ensemble produit par la machine abstraite
    * et cela pour chaque exécution autorisée
    * la machine est plus une boîte noire
    * plus utilisé lorsque qu'on part d'une machine abstraite avec les observables nécessaires à nos propriétés
    * méthode B
+ présentation de Serguei, HOCore par rapport au λ-calcul
    - programmation distribuée, 2 types de modèles calculs existent
        1. Calcul de processus avec envoi de messages
        2. Modèle avec une mémoire partagée
    - HOcore est un modèle de calcul minimal pour des processus avec envoi de messages
    - HOcore utilise une machine abstraite de Keviny? pour évaluer ses termes
    - cette machine abstraite est équivalente opérationellement au λ-calcul évalué avec call-by-name (prouvé en coq)
        * les réductions se font aux mêmes endroits en passant d'un modèle à l'autre
        * le passsage d'un modèle à l'autre est "correct" opérationellement
    - on veut plus que l'équivalence opérationelle on veut __full abstraction__ entre les deux
        * full abstraction entre les deux modèles de calculs
            + prendre une définition d'équivalence entre 2 termes de λ-calcul
            + prendre une définition d'équivalence entre 2 termes de HOcore
            + montrer que ses deux définitions d'équivalence sont traduites lors du passage d'un modèle à l'autre
            + cad si deux termes sont équivalents dans un modèle alors leurs traduction sont également équivalentes dans
            l'autre modèle
        * preuve de full abstraction pour une équivalence de terminaison en λ-calcul
            + équivalence entre deux termes en forme normalisée
        * preuve de full abstraction pour une équivalence applicative en λ-calcul
            + équivalence sur l'évaluation de deux termes
+ transition system
    * a pair (S, →), with S a set of states, and → a set of transition between states
    * it can be represented by a directed graph
    * _labeled transition system_
        - CompCert uses a _labelled transition system_
        - a tuple (S, ∧, →), S is a set of states
                                ∧ is a set of labels
                                → is a set of transition between labels
        - (p, a, q) ∈ →, means there is a transition from state p to state q with label a
        - examples of labels used can be expected input to trigger the transition
        - in CompCert, a labeled transition system is used to describe the operational semantics
        in small-step style. The labels correspond to the trace produced by a step.
+ simulation
    * bisimulation _S1 = S2_, is an equivalence between two state transition systems
			- prove that for every state in S1 there is a state in S2 such that
				it can perform same transition labelled α and inversely
			- usually we want to prove that 2 programs are contextually equivalent
				→ they have same observable behaviour under any context
			- the drawback is we ahve to prove that ∀ context which may be tough
			- proof techniques are developed to prove contextual equivalence
				+ extensional equivalence
					* programs are considered as black box and you show that
						given an input they have the same outputs
					* reduce the forall contexts to forall inputs
				+ intensional equivalence
					* you compare internal structure of the two programs
					* for example you can compare normal form
					→ you take the body of lamda abstraction which allows you to 
						remove the ∀
			- we usually want to have
				+ soundness
				+ completeness (not always true)
    * forward simulation _S1 ⊆ S2_, all the states of S1 relates to a state of S2
    * backward simulation _S2 ⊆ S1_, all the states of S2 relates to a state of S2
    * For compilation
			- you usually want backward simulation so Behaviour(P2) ⊆ Behaviour(P1)
			- however backward simulation is harder since your _compile_ function
				goes from P1 to P2
			- hence you make a forward simulation and gets Beh(P1) ⊆ Beh(P2) and proves that
				P2 is deterministic then Beh(P2) is a singleton
			- and you get bisimulation
		* different forward simulation styles in small-step
			- lock-step
					+ relation 1-1 between each transition of P1 and P2
					+ if P1 make a step and then P2 also makes one and the states are 
					 	still related
			- Plus
					+ relation 1-n with n ∈ [1,N]
					+ similar to lock-step
			- Star
					+ relation 1-n with n ∈ [0,N]
					+ like Plus but is symmetric, P2 can make 0 step while P1 reduces
					+ Stuttering issue, needs to prove that P1 do not "stutter" (loop 
					 	indefinitely)
					+ Prove that either we have Plus relation or we have a measure 
						that decreases strictly when taking a stuttering steps (the 
						computation goes forward)

- Formal security
  + Composed of three models
  + Execution model
    * semantic of a program
    * program behaviour is compositional
      * each computation behaviour is defined in isolation
      * whole program behaviour is composition of sub-components
  + Leakage model
    * to each atomic computation we associate a leakage
    * leakage of a computation may depend on the results of previous computation
  + Attacker model
    * define what the attacker obtain from the leakage of an execution
    * it may be all the leakage but more often it is a subset
    * we call an observation the set of atomic leakage captured by attacker
  + security proofs
    * two ways
    - simulation-based paradigm,
	    one proves that for every observation O, leakage can be computed
			from a small subset on inputs I. Then we prove that inputs I
			are independent from the secrets.
    - information-flow paradigm,
			one proves that there exists a subset of inputs I such that for every
			execution agreeing on I we obtain equivalent leakage. Then we prove
			that inputs I are independent from the secrets.

# Math
+ __algebra__
    - magma, loi interne
    - demi-groupe, associative
    - monoid, possede un element neutre
    - groupe, inversible
    - groupe abélien/commutatif, commutatif
    - anneau, groupe abélien + monoid
    - anneau commutatif, groupe abélien + monoid commutatif
    - corps, groupe abélien + groupe
    - corps commutatif, 2 * groupe abélien
+ __constructivism__
    - it's more or less classical math without the axiom of excluded middle (P ∨ !P)
    - More sound? because you only use instance that you have proved
    - difference proof b contradiction and negation
        + proof by negation is accepted in constructivism
            > Suppose P ... (contradiction) -> !P
        + contradiction is not
            > suppose !P ... (contradiction) -> P
        + because contradiction suppose !!P -> P
        + in contradiction you make the ! disappear
    - intuition
        + excluded middle used deduction which is not observable for constructivsm
        + excluded middle and instances derived rely on an external oracle for certain cases
        + example: classical logic can prove ∃x, P(x), but sometimes it is not possible to calculate such x (notion of oracle)
+ __minimal logic__
  - classical logic > constructive logic > minimal logic
  - classical logic = constructive logic + exclusive tier (or equivalent)
  - constructive logic = minimal logic + ex falso quodlibet ( False ⇒ P )
+ __Backus-Naur form (BNF)__
    * notation technique for context free grammar
    >  bool ::= true | false
    >  nat ::= 0 | S(n) for n in nat
+ Proving that classical logic is coherent from intuitionist logic
  - Done by Godel/Kolmogorov
  - having a traduction from P to [P] and prove
  Classic |- P then Intuitionist |- [P]
  - for simple P, [P] = not not P

+ __Complexity__
  * P ⊆ NP ⊆ PSpace ⊆ ExpTime ⊆ ExpSpace
  * Deterministic and non-deterministic turing machine
    + contrary to common beliefs, non-deterministic and deterministic turing machines
    can simulate each other
    + however non-deterministic can found a solution faster than deterministic machine
  * _P_, problem solvable in polynomial time by a deterministic turing machine
  * _NP_,
    + two equivalent definitinos
      - solvable in polynomial time by a non-deterministic turing maching
      - problem solvable in non polynomial time but verifiable in polynomial time by
      a deterministic turing machine
    + traveling salesman problem to find a path passing through all cities shorter than _k_
  * _NP-hard_, a problem which every problem in NP can be reduced to
  or a problem which is at least as hard as any NP problem
    + traveling salesman problem to find the shortest path that pass through all cities
  * _NP-complete_ => NP AND NP-hard
    + any NP problem can be reduced into it
    + can verify the answer in P time
  * _PSpace_
    + problem solvable by a turing machine in a polynomial amount of space
    + independent of determinism of the machine since the computation ability is the same
    + ex: sokoban game
    + is a 1 player game with polynomial number of positions but can loop infinitely
    between positions
    + can be simulated into a two player games with polynomial number of turn

+ __Calculability__
		* Questions it addresses
			+ What does it mean for a function to be computable?
			+ How can we classify noncomputable functions?
		* Church-Turing thesis 
			+ any function computable by an algorithm is a computable function
			+ any function computable by a Turing machine is a computable function
		* Historic of computability
			1933. Godel and Herbrand defines _general/primitive recursive functions_
				- includes constant functions, projections, successor function
				- closed under composition, recursion and minimization
					+ minimization: finds smallest number that causes a function to return zero
					+ often denoted: μ
				- for a long time mathematicians thought that generic recursive 
					and primitive recursive functions were equal
					* primitive recursive functions do not have minimization
					* Ackermann function is a counterexample (general recursive but not primitive)
						+ A(m,n)
							- n + 1               si m = 0
							- A(m-1,1)            si m > 0 et n = 0
							- A(m-1, A(m,n-1))    si m > 0 et n > 0
						+ croît extrêment vite et cette croissance est utilisée pour prouver
						qu'elle n'appartient pas aux fonctions recursives primitives
			1936. Alonzo Church creates the λ-calculus.
				- λ-calculus is a method to define function s
				- natural numbers can be represented in λ-calculus with Church numerals
				- a function is λ-computable is it can be represented in λ-calculus
			1936. Turing creates a theoretical model for machines: the Turing machine
			1937. Church, Kleene and Turing proved that these 3 models are equivalent
				- __Turing-computable ⇔ λ-computale ⇔ general recursive__
			* Turing complete 
				- models equivalent to turing machines in term of computability
				- combinatory logic
					+ variant of λ-calculus 
					+ replace λ-abstraction with combinators: I, K, S
				- _markov algorithms_: string rewriting systems that uses grammar-like rules
				- register machine: 
					+ closest to our current computers
					+ use multiple unique registers holding integers
				- computaer machine 
				- canonical/normal systems
			* non-computable functions
				- halting problem
				- busy beavers: upper bound on number of symbols printed before halting

+ __Markov chain__
	* stochastic process which possess the markov property 	
		- stochastic process: random process showing the evolution of a random variable
		- markov property: future event is solely based on present state 
			+ does not rely on past events
			+ system does not have memory
+ __Monte-Carlo algorithms__
	* algorithms which terminates in fixed time with high probability of
		finding the correct result 
	* you can repeat the algorithm to increase the success rate
+ __Markov Chain Monte Carlo (MCMC)__
	* to obtain a sampling from a distribution probability
		- you have the sequence of states of a random variable with distribution
			probability 
		- each state is solely obtained from the state just before 
		- you want to generate a sample from these data
	* you get a sample close to the distribution but with randomness
	* application
		- networks: you want to have sample networks that keep the structure
			while being generated randomly
		- biology: molecule distribution / keep structure of molecure / still 
			have randomness
			
		
	
+ __Imaginary numbers__
	* created during 16th century
	* le corps des nombres imaginaires complète le corps des nombres réels pour faire un
	ensemble algébrique clos
	* cad tout polynôme de degré _n_ a au moins _n_ racines
	* très utile dans la résolution d'équations polynomiales
	* en physique permet d'unifier certaines équations qui n'avaient pas de sens dans
	certains cas, (loi d'Ohm pour le courant alternatif, loi de Snell-Descates (angle
	rayon diffracté réfracté, séries de Fourier ...)
+ __Closure__
    * an operator is a closure of a set S if when applied to members of S
    it produces an element of S
    * __transitive closure__
        + the trans closure R+ of a binary relation R on set X, is the smallest relation on X that contains X and is transitive
        + ex:
            * X = set of airports
            * x R y = there is a direct flight from x to y
            * x R+ y = is is possible to fly from x to y
            * R+ contains R because x R y => x R+ y (and is minimal?)
            * R+ is transitive (R is not)
            * R+ is a relation on X
            * Then R+ is the transitive closure of R on X
        + R+ always exists
            * can be built like
            > R+ = U R_i
            > with R_(i+1) = (R) o (R_i) (composition)
+ __Stat/Proba__
  - X random variable of _X_
    * _X_ alphabet
    * X is a variable which value is random over _X_
    * P(X) is the distribution of X over the_X_
        + P(X): _X_ -> [0,1]
        + Σ_(x ∈ _X_) P(X = x) = 1
  - Bayes theorem
    * P(A | B) = P(A ∧ B) / P(B)

- __Graphe__
  - __Regular language__ peuvent être décrits par
    * utilisé pour les lexers
    * expressions régulières
    * un alphabet avec les trois opérations rationelles
      + union, (concaténation) _a.b_
      + produit (dijonction) _a|b_
      + étoile de kleen _a*_
    * un automate à état fini
  - __context-free grammar__
      + plus puissant que les languages réguliers car ajoute la récursivité
      + utilisé dans les parsers (récursivité pour les expression/parenthèses/...)
      + une grammaire non-contextuelle ou context free est composée de:
        + symboles non-terminaux
        + symboles terminaux
        + d'un axiome, usuellement nommé _S_, il correspond au symbole remplacé dans les
        règles de production
        + d'un ensemble fini de règles de productions/dérivations
            > S → aSb
            > S → ε
            * cette grammaire p-ê utilisé pour avoir des expressions bien parenthésées
  - __contextual grammar__
      + une grammaire contextuelle peut avoir un contexte associée à une règle de production
        > c + S → c + aSb
        > S → ε
        - un exemple de grammaire contextuelle ou c fait partie du contexte
        - dans une context-free grammar les règles e production contiennent seulement l'axiome
        _S_ dans la partie gauche
  - Petri net
    + utilisé pour modéliser des réseaux distribués
    + modélisation à événements discrets dynamiques
    + un réseau est composé du triplet Net = (P, T, F)
      * P un ensemble de places
      * T un ensemble de transitions
      * F un ensemble d'arcs
      * c'est un graphe biparti càd les noeuds sont soit des places soit des transitions
    + un __réseau de petri__ est un réseau de la forme PetriNet = (N, M, W)
      * N un réseau
      * M: P → Z, Z un ensemble dénombrable
        - M correspond à un marquage où les places sont attribuées un nombre de ressources
        - M_0 est utilsié pour le marquage initial
      * W: F → Z
        - chaque arc du réseau est donné un poids
    + sémantique d'exécution
      * une transition t est déclenchée dans un marquage M si les il y a assez de ressources
      dans les places s reliés à t par les arcs aux coûts W(s,t)
      + la transition t continue en transmettant à la place output s le coût de l'arc W(t,s)
    + propriétés intéressantes
      * reachability/accessibilité, regarder si un marquage m peut être obtenu à partir
      d'un marquage initial m0
      * liveness, si toutes les transitions du réseau peuvent être déclenché
      * boundedness, if in all its reachable markinks it does not contain more than _k_ tokens

# Algorithmic
- Category of algorithms
  + Divide and Conquer
      * divide the problem into smaller instance of the same problem
      * this dividision is recursive until we obtain an "atomic" problem
      * the atomic problem is solved and we ombine it to obtain a solution
      * for the original problem
      * Usually based on recursion
      * ex: binary search, quick sort...
  + Greedy
      * Problem is divided in multiples steps where we have to make choice
      * The choice is made to obtain the best store at this step
      * Local optimisation of choice
      * Easy to implement
      * Easy to get complexity
      * Hard to be correct or to prove correct
  + Dynamic programming
      * close to divide and conquer
      * the problems is divided in smaller problems
      * smaller problems are solved and memorized
      * these solutions are used to solve other subproblems
      * Solve main problem by combining all these related sub-solutions
      * Ex: Solve 1 + 1
          - you count each 1 you get 2 here
          - How much is 1 + 1 + 1?
          - you use previous result 2 and you only add 1
          - you don't count each 1 again
      * ex: for fibbonaci
          - with pure recursive when you do f(n) = f(n-1) + f(n-2)
          you recalculate f(n-1) and f(n-2) from 0 for BOTH
          - with dynamic programming you'd store result for each f(N) and
          reuse it
      * Floyd-Warshall algithm for shortest path in graphs
- Data Structures
  - Graphs > Trees > Lists
    + Graphs are solely made of edges and nodes/vertices
    + All nodes in trees have a single parent except the root
    + All nodes in lists have a single child
  - __Graphs__
    + Shortest distance
      * Dijkstra path other nodes with no negative weight
          + start from the source and calculate distances to its neighbour nodes
          + mark the node as visited
          + Then continue with the closest unvisited node
      * Bellman-Ford, can process negative weight
          + list all the nodes
          + if we don't know how to reach the node just leave infinity
          + loop through the list until distances does not update
      * Floyd-Warshall, process all shortest path between every nodes
          + make a square grid with nodes as size
          + Fill every case with inf
          + Then loop through all the edges and add their weight in the grid
          + Then triple loops on vertices
              + outer loop, vertice as intermediary node
              + mid loop, source of the path we calculate
              + inner loop, destination of the path we calculate
		+ Topological sort
			* for each directed edge from A to B, A appears before B in the ordering
			* topological sort algorithm can find a topological
				ordering in _O(V+E)_, a sequence of node 
				- a topological ordering is not unique
			* used for program build dependencies 
				- you cannot build a program unless its dependencies are first built
				- a topological ordering will give an order to build the programs
			* a graph with a cycle cannot have a valid ordering
				- only in DAG
				- in trees topological ordering is natural with a breadth first search
			* _Topological Sort Algorithm_
				- should give a topological ordering
					* list of the nodes which respects a partial order
				- algorithm
					1. find an unvisited node
					2. do DFS on only unvisited nodes
					3. on the recursive callback of the DFS add current node 
						the current node to the topological ordering in reverse order
					4.	if nodes haven't been all visited, repeat from 1
  - __Trees__
    + for data that have a key hierarchy
    + sorted linked lists are a particular case of trees (unbalanced tree)
    + search is faster than linked lists, slower than arrays
    + insertion/deletion faster than arrays, slower than linked lists
    + self balancing trees (AVL, Red-black) put an upper bound on search/insertion/deletion
      - AVL and red-black heigh-balancing (max height of subtrees)
      - There is also weight-balancing trees (size of subtrees)
      - AVL tree
        + Each node has a "weight" between [-1, 1]
          * -1 for longer left subtree
          * 0 for balanced subtrees
          * 1 for longer right subtree
        + AVL when updating make sure that all nodes have their weight between
        the boundaries (it may use rotations)
        + Search average: log(n) worst: log(n)
      - Red-black trees
        + Node are either red or black
        + root is black
        + if a node is red then both children are black
        + every path to leaves contains the same number of black nodes
        + Search average: log(n) worst: log(n)
    + Search trees, left child is lower than parent and right is bigger
    + Heap, root is always bigger than its children
      * often used to implement priority queues (list with elements sorted by priority)
    + Trie (or prefix tree), node hold the values of the path used to reach 
			the node 
      * like compcert id names
      * can be used for autocompletion 
				- you search the trie as user add characters
			* Patricia trie (radix trie, compact prefix tree)
				- node with a single child are merged with their parent
				- for autocompletion you could have a node corresponding to
					a group of characters instead of having one node/one char
		+ Merkle tree
			* leaves have a hash of the data they contain
			* nodes have a hash depending of its children
			* basically the root node is a hash of the whole tree
			* used in blockchain to store the global state and the root node hash
				is used to reach agreement between the nodes of the blockchain
			* specs:
				- construction: O(n)
				- update: O(n)
				- proof size: 
					+ binary merkle tree: O(log2(n))
					+ k-ary merkle tree: O(k logk(n))
		+ Patricia Merkle Trie 
			* used by Eth to store state
			* they have a key/value DB where:
				- key corresponds aggregated hash on the path to a leaf
				- leaf or extension (condensed nodes with single child) nodes contain
					values
		+ Verkle tree 
			* same properties as a Merkle tree but more efficient in proof size
				- binary Merkle tree have O(log2(n)) in proof size 
				- this is an issue for a blockchain if you have to send too much 
					proofs, you overwork the network
			* in Verkle tree you exchange reduced proof size with computation 
				complexity
					- good if you want to reduce network usage and have strong 
						computation
			* specs for a k-ary verkle tree: 
				- construction: O(kn)
				- update: O(k logk(n))
				- proof size: O(logk(n))
			* __the wider is your verkle tree the lower are your proof size but
				the construction and update time increase in return__
			* Design: 
				- instead of hashing you regroup inputs into vector of size _k_
				- then you use crypto called _Vector Commitment Scheme_
				- ex: from vector (z1, z2, z3) you produce commitment C
					+ you can produce proof πi from C and value zi 
					+ with πi and C, you can prove that value at index i of C is zi
				- therefore to produce a proof-of-inclusion you require one proof
					for each commitment along the path
  - __Lists__
    * Linear data structures
      + Stack
      + Queue
      + Vectors (based from array but with flexible size)
      + Linked lists
    * Using Linked list over array
      + when we need insertion/deletion in constant time when you insert next to your location
      + you want to insert items in the middle of the list !!!
      + don't really know the size of the list
      + search is not done with random indexes
      + you don't have issues with memory
    * Using arrays
      + O(1) access to any element which index is known
      + really costly to insert or remove element
  - Hash tables
      + Like arrays but with keys limited in size (compared to integers)
      + Risk of collisiion
        * collisions slow down the hash table
        * a bucket of values is associated to each hash instead of a single value
      + Hashing function may slow the process
      + hash tables vs binary search trees
        * hashtables have constant time for search, insert and delete where
          as binary search trees takes O(log(n))
        * search trees are more memory efficient, they do not reserve
          more memory than necessary
        * bst are more efficient for range search and sorted iteration
- Sum of multiple squares or rectangles
    - Example  from grid we want to make sum
      > 1 1 1
      > 1 1 1
      > 1 1 1
    - create a sum grid
        > 1 2 3
        > 2 4 6
        > 3 6 9
        * every cell is the sum of the value of the rectangle from the cell
        up to the top left corner
        * I(x,y) = i(x,y) + I(x-1, y) + I(x, y-1) - I(x-1, y-1)
            + I is the value in the sum grid
            + i is the value in the original grid
    - Calculate a sum in the rectangle of the original grid
        * S(x0, y0, x, y) = I(x,y) + I(x0-1, y0-1) - I(x, y0-1) - I(x0-1, y)
        * example sum of the square of size at the bottom right
            - x0 = 1, y0 = 1, x = 2, y = 2
            - S = 9 + 1 - 3 -3 = 4
- Union Find
  + Data structure to organize data into equivalence classes
  + It is composed of 3 operations
    * MakeSet, create an equivalence class or set with 1 element
    * Union, merge two sets
    * Find, given an element find its set/ equivalence class
  + Each equivalence class, are represented by a root which is an element of the set
    * can be either linked lists  (O(n) for the 3 operations in a row)
    * or trees (O(log(n)) for the 3 ops sequentially and balanced trees)
        - trees can be flatten by setting each node as a child of the root
        during Find operations
        - complexity becomes O(α(n)) with α the reverse of Ackermann
- Minimum spanning graph
  + subtree of T where all vertices are present
  + the minimum when we want the sum of the edges weight have to be as low as possible
  + Used in design of networks, approximate salesman problem,
  image segmentation, cluster analysis...
- __Byzantine generals problem__
	+ Setup:
		* generals are far away from each other
		* they have to reach a consensus on the battle plan
		* there may have traitorous generals
	+ Objective:
		* _consensus_: all loyal generals decide upon the same battle plan
		* _integrity_: a small number of traitors cannot cause the loyal generals
									to adopt a bad plan
	+ Original version:
		* there is a single commander, other nodes are lieutenants
			- _consensus_: all loyal lieutenants will obey the same order
			- _integrity_: if the commander is loyal, then loyal lieutenants
										obey the order sent to them
	+ __Byzantine fault tolerance__
		* byzantine fault a node that can emulate any kind of behaviour
		* _BFT_ is the dependability of a system under the presence of byzantine fault
	+ dans un système distribué continué de noeuds, le but étant
		de trouver un algorithme de consensus parmi les noeuds malgré la
		présence de noeuds défectueux ou malveillants
		* les noeuds "fautifs" n'ont pas de restrictions
		* il s peuvent crashés, envoyer de fausses données, ...
	+ un système est _BFT_ (Byzantine Fault Tolerance) s'il résiste
		à de telles conditions

# Privacy
- information about individuals
- concepts
  + _identifying/non-identifying attributes_
    * identifying -> names, date of birth, address
    * non-identifying -> movie preferences, hospital admission date
  + _de-identification_
  a property that make it difficult to link an identity through a single data release
    * removes identifying data attributes
    * statistical output over a dataset
  + _re-identification_
  an attack that allows identification of individuals
  through a chaining of queries to a de-identified dataset
  + _k-anonymity_
  property on dataset when each identifying attributes for a person is identical
  to at least k-1 individuals of the dataset
  + insights on privacy
    * we failed to tackle privacy issue when we only consider privacy as a
    property focused on the output of a query (de-identification, k-anonymity)
    * privacy needs to be seen as a relation between the input and the output
    * more explicitly privacy relies on the process which produced the output
    from the private input
  + __differential privacy__
  privacy property on a computations which from private inputs to public outputs
    * the privacy level is equal to the privacy one would have
    if the computation did not include its information
    * more robust against composition of queries (re-identification)
    * does not protect against information leak not related to an individual
    but in a similar situation to this individual
    (ex: risk of cancer when people drink a lot of alcohol)
    * does not ensure that no information about someone will be revealed but
    that no _specific_ information about that someone will be revealed
    * algorithm of computations can be revealed without lowering the privacy of the analysis
    * __How__?
      + adding carefully-tuned noise when producing statistics
      + this allows to exactly control the amount of privacy that will be leaked
      + computations are not deterministic and are approximations bounded by ε
      + __ε__ privacy loss parameter,
        * amount of privacy lost after each query (also ε-differential privacy)
        * also defines the precision of the outputs
        * small ε means that we are closer to the scenario where one information is opt-out
            * stronger privacy
            * less accurate because the output is close to a scenario without one info
    * __Example__
      + with ε = 0.01
      + a medical checkup for a survey says that X have 50% chance to die next year
      + while the survey states that the average for people at X age is 10%
      + if we use differential analysis, insurance company consulting the survey
      can only get in the __worst case__ that X has 10%(1+ε) = 10.1% chance to
      die next year
    * __Composition__
      + the amount of leakage by queries with parameter ε1 and ε2 is similar to
      a single query with differential analysis parameter of ε3 = ε2 + ε1
      + currently the only notion of privacy that can quantify composition leakage



# Hardware
- pipelining
  + at the beginning of processors at each clock cycle only one
  subunit of the processor was working
    1. fetch instruction
    2. decode instruction
    3. execute
    4. write in the memory
    5. update cache
  + to speed up processors each subunit were tasked with processing
  the next instruction in the pipeline instead of staying idle during
  the execution of the current instruction
- superscalar processor
  + with improvements of the processors we added redundant subunits of
  the CPU to be able to do the same things in parallel
  + almost always used with pipelining
- vectorization (SIMD Single Instruction Multiple Data)
  + parallelised processing of data of the same type
  + exist specific registers and instructions for these operations (SSE, MMX)
  + usually used to for loops or operations on arrays/matrices
- unikernel
  + single addressed space system constructed with libraryOS
  + the goal is construct system with a fixed-goal and only the services
  required for this goal
  + usually run as virtual machine
  + pros and cons
    + strong performance due to removing memory abstractions
    + quite secure due to the low attack surface
    + does not need a lot of memory
    + not suited to run multiple applications securely
  + an example is MirageOS

# Data Science
- What is Data Science? __answer a question using data__
- Big Data
  + derive hypothesis/behaviours from data
    * the other way around of statistics
  + 5 V of big data
    * _Velocity_, speed of data generation
    * _Volume_, scale of the data
    * _Variety_, structure of data, also reflects the fact they came from multiple sources
      - video, social network...
      - 80% of the data is considered unstructured
    * _Veracity_, accuracy of data
    * _Value_, derive value of data
  + data is split onto multiple servers
  + applies map reduce on each server
  + scale linearly between amount of data and amount of computing power
- __data mining__
  1. __Set up your goals__
    + key questions to be answered
    + evaluate the cost benefit trade-off between money and accuracy
  2. __Select data__
    + output of data mining depends on the quality of data
    + in some situations you may not have readily available fot data mining
      * hence you may need to diversify sources
      * you'll have different types/size/accuracy that may be expensive to process
  3. __Preprocessing data__
    + the goal is to only keep relevant data
    + is the data still relevant with missing parts?
      * randomly, it's ok
      * systematically, you need to consider the biases that will appear in the analysis
    + you need to consider the validity of your analysis depending of the quality of your data
  4. __Transforming data__
    + determine the appropriate format for the data
    + one consideration is to reduce the number of necessary attributes to achieve our goal
      * usage of data reduction algorithm
    + you may need to tranform the type of data (f.e: continuous income into categories)
  5. __Storing data__
    + Efficiency/Availaibility
      * scientists should have rw permissions to work on data
      * efficient access to database
    + Safety
      * replication
    + Privay
  6. __Mining data__
    + here we make the data work
    + data analysis
    + machine learning
    + data visualization
      * very helpful to develop an understanding of hidden trends in the dataset
  7. __Evaluating Mining results__
    + formal evaluation of the results
    + test predictive capabilities of derived models to reproduce data
      * called _in-sample forecast_
    + results are provided to the patrons of the study for feedback
    + we can iterate the process as many times as needed with incoming feedbacks
- __machine learning__
- definitions
  + _big data_ scale where there are so much data such that traditionnal data analysis tools
    like relationnal database does not work anymore
  + _data mining_ prepare and format data to uncover patterns from data
  + _machine learning_ learn to solve problems by learning from data
  + _deep learning_ field of machine learning that uses neural networks inspired from the human brain
  + _neural networks_ layers of nodes that gets more sophisticated depending of the depth
  + _artificial intelligence vs data science_, DS is the process of extracting knowledge from
    data and may use techniques from artifial intelligenge which consists in everything
    that allow computers to learn how to solve problems
  + _regression analysis_, discover/estimate the relationships between controlled parameters and an output
    - used for prediction and forecasting
    - used to infer causal relationships



# Miscellaneous
- __reentrant code__
    + code that when executed by a thread, even it it gets interrupted, won't change its behaviour whatever do the other threads
    + it's close to _thread safety_ but focus more on one thread whereas thread-safety englobs non-interference of all threads simultaneously
    + thread-safe code can be non reentrant, for example if used with mutex the code can starved and won't execute which could change its behaviour
    + 3 rules
        * shared var are used in an atomic way
        * does not call non reentrant code
        * does not use hardware in a non atomic way
+ __Unicode and encodings__
    * Unicode is a mapping for each character from 0 to 0x10ffff
    * encodings are alogrithms which translates Unicode strings to bytes
    * every unicode character can be written within 32 bits
    * most popular is utf-8
        - the 8 corresponds to the fact there character can be represented
        by 1 to 4 octets (8 for octets, and 4 octets give 32bits)
        - Ascii character are encoded with a single octet starting with 0xxxxxxx
        - then depending of the char we can have
          * 1xxxxxxx xxxxxxxx
          * 11xxxxxx xxxxxxxx xxxxxxxx
          * 111xxxxx xxxxxxxx xxxxxxxx xxxxxxxx  (max 32bits)
    * utf-16, encoding with 1 or 2 words of 16bits
    * utf-32, encoding with a single word of 32bits
    * __utf-n__, n corresponds to the smallest size the encodings works with
		* __code point__
			- atomic unit of information
			- a text is a sequence of code points
			- each code point possess a number in the unicode stanard
		* __code unit__
			- unit of storage of an encoded code point
			- 8 bits in UTF-8
			- 16 bits in UTF-16
		* __grapheme__
			- sequence of one or more code points representing a graphical unit
				for the reader in the used writing system
			- _a_ is a grapheme that may contain 1 code point
			- _ä_ is a grapheme that may contain 2 code points (there is also a representation with
				one code point)
		* __glyph__
			- an image usually stored in a font
			- is used to represent graphem or prats of
+ __call/cc__
    * call-with-current-continuation
    * a _continuation_ is a data structure which represents
    the current context or the state of the execution
    * allows to capture the current continuation in a variable
    and then restore it at anytime
    * similar to _longjmp_ where you can return to lower portions of the stack immediately
    * like a checkpoint where you save the current context, then call a function and when returning
    you come back to the checkpoint however the world still have the modifications done in the function
    * functional version of goto
+ __closure__
    * possibility to save a function with its environment during definition
      > y = 0
      > def toto(x):
      >   return x + y
      Here toto will always return x + 0, since y → 0 in the closure
+ __corecursion__
    *  dual to recursion
    * recursion works from the current case and tries to break it down until it reaches base case
    * corecursion starts from base case and iteratively produce data further from the base case
    * corecursive algorithms use the data they produce as they become available to grow
    * corecursion allows then to work on complex data structure and infinite data types,
    so long as it can be produced from simpler data
    * example: we can create infinite lists with coinduction and not with induction
    * in induction the spec of the datatype explain how we build one
    * in coinduction the spec of the datatype explain how we break the datatype to use the element one by one
    hence it is possible to have infinite data types since we don't need to build it
    * recursions requires one to be able to reach the base case to use the data
    * corecursions is often used with _lazy evaluation_ to work on a subset of an
    infinite data structure
# Random thoughts
- Model of the world
    1. The physically strongest rules
        - capacity to obliterate the others
        - the only mean that can resolve all the conflicts
    2. Capitalism: system where elements get stronger economically through
    consumption/exchanges/sales
    3. Economic strength reinforces physical strength
        - capacity to build an army
        - but physical strength is still more important
            + see north Korea with nuclear weapon is considered threatening
            + even though they are weak economically
    4. Globally, the earth is a capitalist system
        - most countries trade with each other and are more or less dependent of theses trades
        - few exceptions like north Korea
        - How did it happen?
            + not sure but the global opinion is to avoid too much deaths
            + so competition through war is considered dreadful
            + now countries compete through economy
    5. Therefore in order for a country to be powerful, a strong economy
    is the main solution
        - lets consider two possibilities to rule a country (not exhaustive)
        - country chooses their orientation with
            * law creation
            * law control
            * law application
            * if any of these three is missing the people won't abide by the laws
        - capitalism
            + encourage trades and initiative
            + the more you trade/consume/sale the stronger you are (economically)
            + focus on individual freedom and competition
            + better way to boost a country economy (is it true?)
            * strong eat the weak (economy) mentality
        - socialism
            + focus on equality
            * more reliant on the government
            * does not encourage trade
            + in a planet where we're lacking resources, in the global interest
            it's good to have a simple life
            - can it be as competitive as other capitalist countries on the global scene?
#World
- China
  + led by Xi Jiping
    * Chinese leader for life
    * product of the communism party
    * his father was "purged" and he was forced to be a diehard follower
    of the Mao teachings to avoid being purged too
  + Wants to dominate the world decisions
    * its enemy is the US/Europe
    * use all weaknesses of the western countries to its advantage
    * needs strong support from the population
      * internal
        - unify Chinese population objectives
          + relive the splendor of Chinese imperialism
          + condemn the occupation of western people of the past
            * opium
            * Hong Kong and Taiwan independence
        - stability of the country
          + Harsh punishment of people who do not follow the guidelines
          + Control of the media, internet or newspaper
          + Monitor the population, camera, social credit...
        - totalitarian system
      * external
        - The goal is to extends its influence worldwide
        - Economic influence
          + Silk road project
            * build air, sea, land roads between Europe and china
            * facilitate product exchanges in Eurasia
          + Invest in big infrastructure projects
            * Sri Lanka port, which could not reimburse the loan and was forced
            to sell it to China for 99 years
            * Piraeus port of Greece was sold
            * Toulouse airport is sold too
            * In Africa
              - China takes the stand of a country which was also bullied by
              western countries in the past
              - is ready to pour big amount of wealth to dictatorship contrary to westerners
              - More than half of African countries has already received money from China
          + Support western countries which were "cast" aside
            * gave money to Greece and Portugal during financial crisis
            * in return these countries defend China's interest in European summits
            * for ex. Greece vetoed the report on the state of Human Rights in China
        - Military influence
          + Big investments in military
            - weapon research
            - navy
          + start to build external military base
            - Djibouti
            - Asian Meridional sea with installation of military island
        - Political influence
          * Undermine western gathering by "poaching" some members (Greece, Portugal)
          * Make its own summit as opposite of G7
            + with Russia, India, Pakistan, Iran...
            + lots of countries with either economic or military impact
            + more inclusive than western summits
- Stock exchange
  - Initial Public Offer
    * When you need funds but investors and banks do not trust you
    * You offer to anyone to buy part of the company through shares
  - Shares
    * From the holder point of view
      + you want the company to grow up:
        - to receive dividends
        - to sell at a higher price than the buying price (stock exchange)
      + For that you can monitor and influence the company actions
    * For the stake holder (CEO)
      + Before you wanted as the company owner to grow and sustain through time
      + Nowadays CEO are chosen by the shareholders and your salary depends
      on the price of the company stocks
          - Hence your goal is to raise the price of your stocks ASAP
          - For that you can either give more dividends or buy some company
          stocks to create a scarcity and raise its price
          - The remaining money for workers, products quality is lower
          - Hence this is not a long-term strategy
    * How does their price fluctuate?
      + People doing stock exchange try to guess the future of your company
      + They can predict on real facts or they just try to feel the trends
      + If the story is snowballing then the stocks also snowballs till its limits
      + for shareholders this is heaven to sell their stocks
      + for the company they can sell more shares for revenue
    * how does it affect the economy when the shares falls
      + Effect on pensions
        - pension funds invest in stock markets
        - therefore future payouts are lower
        - can struggle to meet their promises
      + Customers confidence
        - people may be discouraged from spending
        - Affect the economy circle
      + Companies falls
        - companies shares does not buy as much anymore
        - cannot start new projects and stagnate
        - employees benefits plummet
        - lay off employees to save up money
  - how to invest?
    + Easy one you invest in index funds so your have a little in every company
    in the index (CAC40) (you follow the trend of the stock market)
    + you chose a professional investor who tries to foresee the stock market for a fee
  - a company
    + used to be viewed as opportunities for everything related to it to grow
      * shareholders, bondholder, employees, suppliers, community
      * this benefited everyone
    + now the goal of a company is to make profit to serve the shareholders
      + This only profit the wealthy and may even sacrifice the medium/poor classes
      + Can explain the growing difference between wealthy and poor

# CryptoCurrencies
- vocabulary
  + _trading_ buying and selling crypto with a short/medium term goal to
    earn money
  + _investing_ buying and selling crypto with long term goal to earn money
  + _fiat currency_ currency that is backed by a government and not
    a commodity such as gold
    * $, € are fiat currencies
    * value is related to government stability
  + _day/swing/position trade_, time-lenght on which trading is done, buying
    and selling (days/weeks/months)
    + _derivatives_ contracts which don't have value by themselves but derives it
    by the underlying asset, buyer agrees to purchase asset on a date at
    a specific price
    * _future contract_, contract where two parties promise to buy/sell each other
      assets at a predetermined price at a specific date
    * _option_ a contract that allow one to buy or sell assets at a predetermined
      price within a specific period of time
      - you are not forced to buy
      - you can use anytime before the expiration date
    * _leverage_ a loan that can be made to a broker before buying assets
      - if investments is good, the broker takes its profit directly on your benefits
      - if investments fail broker can take money that was left as insurance
        by the investor
    * _contract for difference (CFD)_, contract between a buyer and seller
      that stipulates that buyer must pay the seller the difference between the
      current value of an asset and its value at contract time
      - similar to future contract, however you don't buy assets but you just
        get the the money due to the difference
    * _token swap_, alteration of a token from a blockchain to another with
      a predetermined rate
      - used if a project wants to change of blockchain
      - if a new chain starts out lending "temp" tokens at the ICO and then
        finally launch the main chain
- Crypto vs Stock market
  + Similarities
    * trading and investing tools to analyze the market are +/- the same
    * crypto assets are valued with fiat currency
    * trading/investing strategies are similar
    * similar market products: options, derivatives, leverages to inflate gains
  + Differences
    * market volatility, crypto got wild price swings
    * market maturity, market value and trade volume is much less in crypto
    * market assets, you don't invest in a company but in an idea/technology
    * regulations are still low in crypto since we're still trying to understand
      these tech
    * 24-hour trading, opposed to stock market there are no closing the
      crypto market
    * privacy and anonymity, you don't have to decline your identity
      to trade
- Trading analysys
  + Fundamental Analysis (FA)
    * determine asset (future) value using both economic and financial factors
      affecting the asset
      - off-chain metrics: community, regulations ...
      - on-chain metricts: network fees, usage, chain scalability
  + Technical Analysis (TA)
    * predict asset future price movement based on its historical price movement
    * like using machine learning to predict price behaviour
  + which to use?
    * use both wisely
    * for short-term you'd use TA, or to figure out when to buy
    * for investment you'd rely more on FA
  + two types of market
    1. spot market, where you trade assets directly
    2. derivatives markets, where you trade derivatives
      * futures contracts, options contracts, contracts for difference,
        leveraged tokens and token swaps
- risk management
  + _market risk_, an asset or the whole crypto will unexpectedly swing and
    negatively affect your market position
  + _liquidity risk_, a situation where you're unable to exit a position
    * typically when you can't find a buyer for your asset
  + _legal risk_, when regulation or policy negatively impacts an asset
    or platform
    * you may get liquidity risk if you're barred from purchasing
    * you may even just end up losing your funds
  + _operational risk_, when you can't perform trading activity
    * may be technical failure of a platform or of the chain
  + _systemic risk_, loss incurred due to a faulure in the entire tradind system
    * collapse of systems within the marketplace
    * ex: 2008 financial crisis, systemic failure that led to market meltdown
- crypto platform exchange criteria
  + liquidity, number of assets supported and size of different order books
    * indicates popularity and reliability of the platform
  + fees, may affect the kind of trading strategy you want to use
  + payment options, how you can put your fiat money
  + security
  + user experience
  + customer support

# Formal methods
- mathematical techniques for specification, development and verification
  of software and hardware
- _formal specifications_
  + a spec is formal if composed of 3 rules
    * syntax: rules for determining the well-formedness of sentences
    * semantic: rules to interpret the sentence within the domain considered
    * proof theory: rules for inferring informatin from the spec
  + specifications using mathematical methods
    * describe how a system should behave
    * allow to verify that the system will have key properties
      that we are lookin for
  + make sure that the design is right
    * prove that design validate the required properties
  + used to develop an implementation
    * using formal verification you can check that
      that a system is correct with respect to its spec
    * use probably correct refinement to transform specifications
      into an implementation
  + Paradigms
    * history-based specification
      - characterize its maximal set of admissible histories/behaviors
      - properties are specified by temporal logic assertions
      - these assertions can refer to past, current and future states
      - temporal logics are often necessary to specify properties
        over time bounds
    * state-based specification
      - characterize the admissible states of the state at a snapshot
      - properties are specified  by
        + invariants constraining system objects at any snapshots
        + pre and post assertions constraining the application
          of system operations at any snapshot
          * pre-assertion captures a weakest necessary condition on input states
            for an op to be applied
          * post-assertion captrues a strongest effect condition on output states
            if operation is applied
      - used by B, Z, CFM
    * transition-based specification
      - characterize the required transitions from state to state
      - properties are specified by a set of transition functions
        + transition functions give output given an input state
          and an event
        + necessary preconditions can be added to guard a transition
      - Statechars, PROMELA, SCR
    * functional specification
      - specify a system as a structured collection of mathematical functions
      - 2 ways
        + algebraic specification
          * functions are gouped by types of domain/codomain, therefore
            defining algebraic structures
          * properties are specified as conditional equations that
            capture effect of composing functions
          * OBJ, ASL, PLUSS
        + higher order functions
          * functions are grouped into logical theories
          * theory contain, type def, variable declarations and axioms
            defining the functions in the theory
          * functions may be arguments of other functinos
          * HOL, PVS
    * operational specification
      - characterized by a collection of processes that can be executed by
        an abstract machine
      - Petri nets or process algebras
- _formal development/verification_
  * act of proving the correctness of algorithms of a system in respect
    to a formal specification
  * 2 approachs
    + __model checking__
      - consists of a systematically exhaustive exploration of
        the mathematical model (finite state machine FSM)
      - possible for finite models
			- possibility of state explosion problem
      - for infinite models you use abstraction techniques to consider
        a whole groups of states in a single operation and reduce computation
        * state space enumeration
          + reduce the state by only maintaining the parts relevant for
            the analysis
          + allow to group states which have identical relevant parts
        * abstract interpretation
          + sound approximation of the semantics of computer programs
          + use an abstraction of the objects that can lead to a sound
            analysis of studied property
            - create an abstract semantics on which model checking will be applied
            - create an abstraction function that maps elements o
              concrete semantics to abstract semantics
            - create a concretization function
          + abstract semants are described using fixed points in presence of
            loops or recursive procedurs
          + abstract domains examples:
            - intervals for real
            - congruence relations on integers
            - convex polyhedra for related variables
              * x + y + z = 0, will give a 3d polyhedra
        * symbolic algorithms 
					- represent the graph using quantified propositional logic instead
						of a finite state machine
					- then solve it with a binary decision diagrams
				* bounded model 
					- unroll the FSM for a fixed number of steps
					- check if property is violated within this number of steps
					- this turn the problem into SAT
			- biggest advantage of model checking is that it is often
				fully automatic
			- biggest disadvantage is that it generaly does not scale to large
				systems
    + __deductive verification__
			- generate from the system and the formal specs a collection of
				_proof obligations_
			- truth of these proof obligations imply correctness of the 
				system with respect to the specs
			- how to discharge proof obligations 
				+ interactive theorem provers : HOL, ACL2, Isabelle Coq
				+ automatic theorem provers: SMT solvers (satisfiability modulo theories)
			- usually require expertise from a human
- __Semantics__ used to describe a programs behaviour
	+ _Denotational semantics_
		* suited for functional programing
		* describe program as a function 
		* return anoutput from an input
		* does not really describe the system structure but behaviour 
		* gets complicated with non-termination or using complex types
			- return records/functions
			- use polymorphic functions ...
	+ _Operational semantics_
		* suited for imperative programing 
		* sequence of state transitions representing an execution 
		+ Different operational semantics
			* big-step
					+ also called natural semantics, intuitive
					+ have a strong induction principle (for example for a call command you will
					have as premise that the function called terminates directly instead of
					going in the internals of the function called)
					+ easy to extend
					- cannot differentiate between divergence and going wrong behaviour
					- concurrency and unstructured control (goto) hard to handle
					* Reasoning with pre and post conditions
			* small-step
					+ also called structured operational semantics
					+ view computation as a sequence of λ-calcul reductions, which are an elementary computation
					+ clean and capture all possible program behaviours (termination, divergence, crash)
					+ extend to unstructured constructs
					- lack a powerful induction principle
					- syntax must be extended with intermediate forms during reductions
					> (while b do c)/state -> c; (while b do c)/state     with eval b = true
					- the reduction create "new terms" like the above example where "c; while" did not exist
					* Reasoning by simulations with a case analysis on each transition
			* small-step with continuation
					+ add a "continuation" term in the state allowing the small step to focus
					on a part of the command without bothering with the remaining commands
					>  (while b do c)/k/state  ->  c/Kwhile b c k/state    with eval b  =true
					>   skip/Kwhile b c k/state -> (while b do c)/k/state
					+ remove the need to introduce intermediate forms in the semantics
					+ remove the need to generate new forms during the reduction
	+ _Axiomatic semantics_
		* works with pre and post conditions
		* works with set of states instead of single ones
		* we do not know the states but solely statements that satifiy the states
# Logics
- How and what you can reason about
- __minimal logic__
 + formulated using
	* implication: → 
	* conjunction: ∧
	* disjunction: ∨
	* falsum: ⊥
 + rules of inferecence
	* without excluded middle: 
		- you always have A or not A
		> A ∨ (A → ⊥)
	* without principle of explosion: 
		- to derive any proposition B one may do this by deriving absurdity
		> (A ∧ (A → ⊥)) → B
- __intuitionistic/constructive logic__
	* minimal logic + principle of explosions
	* used in Coq
	* you can only prove what you can build
- __classical logic__
	* intuitionist logic + excluded middle
- __modal logic__
	+ called the logic of necessity and possibility
	+ we add notion of "necessarily": □
	+ we add notion of "possibly": ◇
	+ rules of inference 
		> □P → ◇P
		> ◇P ↔ ¬□¬P
		> □P ↔ ¬◇¬P 
	+ __temporal logic__
		+ logic where the truth of a statement may vary with time
		+ those are called "tense expressions"
		+ tense expressions are treated in term of modalities
			* □P: P is the case at some time t 
			* ◇P: P is the case at every time t
	+ __epistemic logic__
		* related to the knowledge of one party
		* can be expressed with modalities
			- K translates into "it is known that"
			- _K_a P_  translates into "Agent a knows that P"
			- □ is translated as "x knows that ..."
			- ◇ is translated that "for all x knows, it may be true that ..."
- __separation logic__
	* logic used to reason on shared memory (usually heap)
	* formalize how a command impacting a local region of memory do not influence
	the rest of the memory and other commands which do not associate with
	the impacted region
	* close to hoare logic
	* The frame rule is the most important one
	>     {p} c {q}
	> ------------------------  Frame rule
	>   {p * r} c {q * r}
	_p_ and _q_ pre and post-predicate on the memory around command _c_
	if _r_ holds onto a part of memory not related to _p_ and _q_ then
	execution of command _c_ preserve predicate _r_
- __propositional logic or 0th order logic__
	* only deals with simple declarative proposition
		* proposition: statement which is either true or false
	* complete: all statements are provable
	* decidable: there is an effective method to prove everything (truth table algorithm)
	* compact
			- si toute partie finie d'une thèorie est satisfaisable alors
			la théorie est satisfaisable
			- satisfaisable: il existe une instance validant la formule
- __predicate logic or 1st order logic__
	+ propositional logic that also covers predicates and quantification
		* add predicates: proposition which truth depends on the value of its variables
			- "France is in Europe" is a proposition
			- "x is in Europe" is a predicate, truth depends on value of x
		* add quantification operators ∀ and ∃
	* complete: all statements are provable
	+ not decidable (in general): there isn't an algorithm to prove everything
- __higher order logic__
	* orders 
		+ first-order logic quantifies only variables
		+ second-order logic quantifies over sets 
		+ third-order logic quantifies over sets of sets 
		+ ...
	* not complete
	* not decidable
- Logics in computer science
	+ Key fields: computability theory, modal logic, Curry-Howard correspondence, 
		category theory
	+ __computability theory__, refer to #Math
	+ __modal logic__, refer to the paragraph in #Logics
	+ __Curry-Howard__, refer to earlier paragraph 
	+ __category theory__, refer to #Math

# Math Theories
- __What is a theory?__
	+ a theory is a set of theorems
		* a theorem is a statement which is true with respect to the theory
	+ a theory can be defined by an _axiomatic system_ 
		* a set of elementary statements (axioms) from which 
			all the statements can be derived 
		* axioms are by definition true with respect to the theory
		* ex: ZFC, Peano arithmetic
	+ a theory can be dfeined by a structure 
		* the theory corresponds to the set of statements satisfied by the structure 
		* ex: (N, +, x, 0, 1, =)
		  - called true arithmetic
			- set of all true 1st-order statements about arithmetic of natural numbers
			- cannot be described by an anumerable set of axioms
- __Properties of theories__
	+ _Consistent_: we cannot prove P and not P within the theory
	+ _Complete_: consistent theory where every statement is provable/disprovable
- __Set theory__
	+ historic
		1874. publication of the naive set theory by Georg Cantor
		1900. discovery of paradoxes in Cantor set theory
		1973. ZFC axiomatic system is published and the most commonly used
			for set theory and is consistent
		currently. 
	+ applications
		* algebraic geometry can be defined under set theory
		* algebraic topology can be defined under set theory
		* graphs, manifolds, rings, vector spaces can be defined with sets
		* properties of natural and real numbers can defined with set theory 
			augmented with 1st or 2nd order logic
- __Category theory__
	- some intakes
		+ can be seen as a the "next level" from set theory
			* same as set theory can be seen as the "next level" to propositional logic
			* every construction in set theory can be generalized in category theory
			* language of category theory can emulate any statement of set theory 
			* ex: a set cannot contain itself in set theory but can be expressed
				in category theory
		+ category theory is definitional but not constructive 
			* categories and morphisms exist but does we do not describe 
				how to construct them 
			* using category theory there are no guarantees that it possesses
				any object 
			* set theory on the contrary describes what sets contain or do
		+ set theory explains internally and category theory externally
			* set theory describes set from its part and relationship between its part
			* category theory describes relationship between categories
		+ category theory is quite abstract allowing to solve some
			problems elegantly and straightforwardly but without giving 
			any insights on the topic
			+ category theory is really generic 
			+ most mathematic areas can be reformulated using category theory 
			- some results from category theory may be seen as _abstract nonsense_
				because these results translated in other theories may be obvious or
				irrelevant
			- this may due to how generic and how category theory focus on objects
				behaviour rather than objects themselves
	+ Basic elements 
		* _morphims_
			- instead of studying the structure of a mathematical obect, 
				category theory focuses on studying on morphisms which
				preserves the structure between 2 objects 
			- ex: morphisms between groups f: (G, +) -> (G', *)
			- allow to study general properties about groups and effects 
				of axioms between groups
		* _functors_ 
			- "morphisms" between categories
			- mapping between categories while preserving its structure
			- in a category which objects are categories, morphims represent functors
		* _natural transformations_
			- a natural transformation turn a functor into another functor
				while conserving the structure of the concerned categories
			- can be seen as a morphims of functors
		* summary 
			- functor = morphism of morphisms
			- natural transfor = morphism of morphism of morphisms
		* a category is a labeled directed graph
			- nodes are called objects
			- labelled directed edges are morphisms/arrows
			- a category can compose its morphisms associatively
			- each object of a category has an identity arrow/morphism
- __Type theory__
	* formal system where every object has a type 
	* a type defines the meaning of its objects and the operations 
		which can be applied to them
	* was created to avoid paradoxes in previous foundations 
		- naive set theory 
		- formal logics
		- rewrite systems
	+ _Basic concepts_
		* term, types and value
			- each term posses a type 
			- a type defines the range of value that aterm might be evaluated to
		* typing environment 
			- mapping from term to type
		* rewriting/reduction rules 
			- type theories have reduction rules 
			- corresponds to explicit computation
			- when a term cannot be reduced further, it's called _normal form_
			- a theory is strongly normalizing if every term can reach a normal form
	+ Decisions problems 
		* classic problems of type checking, typability and type inhabitation
		* _type checking_, decides type safety of any program
		* _typability_, decides if there exist a type t and an environment E
			such that a term has type t in environment E
			- closely related to type inference 
			- typability addresses existence of a type for a term
			- type inferece calculates such a type
		* _type inhabitation_, decides if there exists a term inhabiting a type
	+ Components of type theories
    * term taking term as parameter 
        + classical function
    * term taking type as parameter
        + polymorphism
        + sort, function taking type as parameter
    * Dependent type
				+ type taking term/type as parameter
        + ex: list of length n, type constructor
		* Inductive types
			* variant of algebraic types 
			* sum/product types with recursivity
			* recursion is not infinite, we expect to find a base case
				+ we have recursion with decreasing argument to avoid non terminating program
				+ otherwise when we're able to reduce expressions an infinity of times
				we can almost always give it the empty type (False)
			* coinductive types
				+ infinite data types created by a giving function that
					generate next elements
			* Session types
				+ types for protocols
			* Equality types
				+ Martin-Löf Equality 
					- two objects are equal iff they are interchangeable
				+ Leibniz Equality 
					- two objects are equal iff they satisfy the same properties
  - _simply typed λ-calculus_
    * term taking term as parameter
    * types with no parameters
		* equivalent to higher-order logic
  - _System F_ (Girard)
    * Add polymorphism to typed λ-calculus
    * Is able to express 2nd order arithmetic
      + 1st order + quantification on predicate
      + 2nd order arithmetic can express real numbers
          => system F can formalize analysis
  - System Fω
    + Add type constructor to system F
    + add a layer of abstraction with "kinds" which are types of types
  - Logical Framework (LF)
    + Add dependent type to λ-calculus
    + we do not have proposition as type
    + we only have:
      * boolean for proposition
      * term as demonstration
      * type of a demonstration
    + hence logical operator like ∀ or ∃ can be directly implemented
        in the type of the demonstration
    + whereas adding new rules in other systems
  - Systems with all 4 parametrisation forms
    + Intuitionistic type theory or Martin-Löf type theory (Agda)
        * dependent type
        * and two operators for type constructor (∀ and ∃)
    + Calculus of Construction (Coq)
      * Fω + dependent type
    + Pure Type Systems
      * Unification of previous type systems for a clean formalisation
      * In previous system, multiple λ-terms were introduced for terms, types, kinds...
      * In previous system, multiple functions were introduced for terms, types, kinds...
      * Now a single λx: A.B for all abstractions
      * now a single Πx: A.B for type of functions
      * to differentiate the element we're working with __universe__ are introduced
        + universe of types *
        + universe of kinds □
  - λ-cube of Barendregt sums up these type systems in a cube
    + λ-calculus a origin
    + adding the other form of parametrisation leads to other types systems
  - from PTS we can play with universe
    + for example Coq split the term universe into Prop and Set
      * Set for computable term (for extraction)
      * Prop for proof term (for demonstration only)
    + Careful about paradox!
      * Predicative, a universe cannot type itself
      * Russel: "Whatever involves all of a collection cannot be part of the collection"
      * impredicative: we can quantify on ourselves (Prop in Coq, with some constraints to avoid paradox)
    + How to reuse definitions between universe
      * cumulativity
          - A: Type{i} -> A : Type{i+1}
      * polymorphism of universe
          - define functions with parameters of arbitrary universe i

# SMT 
- Introduction to SAT (Boolean Satisfiable Theory)
	+ SAT is a decision problem 
		* given a propositional formla, is it satisfiable
		* exists values for the variables of propositional formala such that 
			the statement is true
	+ a basic method is to use _truth table_
		* always works
		* but exponential in the number of varables
	+ SAT is NP-complete
		* SAT was the starting point of research on NP-completeness
- SMT solving
	+ Satisfiability Modulo Theories 
	+ SAT with extended with extra theory: linear inequality 
	+ decision problem applied to an extended theory
		>    (2a > b + c ∧ a) satisfiable?
	+ tools: Z3, Yices and CVC4
		* have their own syntax 
		* also accept a common one which is SMT-LIB standard

# WebAssembly
- two formats: 
	+ _.wat_ (WebAssembly Text): text format using S-expressions 
	+ _.wasm_: binary format intended for a wasm virtual machine

# Programing Design Patterns 
- Creation patterns
  - __Builder pattern__
    * what for?
      + you want to create a complex object Foo
      + init function would require lots of optional arguments
    * How? 
      + create a `Builder` which functions are used to configure the future 
        Foo instance
      + Builder functions takes itself and return/or modify the factory
      + when configured call a factory function `build()` that will return 
        our desired Foo instance
      > FooFactory::new().set_bar(3).set_toto("yolo").build();
  - Factory Pattern
    * what for? 
      + you have multiple types that share the same interface `Foo`
      + you want to have a single way to init these types 
      + you only use these objects through their common interface `Foo`
    * how?
      + Have a `FooFactory` type which have an init function
        returning an object implementing `Foo`
