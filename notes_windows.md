# Windows architecture
- Design goals for Windows NT
  + __Extensibility__
  + __Portability__
  + __Reliability and Robustness__
  + __Compatibility__ wirh olser versions of Windows and with MS-DOS
    + also good interoperability with Unix, OS/2, NetWare
  + __Performance__
- OS
  + Monolithic like Linux
    * OS and drivers share the same kernel  protected memory
    * to address this issue:
      - WHQL, _Windows Hardware Quality Testing_
        => Series of tests audited by Microsoft
      - KMCS, _Kernel Mode Code Signing_ requirements
        => Drivers need to be eligible by Microsoft to run
      - Virtualization Security
      - Device Guard
        => Security feature which only allows the execution of "trusted"
          applications by Microsoft
        => Code integrity and certificate checks are performed outside the OS
          using hardware-assisted virtualization
  + uses object-oriented design principles
    * usage of objects to represent system resources
    * using formal interfaces to pass access/modify other components structures
  + Most of the kernel-mode OS code is written in C
  + Modes of execution
    - user-mode processes
      + user processes
      + service processes
        * host Windows services
        * Task scheduler or Print Spooler
      + system processes
        * fixed service
        * logon, session manager, not started by the service control manager
      + environment subsystem server processes
        * support for other environment
        * Posix, OS/2, WSL (_Windows Subsystem for Linux_)
    - DLL subsystem and Ntdll.dll
      + to make system calls, windows applications go through of the DLLs
      + the DLLs subsystem translate a documented function into the appropriate
        internal native system calls mostly implemented in __Ntdll.dll__
      + _Kernel32.dll_, _advapi32.dll_, _User32.dll_, _Gdi32.dll_ are core
        subsystem DLLs
    - kernel-mode
      + executive, base OS services
        * memory management, threads, security, I/O, networking, IPC
        * contains architecture-specific code
        * for system specific code you use HAL
        * _Ntoskrnl.exe_
      + windows kernel
        * low-level OS
        * thread scheduling, interrupt, multiprocessor sync
      + device drivers
        * translate user I/O calls into specific hardware I/O requests
        * non hardware too: file system, network drivers
        * _*.sys_ in _\SystemRoot\System32\Drivers_
      + hardware abstraction layer (HAL)
        * isolate code of the kernel from platform-specific differences
        * different system among the same architecture
        * _Hal.dll_
      + windowing and graphics
        * GUI functions (Windows USER and GDI)
      + hypervisor
        * single component
        * own memory manager, scheduler, interrupt, …
        * _Hvix64.exe_ for Intel, _Hvax.64.exe_ for AMD
    + Symmetric multiprocessing
      - no master processors, kernel and user code can run on any of the
        available processors
      - supports four multiprocessor systems
        + multicore
          * use the cores as distinct processors
        + simultaneous multi-threaded (SMT)
          * support for Intel Hyper-Threading
          * two logical process for each physical code
          * each logical processor has its own CPU state but they share
            the onboard execution engine and cache
          * allows a logical CPU to advance while the other is stalled (after
            a cache or branch misprediction)
        + heterogeneous
          * for ARM versions of Windows
          * when you have processors with different capacity able to execute the
            same instructions
          * optimize processing power
        + non-uniform memory access (NUMA)
          * optimise spatial usage of memory
          * processors are grouped into nodes
          * the scheduler attempts to assign threads to nodes located
            in the same memory area

# Boot order
- Idle(0), track idle
- System(4), process owning all the system threads (kernel mode)
  - Registry
  - Memory compression
  - smss.exe, Session Manager SubSystem
    + first user mode process created
    + start _csrss.exe_, Client Server Runtime SubSystem
      * critical for the Windows subsystem
    + starts _Wininit_

# Subsystems
- Each binary file on windows specify a subsystem to be ran on
- a subsystem provide an environment and API to the kernel to execute a
  program
- Generally a subsystem contains multiple susbsystem DLLs that either:
  + execute fully in user mode in the subsystem dll
  + call a function from the underlying NTDLL
  + computation are required in the environment subsystem process,
    a client/server request (ALPC) is made between subsystem dll and
    the env subsystem
- Windows subsystem, Posix, OS/2, WSL
- Windows SubSystem
  + Csrss.exe is launched for each instance
  + Win32k.sys
# WSL (Windows SubSystem for Linux)
- usually every binary

# Services
- a process
- Conforms to the interface rules of the Service Control Manager (SCM)
- started by the system boot, user through services control applet or an
  app using the service functions
- services can execute even when no user is logged on
- services.exe,
  + control all the services running
  + starts _svchost.exe_
- services require a trigger to start running
- many 3rd party applications start services
- controlled and configured by the administrator
- ran by built-in user accounts: System (root), Network Service, Local Service
- they are often interdependent

# Windows Registry
- Windows is the only OS using registry
- in Linux configuration is done through text files
- central repository of all configurations
- database designed for fast reads and writes and efficient storage
  + always loaded in the RAM
  + defined with key-values
- Registry keys
  + HKLM stands for HKEY_LOCAL_MACHINE
    * SAM, security accounts manager (store encrypted passwords)
    * Security (only accessible by admins)
    * System
    * Software (information of installed software)
    * Hardware
    * Components
    * BCD
  + HKCC, hkey_current_config
    * information gathered at runtime, not permanently stored
  + HKCR, hkey_classes_root
    * information about registered application
  + HKCU, hkey_current_user
    * information about the logged-in user
  + HKU, hkey_users
    * used only if there multiple people are logged in
- Registry files are stored in the hard drive and are called _hives_
- Some are stored in:
  + Windows/System32/config
  + Users
  + Boot partition
- Everything uses registry to fetch and update information
  + except .NET framework applications (they use XML)
  + portable applications, keep the configuration data locally
- to modify the registry safely you use the control panel
  + for admins you can use the registry editor
- Restore points create a backup of the registry and system files (_System Restore_)
  + restore points are created when:
    * software is installed
    * windows update
    * installation of a not signed driver
    * not enabled on Windows 10 but can be done regularly if configured
    * manually
  + restore points are stored in _C:\\Local Disk\System Volume Information_
    * access restricted to admin

# System file checker
- Check all the Windows System files
- Windows OS is solely made of files and the registry
  * in case of failures it's either the registry or the system files
    which contain the issue
  * use restore points for registry issued
  * for the files you can use __sfc.exe__ utility (System file checker)
- Sfc check signatures of the files
- System files are stored in _C:\\Windows\System32_
- There is Windows Side by Side if you wanna different dlls the one from
  System32 (_C:\\Windows\WinSxS_)
  * allow to keep the windows files intact
  * the registries specify where the app can find its dll
  * very useful if you install old applications/games/drivers

# Startup repair
- when your Windows does not start
- WIM file (Windows Image Format)
- called __winre.wim__
  + installed on every pc
  + you can boot from it, build an image
  + launch startup repair

# Windows 10 storage spaces
- Windows wants to swap hard drive with virtual storage
- between hard drive and OS there is the _Disk Manager_
- the disk manager allows the OS and drives to communicate
- Except the C: other drives shoulb be used as __storage__

# Windows File Systems
- FAT32 (Win), New Techonology FileSytem(Win), ext4(Linux), Hierarchical FileSystem+ and APple FileSytem (macOS), Btrfs (Btree File System successeur de ext4)
- ISO 9660 (CD-Rom) is also a file system
- Allow data storage and retrieval
  + file names
    * case sensitive/unsensitive
    * case preserving/unpreserving
    * NTFS is case unsensitive and preserving
    * ext4 is case sensitive and preserving
  + directories
  + metadata
    * size
    * time creation/update
    * author
    * …

# Windows process
- a virtualized container started by a file
- contains
  + Memory
    * code
    * stack/heap
  + System resources
    * handles
      - resources in Windows are modelled into an _object_ data structure
      - applications access these objects through _handles_
      - registry keys, files and folders, graphical objects, events
      - each object has its own Access Control List (ACL), that specifies
        the allowed actions that a process can perform on the object
    * dll
      - library of APIs to the kernel
      - code that actually runs
      - in System32 there are around 5000 dlls
      - 2 categories of dlls
        + Windows dlls, protected by Windows File Protection
          * only TrustedInstaller can touch these files
          * when updating or installing an app, TrustedInstaller is the
            user performing
        + application dlls
          * written by developers
          * generally found in _Program Files_
  + threads
    * a CPU only see threads
    * a processor containes multiple cores
    * if a core supports hyper-threading then it runs 2 threads simultaneously
    * an OS sees 4 CPUs with 2 hyper-threaded core
  + security (permissions)
  + IPC
    * rules on which systems stability relies on
    * rules allowing app to communicate
    * rigid for stability
- IPC
  * clipboard
  * COM
  * Data copy
  * Dynamic Data Exchange
  * File Mapping
  * Mailslots, anonymous and named pipes
  * RPC (follow standards)
  * Windows Sockets
- Processes can be
  - user installed applications
    * installed in Program Files and Program Files (x86)
    * more and more in the user profile
  - services
    * all children of __services.exe__

# Windows Internals
- contrary to linux Windows have no clear separation between kernel and userspace
- there is no clear interface for user applications to interface with the kernel
- Win32 API is what is proposed to developers
- but contrary to Linux system calls it's a thick wrapper
- Win32 API relies on the NTDLL, which is the interface to the kernel but is
  not documented
- ntdll.dll documentation  comes from reverse-engineering or leaks
- C fopen -> CreateFile -> NTCreateFile
- Why?
  + linux system calls are almost the same as the C functions
  + linux the Kernel interface is stable
  + Windows wanted more flexibility
    * they can easily change ntdll
    * Windows NT was meant to support multiple OS
    * provide multiple API for different OS
    * it originally supported Posix and OS/2
    * with its popularity it stopped supporting other OS
- everything in Windows are "secure" objects
  + can have security permissions associated with the objects
  + control which user can read/write... the object
  + file, directories, threads, devices, semaphores, registry key/values ...
  + more precise, flexible but more complicated than in windows
  + process and threads also have security attributes
    * the user can inspect a thread object and modify its structure
      like the instruction pointer
- DLL loading
  + order:
    1. Executable directory
    2. _C:\Windows\System32_
    3. Current directory
    4. in the %PATH%
- File access
