# TODO 
- FPC-BI (Fast Probabilistic Consensus withing Byzantine Infrastructures)
	+ https://arxiv.org/abs/1905.10895	
- FCoB (Fast Consensus of Barcelona)
- Stronghold, formal verification
- Vector commitment, elliptic curve pairings, 
	+ stronghold 
		+ secret storing
		+ memory manager verification
- machine learning and formal verification
- HoneyBadgerBFT: https://eprint.iacr.org/2016/199.pdf 

# Organisation
- Applied Research
	+ Wolfgang Welz
	+ Vlad Semenov (Applied Research)
- Research 
	+ Angelo Capossele Research (teamlead)
	+ Andreas Penzkofer (Analysis of the protocol)
	+ Andrew Greve (Taiwan, social study background, organisational)
	+ Bartosz Kusmierz (Poland, consensus) 
	+ Ching-Hua Lin (Taiwan, goShimer, new consensus protocol)
	+ Darcy (mathematician, probabilistic theory, game theory)
	+ Hans (german, creative, previous hacker)
	+ Luigi (Rate control, congestion, ban projection, teamlead) 
	+ Olivia (mathematician)
	+ Vassil (crypto professor, part-time)
	+ Vidal Attias
	+ Jonas (math, goshimer)
	+ Sebastian (professor, part-time, data analysis)
	+ Daria and Piotr (goShimer)
	+ Ajay (new person, distributed engineering) 
- Engineering
	+ Gal (Head of previous node implementation, more research)
	+ Andrea Villa (Security)
- Smart contract 
	+ Evaldas (smart contract)
	+ Karolist
	+ Dave (official teamlead)
	+ Levente Pap (spec)

# Projects 
- nodes implementation:
	- __IRI__ (IOTA 1.0)
		+ out of service
		+ IOTA Reference Implementation 
	- __Hornet__ (IOTA 1.5)
		+ node softaware written in Go 
		+ intended for low-end devices like Rasberry Py
	- __GoShimmer__ (IOTA 2.0)
		+ research prototype where IOTA 2.0 modules are tested
		+ modular, each module represents one of IOTA components
		+ code to run GoShimmer node
	- __Bee__ (IOTA 2.0)
		+ framework for building IOTA nodes, clients and applications
		+ modular collection of extendable crates which expose foreign 
			function interfaces for the next iteration of client libraries
	- __Wasp__
		+ for smartcontracts as a 2nd layer protocol
		+ node software to run the ISCP on top of the Tangle
		+ built on top of GoShimmer
		+ implementation of a _validator_ node
	- __Chronicle Permanode__
		+ official Permanode solution of the IF
		+ enables unlimited storage of the Tangle entire history
		+ make it so data is easily accessible
- __Chrysalis__
	+ intermediate update, IOTA 1.5
- __Pollen__
	+ decentralized testnet for IOTA 2.0
- __iota.rs__
	+ official rust lib
	+ goal is to have _one source code of truth_
	+ allows to do:
		* create msg with indexation and transaction payloads 
		* get msg and output 
		* sign transactions 
		* generate addresses 
		* interact with an IOTA node
- __Firefly__
	+ new official wallet for Chrysalis

# IoT
- machine to machine payment (M2M-participants)
	+ intelligent machines become self-determined participant of the economy
		* they own their own bank accounts
	+ a machine will be able to earn and money on its own
		* a machine could rent its services
		* pay/call technicians for self-reparation
		* refill on necessary goods
- micro-payment transactions still have high costs 
	+ barrier to an M2M economy
	+ free-transactions would be a starting point
- Internet of Everything ( __IOE__)
	+ people, processes, databases ... are connected to form a single entity
	+ communication technology
		* IoE will use different tech for communication
		* well-known static internet
		* 5G will enable the connection of a huge amount of devices and IoT
			- big witdthband 
			- revolutionize mobility, coverage will be better 
				+ enable connected cars since transition between antennae will be smooth
			- 5G pros and cons 
				+ Up to 90% less power consumption of mobile devices (depending on the provider)
				+ 1/1000 Energy consumption per transmitted bit
				+ Extremely low latency enables real-time responses
				+ Pings of less than 1 millisecond
				+ 100 billion mobile devices simultaneously addressable worldwide
				+ Around 1000 times higher capacity
				+ Up to 100 times higher data rates than today’s LTE network 
					(i.e. up to 10,000 MBit/s)
				- smaller range, transmitters would have to be placed every 300 meters
				- effects on health not clear
		* LiFi, _Light Fidelity_)
			- data is passed through LED lights 
			- LED send 0 and 1 throough blinking
			- human do not perceive the difference
			- much faster than current WLAN signals
			- LiFi pros and cons
				+ Higher speeds than Wi-Fi, HD streaming would no longer be a problem
				+ Greater bandwidth, allowing a higher number of data channels in the same space
				+ More secure, an attacker would have to have physical access to the light
					source to intercept or manipulate data packets.
				+ Prevents piggyback transmissions (unauthorized access to the network).
				+ Easy to implement for existing LED lights
				+ Eliminates neighboring network interference 
					(e.g. WiFi channel overlap with the neighbor).
				+ No radio interferences (radio, radios, microwave etc.).
				+ Causes no interference in sensitive electronics and is therefore well 
					suited for use in sensitive environments such as hospitals and airplanes.
				- Optical data transmission only works when the devices are in direct 
					visual contact, transmission through walls is not possible.
				- Lacking mobility, due to the required visual contact, stationary 
					transmitting stations are still required at present.
				- The light sources must always remain switched on for operation.
			* LoRaWAN
				- communication architecture optimized for IoT
				- data are transfered over license-free radio frequencies
				- low energy consumption, long range
				- Pros of LoRaWAN
					+ Use of license-free frequency bands from the ISM bands. In Europe these 
						are the bands in the 868 and 433 MHz range. By using frequency spreading, 
						the technology is almost immune to interference radiation.
					+ High ranges between transmitter and receiver, from 2 km in urban areas 
						up to 15 km in rural areas. Depending on the environment and buildings,
						whole cities could be covered.
					+ Battery-powered sensors can be operated for more than five years 
					  with one battery charge. With this technology, large sensor 
						networks can be maintained with low maintenance costs.
					+ Considerable cost savings of the required infrastructure compared 
						to existing systems.
					+ The system has a high sensitivity of -137 dBm. This allows higher 
						penetration deep into buildings and basements, which increases the 
						availability of the network.
					+ Adaptive data rates (ADR): the server manages the data rates 
						(0.3 to 50 kbit/s) individually for each terminal device. The signal 
						strength is also controlled depending on the distance to the base station.
						This ensures optimal conditions regarding the fastest possible data rate,
						best possible network capacity and low energy consumption.
					+ Many operators already use LoRaWAN and offer the technology as part of 
						their service offerings in numerous countries worldwide. This makes the 
						technology even more interesting as it is compatible with the networks 
						of different operators.
					+ Large networks with millions of devices can be supported
					+ Support for redundant operation
					+ Plug and Play, the standardized interfaces (API) allow sensors and 
						applications to be connected quickly and flexibly.
				  + LoRa-enabled sensors are already available on the market or the existing
						sensor technology can be easily converted for LoRa by the sensor 
						manufacturers by exchanging the radio module
					
# Rate-Control
- __Problem__:
	* Since IOTA is a permissionless DLT you need a mechanism to avoid DoS attacks
	* In Bitcoin access control is moderated by:
		+ only miners are able to add transactions 
		→ IOTA do not differentiate between miners and users
		+ blocks are mines using PoW
		→ we want IoT to be able to add transactions, there is too much difference
			in computing power between specialized hardware and IoT, using PoW you can 
			spam the Tangle with ASIC
- __Challenges__ for the control-rate algorithm
	1. no fee-based 
	2. heterogeneous network with wide differences between nodes in terms of capabilities
	3. consider presence of malicious nodes
- __Solutions__
	+ _VDFs_ (Verifiable Delay Functions)
		* cryptographic puzzle similar to PoW but the main point is that
			__currently we are not able to speed up the computations using parallelism__
		* therefore solving speed does solely depend on machine capabilities and 
			pooling together multiple machines do not improve speed
		* VDFs proof generation algorithm:
			1. pick difficulty τ (number of sequential operations to solve)
			2. pick RSA modulus _N = p . q_ 
				- length of N is λ (usually 1024, 2048 or 3072)
				- p and q prime numbers of the same number
			3. pick a hash function _H_ 
				- can use SHA-256
			4. compute _y = H(m)^2^τ mod N_
				- in short square τ times the hash of a message m and take its modulo N
				- m is the previous transaction issued by the same node
			5. create proof (l, π) from _y_
		* VDFs verification algorithm is independent of τ (the difficulty)
			- requires at most _2k / log2 (2k)_ multiplications
		* __pros__
			- compared to PoW the capabilities between machines is reduced since 
				parallelism does not improve computation speed
		* __cons__ 
			- still quite new we don't know if VDFs functions can be parellelized
			- there are still gaps of possible transaction speeds between IoT and good CPU

# IOTA goals 
- __complete decentralization__
	+ today not the case with the coordinator
	+ Coordicide project aim to get rid of it
- __permissionless access to the network__
	+ everyone can join and at any time
	+ more honest nodes imporves network security 
- __partition tolerant__
	+ part of the tangle can be disconnected 
	+ this branch tangle can run for a period of time autonomously
	+ can be reconnected to the main tangle
- __finality within seconds__
	+ voting process allows to make complete decisions very quickly
	+ _true finality_ 
		* deterministic rather than probabilistic
		* an attacker with unlimited hashing power would not be able to
			reverse the ledger state
- __reliable timestamps__
	+ named Tangle Time 
	+ timestamp in a message that was confirmed
	+ Tangle Time cannot be attacked without enough Mna
- __scalability__
	+ the more the nodes the faster is the network
	+ dynamic sharding
- __adaptive rate control algorithm__
	+ make impossible for an attacker to flood the network 
	+ work on a node-to-node basis 
- __increased reliability__ 
	+ different tip selection strategies 
	+ preferred part of the tangle is chosen through voting
- __Security/Mana__ 
	+ novel sybil protection mechanism
	+ based on mana and voting
- __Intelligent and stable auto-peering__
	+ reduces maintenance effort for node operators 
	+ makes network more stable
- __UTXO (Unspent Transaction Output) model__
	+ each token is uniquely identifiable 
	+ each output specifies the token it wants to move
	+ balance is made of value blocks corresponding to money left
	+ in a transaction you have:
		* Inputs: define a list of OutputIDs referencing the consumed Outputs
							from previous transactions
		* Outputs: define where and how many of the tokens are moved
		* ex: Input=100Mi, Output1=40Mi to user A, Output2=60Mi redirected to myself
	+ pros and cons:
		+ easy double spend detection: an output block would be used twice
		+ much easier to calculate balance (scalable)
		- more complex to implement
- __charge-free transactions__
	+ enable micro payments 
- __local snapshots__
	+ nodes can only store a subset of the ledger history 
	+ nodes do not require expensive hardware 
- __fairness__ 
	+ all transactions are treated equally 
	+ you cannot pay to get a higher priority 
- __modularity__
	+ for easy redesign
- __reliable management__
	+ absence of miners remove some conflicts of interest
	+ operated by a non-profit organization
- __open source__
- __immutable__
- __data transactions__
	+ most of transactions will be just data stored on the tangle
	+ this data can be traded, collected, exchanged ...

# Use-Case 
- Microtransactions 
- Machine-to machine economy (M2M)
	+ with rise of AI, IoT becomes autonomous and can directly communicate and
		exchange with each other
- Digital identity
	+ decentralized digital identy protocol
- IOTA Stronghold
	+ collection of libraries for secure management of passwords and keys
	+ secure implementation using the IOTA protocol
	+ mutliple strongholds can work as a network and communicate and collaborate
- IOTA Streams
	+ protocol to send/communicate messages to IOTA nodes
	+ data transfer protocol that can be used for diff types of data
	+ way to exchange and control access to data in the tangel
- IOTA Access 
	+ open source DLT framework 
	+ build access control systems for smart devices
	+ ex:
		* remote car rental
			- like drivy but without the company fees
			- when paying a rental, access to the car is authorized through 
				access control
		* Wi-Fi access
			- only allow wi-fi for customers
			- can add pay-per-minute/bytes access
- Digital assets
	+ also called Colored Coins 
	+ you could for example label a IOTA token which represents ownership
		of a physical object
- Smart-Contract
	+ to create a smart contract 
		* assign it an address 
		* create a digital asset and send to the SC address
	+ send a request to a SC 
		* send a request coin/digital asset 
		* the digital asset will be converted back to a token once the SC processed 
			the request
		* SC keep the token
	+ 
- IOTA Oracles
	+ bridge between digital and physiscal world
	+ oracles provide blockchain with outside information
		* input for smart contracts 
	+ on IOTA data can be issued by the IoT itself
		* guarantee authenticity of the data 
		* usually you need an intermediary to put data on the ledger
- Flash Channels 
	+ offline payment channel for deposits and withdrawals
- data marketplaces 
	+ all data on the tangle could be operated as assets
- Digital twins 
	+ digital representation of tangible/intangible object from the real world

# Mana 
- in IOTA there are no miners/stakers who charge a fee for processing transactions
- to regulate the network, IOTA introduced Mana 
	+ defense against Sybil attacks
	+ for tools like FPC 
		* FPC is used by nodes to select a transaction between conflicting ones
		* Node opining is weighted by ther Mana
	+ autopeering
	+ congestion control
- Mana aims to give a ranking/reputation to nodes participating in the network
	+ to distinguish honest nodes with secured history from malicious nodes
- How to get mana
	1. by holding on IOTA tokens
		+ mana is affilitaed to IOTA tokens
		+ IOTA tokens generates mana over time
	2. by running a public node
		+ you earn Mana by processing a transaction for a customer
		+ mana received is proportional to amount transfered
	3. by renting mana from other addresses 
- Mana decays over time
	+ there is constant amount of mana as there is constant amount of IOTA tokens
- if network is close to its processing capabilities you require mana
		to make transactions
	+ Node processes messages at a rate specified by the protocol 
	+ people with Mana will be favored by the protocol
	+ people owning tokens or running nodes will be favored
- Difference with a fee in other chains
	+ you only need to pay a fee if you don't contribute to the network
	+ similar to PoS but you don't lock tockens
- 2 types of Mana
	+ access Mana: can be lent/leased
	+ consensus Mana: should not be lent/leased, used for voting weight

# Sharding 
- two main approaches around Sharding:
	+ Hierarchical 
		* 1 central element (root shard) which coordinates other shards
		* simpler to develop
		* scalability is limited by the root shard capabilities
	+ Non-Hierarchical
		* no central element
		* security and bandwidth control requires a complex solution
		* theorically infinitely scalable
- IOTA 
	+ __Data sharding__
		* hierarchical
		* only for data messages (transaction is not supported)
		* excluding value messages simplify a lot 
			- no need for consensus, mana...
		* hierarchical structure for proof of inclusion
			- able to prove that data exist in a shard without being part of it
		* Shard creation:
			- main tangle is the root shard
			- any node can start a new data shard 
			- data shard creation payload 
			- parameters
				+ access: permissionless/permissionless
				+ privacy: public/private
				+ sybil protection mechanism you want
					* mana, painted mana
				+ throughput: necessary for Congestion Control
			- you can create data shards out of data shards 
				+ they won't be linked directly to the main tangle
				+ the shards can have a parent/child relationship
				+ possible "branch shards" that just host other data shards
		* Confirmation:
			- _participation rule_: a node inside of a child tangle is also part of
				the parent tangle
			- confirmation is necessary because orphanage is still possible in shards
				+ bad timestamps 
				+ unlucky with TSA (Tip Selection Algorithm)
				+ "bad reputation" among high mana nodes
			- makes the system more future-proof ???
			- local confirmation is not enough since it does not mean anything to other shards
			- protocol 
				+ all nodes send "stamp" messages to parent tangle
				+ stamp contains:
					* id of the child-tangle 
					* id of the message inside the child-tangle
				+ message is confirmed in a data shard iff:
					1. it is approved by a message referenced by a stamp
					2. this stamp is confirmed in the parent-tangle
				+ recursive approach that ends up in the main tangle
					* you have a chain of hash from the main tangle to your message in 
						a data shard
					* you can store these hashed in a merkle tree for the participation rule
	+ __ISCP (Iota Smart Contract Protocol)__
		* based on Blockchain and SmartContract
		* hierarchical
		* parallel chains transactions anchored on the main Tangle
		* Pros and cons 
			+ allows value messages
			+ has total ordering
			+ can withstand MPS in order of millions 
			- capped scalability at a low number
			- smart contract validators are required
	+ __Fluid Sharding__
		* non-hierarchical
		* Sharding Space, S² we consider a globe
		* each node has a geoID which are coordinates in S²
		* each node has its own Radius of Perception
			- node only process messages from nodes in its radius 
			- you may change the radius 
			- more or less each node is its own shard

# Consensus
- Objectives/Requirements 
	+ Permissionless
	+ No stake
	+ Fairness for small and big players
	+ Scalability 
	+ Energy efficient
	+ Security
	+ Fast 
- How is consensus reached
	1. 2 Conflicting transactions are added by different nodes on their copy of 
		 the tangle
		+ each node may choose their preferred tip selection algorithm
	2. Transactions added are checked locally by the nodes
	3. Nodes broadcast their copy of the tangle to other nodes
	4. Eventually some nodes get both conflicting transactions 
	5. These nodes detect the conflict and report it
	6. Nodes use FPC to select the valid transaction
		+ vote is weighted by mana
	7. Future transactions will not reference the transactions considered as "invalid"
		+ if transaction A references a future double spend transaction B
			* A is valid
			* you don't want B to be referenced
		+ use approval reset switch 
			1. A will be put in a weak reference queue 
			2. a transaction that references A will use a weak reference 
			3. a weak reference increase the cumulative weight of A but not
				of its subtangle past 
				- meaning that B cumulative weight won't increase through A
	8. Nodes check locally number of references of pending transactions
		+ when a threshold is reached it confirms it as final and irreversible

# Fast Probabilistic Consensus (FPC)
- used by nodes to determine which transaction to choose among conflicting ones
- Objectives 
	+ if a majority of nodes prefer 1 then final consensus should be 1 with high probability
	+ low communcation complexity (compared to traditional BFT)
	+ presence of byzantine nodes
- Model 
	+ n nodes in the network
	+ qn malicous nodes
		* malicious nodes are omniscient
		* two possible behaviour
	+ (1-q)n honnest nodes
	+ protocol is divided into epochs
		* node can query k other nodes for their opinion
			- _k << n_
			- malicious and honnest nodes always respond to their query

# Congestion control 
- Objectives 
	+ Fairness
	+ Consistency 
	+
- We want nodes to send an average same number of transactions regadless of
	their computing power
- How are messages received?
	+ when node A has created a transaction t it gossips it to its neighbours
	+ Node B receives t then use a scheduler to choose which received transactions
		will be verified 
		* put message t in a queue dedicated to messages created/issued by A
		* scheduler dequeue a message from queues depending on the Mana of the nodes
- Prevent spamming from a node by putting a threshold on queues 
	+ if nodes are authorized 10tps/s 
	+ if queue of a node is bigger than a threshold then this node is blacklisted


# Economic Model of IOTA
- Contrary to other crypto, holding IOTA gives you access to network resources
	+ since transactions are free you may have huge number of transactions
	+ therefore you may be limited by the network capacity, TPS
	+ ppl with more Mana will be able to issue more transactions 
		* Mana is affiliated to IOTA tokens
- Ppl with tokens are able to rent their Mana if they don't use it
	+ other ppl may pay you to use your mana to issue transactions 
	+ mana rented decays over time 
	+ mana is regenerated by your tokens
- Similar to PoS 
	+ you earn money by holding on tokens
	+ difference is that in PoS rewards are paid out by inflation
		* block reward
		* you may not earn that much because your tokens are diluted by the inflation
	+ not the case in IOTA since amount of tokens is fixed
- Mana can be considered as a fee for people who do not help the network
	+ if you run a node or hold tokens you won't need to rent mana for transactions
	+ you will use your own mana

# Tip Selection Algorithm 
- old algorithm was a _cumulative weight based random walk_
	- calculating the cumulative weight is costly 
	- there is a probability that a honnest transaction is unlucky and
		is never referenced
	- opens up all kinds of attack vector (parasite chain, blow ball ...)
- current algorithm __weighted uniform random tip selection__ 
	+ with the white-flag approach you don't need a complicated TSA anymore 
		since we are allowed to reference conflicting transactions
	+ increase TPS, select tips which are non-lazy to maximize confirmation rate
		* non-lazy: tips which do not reference transactions too far in the past
		* referencing old transactions does not contribute to the rate of newly
			confirmed transactions when a milestone is confirmed
- Definitions 
	+ _solid_: past cone of transactions are in the database 
	+ _tail transaction_: transaction at index 0 of a bundle
											only tail transactions can be tips
	+ _tip_: solid tail transaction of a valid bundle without any approvers
	 			 its past cone only contains valid bundles
	+ _score_: likeliness to select a given tip
	+ _confirmed root transactions_: set of transactions that have been confirmed
		by previous milestone and that a given transaction may see first when
			walking its past cone
	+ _Transaction Snapshot Index (TSI)_: index of the milestone that confirmed
		a given transaction
	+ _Oldest Transaction Root Snapshot Index (OTRSI)_: defines the lowest milestone
		index of confirmed root transactions of a given transaction
	+ _Youngest Transaction Root Snapshot (YTRSI)_: the highest milestone index
		of confirmed root transactions of a given transaction
	+ _latest solid milestone index (LSMI)_: index of the latest milestone
	+ _Below Max Depth (BMD)_: threshold on which it is decided a transaction 
		is not relevant in relation to the current tips of the Tangle
		* ex: for a transaction if the BMD is bigger than _YTRSI - OTRSI_ 
- scoring, 
	+ transaction is lazy if:
		* current milestone index and its YTRSI is too wide (2 by default)
		* LSMI - OTRSI(tip) > 15
		* LSMI - OTRSI(approvee of the tip) > 7 (for both approvee)
	+ transaction is semi-lazy if:
		* LSMI - OTRSI(approvee of the tip) > 7 (for one approvee)
	+ else not lazy
- weighted random tip-selection
	+ we only select tips from non-lazy or semi-lazy 
	+ non-lazy are chosen with higher probability
- pros and cons 
	+ simpler algo which fits white flag
	- depending on how YTRSI and OTRSI are calculated may be more expensive
		than envisioned

	
# IOTA Smart Contract Protocol (ISCP)
- Principles and goals
	+ SCs are trustless and distributed computation on the ledger 
		* essentially state state machine which takes ledger state to another
	+ Requirements for smart contracts is an _objective state_
		* state equally perceived by all smart contracts
		* achievable by serializing state updates to the ledger by total order
		* it procudes the blockchain: total ordered chain of ledger updates
	+ SCs chain
		* be able to run many distributed smart contract chains in parallel
		* each ISCP chain:
			- is run under the consensus of its committee
			- is distributed
			- can host many smart contracts, sharing the same objective state 
				provided by the chain
		* a chain is functionnally equivalent to a blockchain
		* state of the chain:
			- balance of the native IOTA assets
			- __data state__ 
				* data required for SC in the form of key/value pairs stored outside 
					the ledger
					+ however hash of the data state is stored as a UTXO transaction 
						in the ledger
					+ ledger guarantees there's exactly one state output and
						the containing state transaction
					+ state output is controlled by the entity running the chain
				* storage:
					* _solid state_: when data state is stored in an off-chain database
						+ each validator node have its own database to store the data state
						+ the ledger store the hash and index of the last data state 
						+ if desync, validator node may need to recompute missing step 
							to be up-to date
					* _virtual state_: when data state is still in-memeory
						+ it's possible to have several virtual states in parallel
							as candidates
						+ one of them may be solidified
				* always has:
					+ hash state 
					+ time stamp 
					+ state index
				* data state stores data for multiple SCs 
					+ data for each SC is called partition
					+ keys of a partition are prefixed with 4 bytes of ID of the SC
					+ this 4 bytes ID is called _hname_ of the SC
					+ SC can only update its own partition
				* the set of mutations applied to the data state in one piece is
					called a __block__
					> next_data_state = apply(current_data_state, block)
			- properties guaranteed by the UTXO ledger
				* global consensus on the state of the chain 
				* state is immutable and tamper-proof 
				* state is consistent
			- state output contains: 
				* identity of the chain 
				* anchroing hash: hash of the data state 
				* state index
	+ _access nodes_
		* nodes which sync their states without calculating them
		* used as a public network access to the chain state 
		* allow to avoid DDOS on committee nodes
	+ on-chain accounts 
		* main invariant: __total asset on the chain are always equal to 
			the balances of colored tockens in state output of the chain__
		* chain keeps a ladger of on-chain accounts in the data state 
			- to keep balances of owners of the chain-token
		* each SC has an on-chain account which token can only be moved by the SC
		* __L2 chain uses account-based ledger model__ as opposed to L1 UTXO
	+ goals compared to Eth
		* functionnally equivalent to Ethereum smart contracts 
		* improve scalability
		* improve cross-chain communications
	+ cross-chain interations
		* through on-ledger request
			- a transaction with data and funds on the underlying UTXO ledger
		* through off-ledger request 
			- sent directly to the committee or access nodes instead of using 
				transactions of the UTXO ledger
			- allow higher TPS of SC transactions
- in Ethereum, smart contracts are __on-chain__ == part of the core protocol
	* meaning that they are executed and validated by all nodes in the network
	+ security of SC is propotional to network size 
	+ SC can transfer tokens from their account w/o providing a signature
	- SC scale poorly, must be executed by all nodes
	- every node keeps SC code and state
	- subjected to network transactions fees, which are roughly proportional 
		to the underlying token price
- __off-cchain__ SCs
	+ out of the core protocol 
	+ only a subset of nodes ( _committee_ ) need to execute them 
	+ consensus is reached outside of the core protocol
	+ pros and cons 
		+ SC do not burden the network 
		+ cost of SC is low and predictable
		+ level of decentralization can be adjusted 
		- to transfer tokens, SC must sign transactions to prove they
			have access to the account address 
		- security of SC depends on the committee
			* size, memebers, and entity that sets it up
- IOTA SCs are defined as _immutable state machines_	
	+ each SC has a state attached to the Tangle
		* each state update represents a state transition on the Tangle
	+ SC code and state are immutable 
		* state is updated by attaching new transactions to the Tangle
	+ we have a verifiable audit trail of state transitions
		* we can check validity of state transitions 
		* state transitions cannot be corrupted
- each SC has an owner
	* can be an entity/org/person ...
	* responsible for:
		+ create SC and submit to the network
		+ decides on size of the committee _N_ and select nodes which will 
			be part of it
		+ decides on consensus threshold to accept a result 
			* this number is called a _quorum_
		+ general config of the SC
	* the owner does not participate in running the SC
	* the SC account address is owned by the committee
		+ only a quorum of committee nodes can transfer tokens from the address
		+ __BLS multi-signature addresses__
			* gives nodes of the committee equal ownership over the address
			* enables _threshold signatures_
			* a quorum must sign the same transaction with their secret key 
				to produce a valid signature
- why create a SC? 
	* owner can configure a fee/reward in IOTA tokens 
	* owner and committee can receive the rewards
		+ committee earn rewards by processing SCs that offer rewards
		+ owner may add an option to receive a % of the reward 
	* another incentive to be in a committee is to build good reputation
		+ certain committee may require good reputation 
		+ encourage good behaviour
- SCs programs
	+ WASM is the main VM for IOTA smart contracts 
		* it can also be in any language or even hard-coded in the nodes software
	+ how does it work:
		* inputs: request transaction and current state transaction
		* output: next state transaction
						- always references the inputs used
						- we have a chain of state updates triggered by requests
		* a quorum need to produce a valid signature of the output for it
			to be validated 
			- using BLS multi-signature
			- only then the SC state is updated and money transfers are made
	+ owner creates the program and the binary code is stored in committee nodes
		* hash of the program is stored is always accessible through the 
			state transaction
- __committee__
	* responsible for:
		+ executing the program
		+ listening the Tangle for request transactions 
		+ updating the smart contract state
	* setting up the committee:
		1. owner decides on committee members 
		2. owner init a _Distributed Key Generation (DKG)_ process 
			 among committee nodes
			- create an account address controlled by the SM 
			- generate a private key for each committee node
			- can be centralized or decentralized 
				+ centralized: the owner can override the committee using
					his master key 
				+ decentralized: owner cannot override the committee decisions
	* handling a request transaction:
		- when detecting a transaction, execute the SC 
			+ committee only exchanges hashes and not results
		- if the nodes don't agree on next state
			+ produce an invalid signature 
			+ a consensus on the next state must be reached
				* each node may see a different state depending on its local
					clock or neighbours due to the asynchronous setting
				* the leader of the committee submit the final transaction to the 
					tangle
- __Alias Account__
	* the standard is to use address account (private/public key)
	* does not possess the necessary flexibility for SC 
		+ need a pool of addresses (committee) that may be changed over time
		+ no notions of control levels for address account
	* used to represent SC accounts
- possible issues 
	+ _state fork_
		* a conflicting copy of the SC state
		* happens if two or more transactions are attached to the Tangle and
			and update the state differently
		* may happen if the leader is temp  disconnected and come back when 
			another leader has been chosen
		* solution: 
			- when setting up the SC, a unique colored token called _smart contract token_
				is created and transferred to the SC address
			- each state update sends this SC token to the SC address
			- hence state updates form a non-forkable chain since the token cannot be split
	+ disk space usage 
		* keeping all the state updates transactions might be costly 
		* old transactions may be snapshotted away
		* in doing so old SC states and updates may be lost
		* the snapshot contains all the state info a transaction 
		* the snapshot becomes the start of the new chain
	+ limited size committee permanence is not guaranteed
		* owner may be able to enact contingency plans 
	+ storing and managing keys when nodes are part of multiple committee
		* we require an efficient, secure and scalable key management
- __IOTA Native Tokenization Framework__
	+ Motivation 
		* extend the capabilities of the base ledger to handle different assets
			- current IOTA (Chrysalis) only tracks IOTAs
	+ Original idea: _Colored Coins_
		* Minting is a single time operation and owners can't control the supply
		* anyone can burn colored coins
		* we require some meta-data for human interactions
	* _Token Requirements_
		- Tokens must have a unique identifier
		- Tokens with same identifier must be fungible with each other 
		- Non-fungible tokens must be uniquely identified and such token must be 
			unique in the ledger
		- Token issuers shall be able to control the supply of their tokens 
			+ Min/Max token supply
			+ Entity/entities of issuer(s)
			+ time 
			+ Minting/burning flow (quantity restrictions wrt time)
			+ ... 
		- Circulating supply of native token shall be accessible based on the ID
			of the token
		- Metadata of tokens tied to their ID shall be accessible on-chain
		- Transactions with native tokens shall be feeless just like with IOTA tokens
		- Native tokens shall not bloat the size of the ledger database (dust protection)
	* _Ledger limitations_
		- IOTA uses a UTXO accounting model
		- Instead of using a global state transactions consume outputs and
			produce new onew 
			+ advantages:
				* scalability: transactions can be made in parallel since they depend
					on local contexts
				* double-spends detection: easy to detect since two conflicting transactions
					will try to consume the same UTXO 
			+ cons: no shared glocal state accessible to transactions 
				* global invariants need to be enforced using procedural rules 
					- ex: fixed supply of IOTA is enforced by the fact that you can't 
								create funds out of thin air 
				* supply control depending on a global state is difficult which prevent 
					issuers to control its native tokens
	* __Alias Output__
		- alias is a special type of UTXO with an unique ID
			+ when consumed, the next one produced keep the AliasID, but its state is 
				udated
			+ can be seen as a state transition of the alias
			+ state transition rules can be enforced by the protocol
			+ AliasOutput is composed of: 
				* AliasID
				* State Controller
					- address or alias account, typically the committee
					- can transition the AliasOutput through transactions
				* Governance Controller 
					- is an address or an alias account, typically the owner of the SC 
					- can:
						+ destroy that alias output
						+ assign the state controller 
						+ assign the governance controller
				* Immutable metadata
				* State metadata
					- mutable
				* token balances
					- mutable
		- extended/alias outputs can also define:
			+ a fallback account/deadline 
				* in case of a dormant SC you may want to get back your money
				* deadline may be over a constant defined by the network to give 
					the SC enough time to pick up the request
			+ a timelock, to schedulate an operation for a later time
	* __Example of a simple supply control scheme__
		+ X wants to issue a private currency called "foo dollars" (FOOD)
		1. Alias creation
			+ An alias is created which private keys are held by X 
			+ ID of this alias/foundry becomes _X_ID_ 
				* X_ID = hash of the output which created it
				* everyone knows that X_ID belongs to X, since public keys were
					published in X reports
		2. Supply Control Scheme 
			+ X want to use a scheme (42) of IOTA allowing owner to mint/burn 
				unrestricted amount of native tokens
				* 42 corresponds to the supply control rules scheme we want to use
			+ X constructs as asset id as 
				> AssetID(FOOD) = '42' + 'Foo Dollars'
				> TokenID(FOOD) = X_ID + AssetId(FOOD)
			+ TokenID contains: id of foundry, iota scheme used, name of token
			+ global state of AssetID will be stored in a map of the alias 
				created previously
				* add/delete of entry in map will correspond to the minting/burning
					of a whole new AssetID
				* updates of entry AssetID corresponds to mint/burn of some AssetID
				* structure in the map corresponds to the supply control scheme used
					- in our case: _map['42 Foo Dollars']: 500_
					- most simple case where we only store current circulating supply
		3. Minting Transaction 
			+ X mints the first batch of FOOD native tokens with a transaction
				* 1000 IOTA tokens on the input side
				* X_ID is unlocked on the input side and present on the output side
				* output side contains 500 IOTA tokens and 500 TokenID(FOOD)
			+ network processes and validate the transaction
		4. Basic Transaction Validation
			+ Check if the tag from IOTA to TokenID(FOOD) is valid
				* first N bytes must define the foundry identifier (X_ID)
				* check if X_ID is present among the list of inputs
				* check if unlocking conditions are fullfilled 
			+ check the unlock condition for every input
		5. Unlock Validation
			+ inside the unlock check of X_ID we have access to the transaction 
			 	context
			+ we can see that tokens were minted/burned that map to X_ID
			+ we can assert that subsequent alias output with the same ID was created
		6. Transacting with Native Tokens
			+ used as base IOTA tokens
			+ only the issuer is allowed to burn tokens 
				* he needs to own them to burn them, might need to buy them back
			+ user can check the supply control scheme and current state of suppy 
				in the controlling alias
	+ __Wrapped L2 Assets as Native Tokens__
		* each layer 2 chain is represented as an alias output on Layer1
			+ with Token Foudry Feature we make it possible for SC to mint
				their own L1 native assets
		* one common use is wrapped assets 
			+ ex: ERC-20 token living in an EVM ESCP chain
				* these tokens have a wrapped representation of the tokens on L1
				* tokens can the be traded feelessly on L1
				* or sent to other smart contract chains
		* analogy of L1 and wrapped L2 assets with €
			- European Central Bank (ECB) governs a smart contract 
			- National banks controls ERC-20 L2 smart contracts 
				* they own all the funds since theyr control the L2 smart contracts 
			- banks can decide to wrap their L2 € into L1 assets 
				* essentially printing cash 
				* L1 assets are only owned by owner in L2 
				* can be used without fees 
			- returnin L1 assets to the ECB is equivalent to deposit cash into the bank
- __Smart Contracts__
	+ Structure of the SCs chain
		* there are _core contracts_ which are SCs deployed by default when 
			creating the chain
		* other SCs are deployed by sending requests to the _root core contract_
			- it keeps a registry of deployed SCs
			- performs factory function
	+ Structure of the SCs
		* immutable _smart contract program_
		* identified by its _hname_: 4 bytes id 
			- usually hash from the name of the SC
			- hname = 0, is reserved for the default smart contract
		* partition of the chain data state restricted by the VM
		* associated on-chain account of tokens it controls
			- _AgentID_ of the account = chainID + hname
		* Entry points 
			- functions that can be called
			- identified with the hname of its name or signature
			- two types 
				+ _full entry points_
					* may call another SC of the same chain synchronously
					* or send request to other chains
				+ _view entry points_
					* read-only access to the state
					* can call other vien entry points
	+ Default Smart Contract
		* 6 smart contracts are deployed automatically
		1. _Root SC_
			- maintain the fundamental parameters of the chain: chainowner_id
			- deploy new SCs and maintain the registry of these
			- management of fee handling
		2. _Accounts SC_
			- maintain the on-chain ledger of accounts 
		3. _Block SC_
			- maintains a registry of big binary data objects: Wasm binaries
			- used by root to deploy SCs
		4. _Blocklog SC_: keeps track of all blocks of requests
		5. _Eventlog SC_ : maintains on-chain log of evens emitted by SC
		6. _default SC_: called when target contract is not found
		* these built-in may be seen as part of the VM itself
	+ Interaction between SCs
		* in the same chain 
			- call the SC hname and the entry point hname
			- with parameters and funds
			- sandbox ensures: 
				* funds are enough for the transfer
				* proper state context for the called contract
		* cross-chain messaging
			- on-chain request
				* same as with a user, through UTXO ledger 
				* sender is identified with the chain_id and the hname of the issuing SC
- __The distributed system__
	+ Computation of SCs by committees
		* state transition can be reran to verify the state of a chain 
			- hash of state, request and SCProgram are immutable on the ledger
			- you are capable of rerunning the state transitions if necessary
		* instead of rerunning SCs, trust comes from a consensus through the
			distributed processor (committee)
			- consensus using quorum majority in BFT setting
			- for N nodes in the committee we determine threshold T ∈ ]N/2; N[
			- Consensus is reached if more than T nodes produce the same result
			- Rest of nodes may be faulty, malicious or unavailable
			- usually T = floor(2N/3) + 1 is an optimal value
				* T > N/2 is a must to avoid having two quorums majority
		* even in the case where an attacker mangae to manipulate consensus, 
			an audible trail on the Tangle can be used to verify the calculations
		* __even honnest nodes may see some factors differently and produce 
			different results__
			- each committee node has its own access to the UTXO ledger
				+ connected to different IOTA nodes 
				+ they don't see the exact same thing such as token balance of an address
			- each node has its own local clock
			- requests may reach the committee node in an arbitrary order with
				arbitrary delays
			- before calculations, __nodes need consensus__ on:
				+ state output: current state of the chain
				+ timestamp to be used for the next state transaction
				+ ordered batch of requests to be procecessed
				+ address where node fees must be sent (if necessary)
				+ mana pledge targets
	+ Proof of consensus 
		* after computing committee has to anchor the state updates
			- the anchor transactions contains: state transition, the AliasOutput
			- this must be signed by the quorum of nodes
		* BLS threshold signatures
			- BLS crypto with threshold signatures in combination with polynomial 
				Shamir secret sharing
		* Distributed Key Generation (DKG)
			- to produce a valid threshold signature nodes must have the corresponding
				private keys
			- at the formation of the committee a DKG procedures is held to
				produce the keys 
			- Rabin-Gennaro algorithm is used as Verifiable Secret Sharing (VSS
		* Consensus algorithm for ordered batch requests
			- ust BFT algorithm which guarantees consistency and BFT if less than
				1/3rd of nodes are malicious
			- take Asynchronous Common Subset (ACS) of HoneyBadgerBFT algo
				* HoneyBadgerBFT don't make any assumptions on asynchronous model
					- no guarantees that a any msg will eventually be delivered 
					- even with this assumption current protocol are too slow
				* moreover they use an atomic broadcast protocol
				* use threshold public-key encryption
					- this tool usually is undesirable for computation time 
					- but good when network resource are low
			- ACS is performed by sharing proposal using Reliable BraodCast (RCB) 
				and then agreeing on whose proposals will be included into the final set
			- agreement is performed using Byzantine Binary Agreement (BBA) protocol
				+ two round consensus protocol
				+ when communicaiton is costly
				1. everyone broadcast their choice value
				2. tell if they have come to a conclusion 
					- yes: they stick to their choice 
					- no: they choose default value
			- to cope with FLP impossibility theorem the BBA algo uses a Common Coin (CC)
				as a source of the randomness in the algorithm
			- weak asynchronous model is assured by resending messages until 
				an ACK is returned
			- each node supplies to the ACS its batch proposal
				+ set of requests ids
				+ a timestamp
				+ consensus and access mana pledge addresses 
				+ fee destination 
				+ partial signature for generating non-forgeable entropy
			- upon termination of the ACS each committee node gets the same set of 
				proposals where 
				+ each request in the set has been proposed by at least F+1 nodes 
				+ timestamp is a median of proposed timestamp
				+ access/consensus mana and fee destination is chosen using 
					a deterministic random function on the proposals
				+ entropy is taken by aggregating the partial signatures in the
					agreed proposals
			- all honnest nodes will calculate the same consensus batch of requests
- __Deployment of the chain__
	1. Selection of the committee
		+ may be done by an algorithm or manually
	2. DKG 
		+ each committee memeber get a private key
		+ generates the _state/controlling address_, the partial public 
			keys and the master public key 
		+ state output will be unlocked only by signing it with the private keys
			behind the state address
	3. Creating the origin transaction with origin output on the tangle
		+ the initiator creates the origin transaction with _origin output_
		+ the origin output does:
			* starts the chain
			* mints the _alias address_ which will be used as chainId
			* contains the state address genereted during DKG 
			* contains a state hash of the initial state
	4. Creating the init request transaction by the initiator
		+ after confirming the origin transaction 
		+ this will request data for initialization of the root smart contract
			* init the state on the chain with chain_owner_ID equal to the initiator
		+ will mark the chain as fully functionnal
	5. Activating the SC
		+ chain needs to be activated on the committee Wasp nodes
		+ chain is activated upon node start from the registry of the node 
		+ chain pulls the backlock from UTXO and processes the init request
	6. the init request transaction is processed by the root contract
- __Virtual machine__
	+ deterministic
	+ two parts: the VM and VM plugins (processors, SCs)
	+ VM abstraction
		* generic interfaces that make ISCP and Wasp nodes agnostic about 
			what kind of deterministic computation machinery is used to run SC programs
	+ VM plugins 
		* VM invokes VM plugins to perform user-defined algorithms: SCs
	+ processor
		* attached to the VM through the generic VMProcessor interface
		* exposes callable entry points
		* but have a default entry point when no entry point was specified
	+ _VMType_ abstraction 
		* VMTypes defines a specific implementation fo the VMProcessor interface
		* constructor creates a VMProcessor form the program binary
		* for an SC VMTypes defines an interpreter of the SC in its binary form
		* each VMTypes is statically built into the wasp node
	+ default VMTypes 
		* builinvm, native SC processor, hard coded into WASP 
			- core contracts are of this type 
		* wasmtimevm, VM type which loads SC programs from WASM binaries
		* examplevm, hard coded SC processors for all kind of examples
+ Calling the VM
	* common inputs 
		- State output of the current state chain
		- Solid data state of the chain 
		- a batch of requests to process 
		- Timestemp
		- Others (Fee destination, mana pledge targets...)
	* returns 
		- the block: batch of state updates (one state per request)
		- essence of the state transaction 
			+ next state output
			+ timestamp = timestamp(previous_state) + nb_requests * nanoseconds
	* for each request, VM locates the target VM processor (SC program) and
		calls the corresponding entry point
		- if SC does not exist, calls the default processor 
		- if target entry point does not exist, call default entry point
	* Called entry point of smart program have access to its partition 
		in the chain's data state via Sandbox interface
		- limited and deterministic access to the state through a key/value
			storage abstraction
	* VM handles fees and exceptions outside the plugins
		- each call to SC program means creditinof token balances provided in
			the call parameters into the on-chain account of the SC
	* all updates to the state are collected and only commited into the database 
		until confirmation of the anchoring output on the UTXO ledger
- Deploying a SC
	1. Store the SC by sending a StoreBlob request to blob SC
		* core SC containing the different user-defined SC immutable data (program 
			data)
		* blob data is accessed on the chain by its hash computed from the blob data
		* VMType is specified as a part of the blob data
	2. Deploy the SC
		* call _DeployContract_ of the root SC
			- hash of the blob 
			- name of the contract
			- description
		* root contract finds the binary code in the blob registry
		* load it into the VM processor using the VMType
		* root contract calls the init entry point of the new VM processor
- __Wasp Node__
	+ ISCP is ran by a network of Wasp nodes
	+ to access the UTXO ledger on the Tangle, Wasp nodes connects to a GOshimmer 
		node on the network
		* one Goshimmer node can connect to many Wasp nodes
	+ to reach consensus and to sync state, Wasp nodes send messages among themselves
		directly without involvement of Goshimmer nodes
		* they have their own network
	+ Wasp subscribes to a Goshimmer node to receive all confirmed transactions 
		containing outputs to the alias address of a chain it is running
- __Off-ledger request__
	+ directly to a Wasp node using a provided API
		* contains same data as on-ledger request
		* contains mandatory increasing counter to prevent replay
		* contains ED25519 digital signature of the request's data essence by the sender
	+ to be accepted 
		* have an on-chain account on the target chain with a non-zero balance
		* must be signed with private key correponding to the on-chain account
		* chain keeps a counter of off-ledger request
			- against replay attacks
		* fees will be taken directly in the issuer on-chain balance
		* wasp nodes receinving the request will whisper is to other committee who
			will check the request too

# Attacks 
- __Conflict Spamming Attack__
	+ recipe 
		1. an attacker submits 2 or more conflicting transactions at the same time 
		2. these transactions will be refered by honnest transactions while the
			 the conflict is still undetected
		3. this introduces a tangle split where a set of transactions 
			will need to be re-attached when the conflift is solved
	+ Solutions 
		* __White Flag__
			- what is it?
				+ allow invalid/conflicting transactions to remain in the DAG
				+ when a milestone is received, the transactions since last milestone 
					are sorted over a total order
					* use a topological ordering
					* on any properties (alphanumeric on hash ...) but the same for all
						nodes
				+ apply the transactions following the sort done previously 
					* transactions that conflict with previous transactions are dropped
					* conflict with burrent bundle or previous milestones
				+ all the not-ignored sorted transactions will be used to 
					compute a merkle tree H
				+ H will be used by the nodes to make sure that their list of not-ignored
					nodes is coherent with the coordinator milestone
			- pros and cons 
				+ faster TPS 
				+ eliminate conflict spamming attack directly, conflicting transactions 
					are not treated differently
				+ no need to re-attach and promote since there are no invalid branches
				- kind of lose the DAG structure since the Tangle is defined at milestones
					and transactions are sorted 
				- transactions in the tangle may not be valid
				- computation of merkle tree by the coordinator and the nodes is expensive
- __Dust outputs__ 
	+ in UTXO model, nodes of the network keep tranck of all the current 
		unspent outputs
	+ the number of outputs may get too large which cause memory & performance issues
	+ this may happen if there a lot of transfers of a very small input of IOTA
		* the UTXO block are split to create the small transfer
		* we call these small transactions _dust outputs_
	+ __Dust protection__
		* the goal is make accumulating dust outputs expensive
		* node must have an unspendable deposit proportional to its number
			of dust outputs
		* roughly 10outputs for each 1Mi deposited
		* maximum with 100 outsputs per address
		* drawbacks 
			- there can't be addresses holding less than 1Mi 
			- a service receiving micropayments have to take the necessary
				measures to receive them properly
- __Forced address reuse__
	+ an adversary pays a small amount to addresses that have already 
		been used
	+ the adversary hopes that the owner of the used address will use the
		small payment as inputs to a larger transaction
	+ this will reveal other addresses that the owner may possess
	+ Dust protection is a good protection since the used address cannot 
		receive small transactions without a deposit
- __Blow Ball attack__
- __Eclipse Attack__
	+ a malicious actor isolating a specific user/node 
	+ create an artificial environment for the node by manipulating all
		the input and output info it receives
		* allows to manipulate his behaviour too
	+ how-to perform the attack:
		1. deploy a phantom network using botnet
		2. flood the target with botnet ip 
		3. make the target disconnect (DDOS for example)
		4. when reconnecting it will reconnect with IPs received previously 
			and be trapped in the phantom network
	+ what for? 
		* gather more mining power for a side-chain
		* make victims validate double-spend
	+ how to defend?
		* random neighbour node selection
		* deterministic node selection
		* increased node connections
		* new node restrictions (prevent flooding the network)
# Stateless client 
- memory issue is that state is easy/cheap to grow
	+ nodes need more hardware requirements 
	+ do not cunfund state and history
		* state is necessary data to process incoming request
		* history is only useful for checking/tracking 
	+ state may contain (in Eth):
		* account balances and nonces
		* contract code 
		* contract storage 
		* consensus-related data
- a solution is to go __stateless__:
	+ nodes don't need to store the state to verify blocks
	+ block/transaction come with __witnesses__
		* witnesses are proofs of the values of the state getting accessed
		* state will be computed into a Merkle Patricia Tree
		* witnesses will be a set of Merkel branches representing the
			value necessary to the execution of the block/transaction
	+ two forms of statelessness: weak and strong
		* __weak statelessness__ 
			- block producers need full state to generate witnesses 
			- block veryfiers can be stateless and only store state roots for blocks
		* __strong statelessness__
			- no node needs full state
			- transaction senders provide witnesses and store portion
				of the state necessary to generate the witnesses
			-  block producers will aggregate the witnesses to the block
- another solution is __state expiry__
	+ state that is left untouched becomes inactive/expired
	+ inactive state is not deleted but can be resurrected making them
		active again
	+ time before expiration: fees for time-to-live, refresh by 
		touching, intervals
	+ mechanism for expiration: 
		+ single state tree with inactive subranch 
		+ move inactive branch of the state tree to an inactive state tree
	+

# Permanode 
- Current permanode storage ~ 2.6 TB/year
	+ too much data to store:w
- transform permanode into a selective permanode
- to solidify a message you require its past cone, the permanode queries
	other nodes or permanodes to get complete archive
- in current Chrysalis, message are issued by milestones from the Coordinator
	+ without coordinator, this job could be done by selective permanode
- motivation: 
	+ you don't want to keep ALL the data
		* users select which messages they want to keep in Chronicle
	+ you don't need to store whole subtangle 
		* only path to closest referencing milestone
		* path between milestones always exist
- Selective permanode design:
	+ user defined filter for persistent messages
	+ API calls to prune the chronicle database
		* can be called manually by users
	+ API call _store_message(msg_id)_ to persist fresh message
	+ the selective permanode will query other node/permanode for missing data
		but required by users
	+ in permanode all kinds of table were created for convenience 
		* addresses, indexes, message ids ... 
		* now certain tables will be optional
	+ traceable selective message paths
		* to ease tracing messages from their closest milestones
		* also keep intermediate messages between selected message and milestone
			in 3 different categories 
			- inclusive-parent-location path
			- inclusive-hash path 
			- full-proof path 
+ value-transaction design 
	* selected transactions will be included in the payload of a value transaction
	* pros and cons 
		+ suitable for tangles both pre and post coordinator
		- to store a message you need to pay
		- lots of value transactions if many stored messages
		- node needs to make sure that value transaction is not orphaned
		- not scalable because each node will output its selected messages
		- is selected messages are gathered into a single payload, then future
			pruning is not possible
+ ISCP-based design
	* message and associated paths will be on ISCP chain directly
	* pros and cons 
		+ suitable for pre/post coordinator
		- additional ISCP needs to be ran with the selective-permanode
		- the SC needs to be well-defined to prune the ISCP chain
		- ISCP not ready yet
			
# UTXO ledger
- much more scalable than keeping account balances
	+ no global state, transactions can be done only seeing local UTXO
	+ double spend are easily spotted (2 transactions from the same output)
- design
	+ __Transaction__
		* composed of:
			- ordered list of inputs 
				+ each input is a reference the consumed output
			- ordered list of unlocks block 
				+ one unlock block for each input
				+ data necessary to confirm access to the consumed output (signature, ...)
			- ordered list of outputs 
			- timestamp
			- access mana pledge 
			- consensus mana pledge 
		* transaction is present in the UTXO ledger iff
			- all inputs reference an existing output on the ledger 
				* transaction spent/consumed the output 
			- no more than 1 transaction can consume an output
			- the transaction itself is valid according to its validation rules 
				* validation rules coming from outputs, inputs, ledger
				* example: timestamp of a transaction must be higher than 
					timestamp of any of its input
	+ UTXO Dag
		* UTXO ledger is a DAG
		* adding a transaction to the ledger is a transition of the ledger state
		* consistency of the ledger state is guaranteed by validity of the transactions
		* consumed outputs may be pruned in the _snapshotting process_
		* only unspent outputs are guaranteed to be present in the ledger DB
	+ Types of outputs
		* many types of outputs are supported 
			- each type has its own validation constraints hardcoded into the ledger logic
			- some unlocking options are described directly in the output
		* invariants of all types of outputs 
			- Output_ID
				+ 34 bytes = Transaction_ID (32 bytes) +
										 index of the output in the transaction (2 bytes)
			- Token balances: digital assets attached to the output
			- Address: ID of the entity allowed to spend the output
				+ hash of the public key of an account
				+ alias address (for ISCP)
		* 4 types
			- _SigLockedSingleOutput_: with iota tokens only
			- _SigLockedColoredOutput_: with any coloured balances 
			- _AliasOutput_: used by ISCP to build smart contract chains
			- _ExtendedLockedOutput_: used by ISCP to represent on-ledger request 
				between chains
				+ superset of SigLockedColoredOutput
	+ Tokens of the outputs
		* output must contain at least one colored balanced which is not zero
			- i.e all UTXO transactions are value transfers
		* color is blacke2b hash of the outputID of the minting output
		* ouptut may contain balances of multiple colors
		* invariant
			- constant total supply of tokens on the ledger
	+ Unlocking inputs
		* for a transaction to be valid each input must be unlocked
		* most common unlock block is a the _signature unlock block_
			+ to unlock inputs which ref output with addresses representing
				a private/public pair
				- contains a digital signature (ED25519 or BLS)
				- inputs, outputs, timestamp and mana pledge
			+ one signature unlock block may unlock multiple inputs ref UTXOs owned
				by the same address
		* another unlock block is used to unlock UTXOs owned by alias address
- __Implementation of chains on UTXO ledger__
	+ Chain representation
		- Chain on the UTXO ledger is identified by an _alias address_
			* also knwown as _chain address_ or _chain id_
			* this id/address cannot be changed and is used to be located on the ledger
		- the chain is represented on the UTXO ledger by a unique _AliasOutput_
			* special output unique to the ledger that keep state of the chain
	+ AliasOutput type
		* contains 
			- like normal output: outputID, token balances, address 
			- alias address: unique address which is minted at creation
				+ blake2b hash of the outputID of the origin alias output
				+ identity of the chain of outputs: chainID
			- state/controlling address: address of the entity allowed to 
				consume the output
			- immutable data: byte array
			- state data: byte array
		* consumed only in a transaction 
			- containing exactly 1 alias output with same alias address and immutable data
			- alias outputs form non-forkable chains on the UTXO ledger
		* unlocking the AliasOutput
			- consumed as input of the transaction by the usual SignatureUnlockBlock 
				corresponding to the state address
			- only the entity which controls the private key behind the state address 
				can create the next alias output
			- state address may be swapped when changing the committee of validators
	+ __on-ledger requests__
		* message composed of:
			- id of the sender 
			- target: chain, SC on the chain, entry point of the SC
			- attached tokens transferred with the request 
			- parameters of the request
		* inter-chain entity, used for cross-chain transactions and 
			for sending tokens to a SC by ordinary wallets of the UTXO ledger
		* VM process requests in batches
			- VM takes each request, the SC program, current data state and produces a 
				state update
			- one batch of requests produces a _block_: sequence of state updates
		* Requiremetns 
			- Guaranteed delivery: message must always reach the target
			- Atomicity of backlog: request must disapper from the batch on unprocessed
				request in the same atomic event as the state update is produced
			- Fallback: if request is not processed until optional deadline, the
				fund is returned to the sender
		* Reqests as UTXOs
			- requests are represented by one UTXO of type _ExtendedLockedOutput_
				+ invariants fields: outputid, token balance, address of the target chain
					* address would be an alias address (address of the target)
					* token balances represent the funds of the request 
				+ output payload
					* target smart contract ID: 4 bytes of hname
					* target smart contract entry point: 4 bytes of hname of the function 
						name or function signature
					* key/value pairs of request parameters
				+ fallback data: optional, if the reqeust is still not processed 
				 	after timeout
			- many requests may be put in the same transactions
		* unlocking the request
			- you have to add the AliasOutput in the same transaction
			- where the unlock block of the request refer to the AliasOutput
			- with fallbacks 
				+ the unlock conditions change when the deadline is passed
		* processing a request 
			- corresponds to consuming the ExtendedLockedOutput request
				+ atomic
				+ VM is responsible for atomic state transition of the chain
			- result of processing is reflected in the changed total assets and state
				hash contained in the state output

				
# Issue from 2018 Whitepaper 
1. Fixed POW Rate control
	- too low pow => no honnest majority 
	- high pow => hard entry level 
	- no guarantee of security or access 
	=> Solved using Coordinator
		+ low pow with security guaranteed by the coordinator
2. Conflict Spamming 
	- Conflicts => many branches => orphanage 
	=> Solved with Crysalis Coordinator
		+ milestones totally order the tangle 
3. Performance
	- Random walk is SLOW
	- Orphanage rate
	- Ternary Slow
	- WOTS (one time signature)
	=> Solved with Crysalis Coordinator
		+ milestrones maximise past cone size
		+ UTXO
		+ Elliptic Curve Cryptography

# Layers of IOTA 2.0 protocol 
- Network layer (Bytes) 
	+ P2P communication
	+ Neighbor Selection 
	+ Peer Discovery
- Communication Layer (Message)
	+ Tangle 
	+ Rate Control 
	+ Congestion Control
- Application Layer (Payload)
	+ Core applications 
		* consensus applications 
	+ Third party applications
# Note 
- Tangle have value and data transactions 
	+ actually ledger state (only value transactions) forms
		a hidden DAG 
	+ tangle is much more than a tangle
		* since transactions choose 2 transactions to attach to 
			it is a form of virtual voting


# Stronghold 
# IOTA Stronghold
- Stronghold is a software implementation which purpose is to isolate digital
	secrets from unintended people/usage
- Four main components 
	1. Client: high-level interface to Stronghold
		+ uses __Riker Actor model__
			* concurrent computation model where everything is an actor
			* actors are the primitive of the model
				- own their own private state
				- can create more actors 
				- communicate with other actors with messages
				- react to messages received
			* Actor model and process calculi are closely related to the modelling
				of concurrent digital computation
			* is idle most of the time and use a CPU thread when they have 1 msg or more
				in their mailbox
		+ client API 
			* create stronhold, init and switch actors actors
			* 2 memory storages:
				- vault (stronhold memory): write, delete, garbage_collect, 
					list_hints_ids, check_existence_
				- store (insecure cache): write, read, delete
			* snapshots: entire state of the Strongold can be be stored and read
	2. Engine: combines a persistence store (Snapshot) with an in-memory state
						 interface (Vault) and a key:value read/write (store)
			* 3 modules:
				- snapshot
						1. header: with version and magic byts for file-type detection 
						2. body: ephemeral public key and, xchacha20 tag and cipher text
					+ you can derive a decryption key from a user provided password
				- vault 
					+ secure database for secrets
					+ a collection of vaults is a stronhold
					+ a vault is collection of record encrypted using the same key
					+ Records may be garbage collected
				- store
					+ key/value cache for the engine
					+ expiration timestamp can be set
					+ use to store general unencrypted dat
	3. Runtime: a process fork with limited permissions within which crypto 
							operations take place 
		+ provide utilities for performing secure computions
		+ concerns
			* guarded memory allocations 
			* assists with read/write protecting sensitive data
			* zeroes allocated memory when handing it back to the OS 
			* uses canary and garbage values to protect the memory pages
			* leverages NaCl for use use on all supported platforms
		+ 3 types of data 
			* GuardedVec: variable-length data of the heap
			* Guarded: fixed-length data of the heap
			* Secret: data of the stack
		- guarantees for GuardedVec and Guarded:
			+ segfault when accessing without a borrow 
			+ different protections: noaccess, readonly (immutable 
				borrow), readwrite (one mutable borrow) with mprotect
			+ guard pages for overflows and large underflows
			+ canary to detect small underflows
			+ memmory is locked with mlock
			+ memory is zeroed when no longer used 
			+ guarded types can be compared in constant time
			+ guarded types cannot be printed
			+ interior data of a guarded type may not be clone
			+ GuardedVec can be (de)serialized
		- guarantees for Secret
			+ memory is locked with mlock 
			+ memory is freed when munlock is called
			+ memory is zeroed out when no longer in use
			+ values are compared in constant time 
			+ values cannot be debugged or cloned
		- Zeroing allocator
			+ wrapper around rust memory allocater
			+ add memory zeroing step to the dealloc process
	4. Communication: Enable Strongholds in different processes/devices to 
										communicate with each other securely
		- based on the rust-libp2p library 
			+ protocols, specifications and libraries for p2p network app
		- behaviour and swarm 
			+ multiplexinf following the Yamux spec 
			+ encryption of the communicion using noise protocol 
			+ multicast dns: for peer discovery 
			+ identify protocol: identifying info and listening addresses when connecting
				to a new peer
			+ request-respopnse protocol: each request between peers expects a response
		- communcation actor continuously polss for events, requests
		- Firewall, checks permission of each outgoing/incoming requests
- system to securely manage secrets 
- the framework is composed of multiple crates:
	+ vault: logic and abrastactions for the storage
		* secure database for secrets
            - collection of records encrypted using the same key
		* a database view, is a vault which is a 			
        * DbView uses GuardedVec type 
			- garbage_collect_ult to 
			- get_guard
		* a stronghold is a collection of vaults
	+ snapshot: method for storing the state of the vault in a file
	+ store: simple uncencyrpted storage protocol, key-value pairs
        - data may have a lifetime for garbage collection
        - one store is mapped to one client
- DbView : view over the data inside a collection of vaults
	+ write 
	+ get_guard
	+ exec_record
	+ garbage_collect
- Vault 
	+ revoke 
- __Record__ : piece of data inside a vault

## Client Structure 
### Stronghold Interface 
- Structure: Addr<Registry> 
- classic api
  - init_stronghold_system: new instance of the system asynchronously 
    + init a registry actor 
    + create 1st secure actor, generate clientID from path
    + records itself in the registry 
  - _spawn_stronghold_actor_ : spawn new client 
    + create ClientID
    + register it in the registry 
    + switch the actor target to new client 
  - _switch_actor_target_ : switch registry current client actor
  - _write_to_vault_ : create vault if it does not exists
  - _write_to_store_ : writes into an insecure cache
  - _read_from_store_
  - _delete_from_store_ 
  - _delete_data_ : 
    + revoke data from specified location (a vault)
    + revoke data is not readable and will be swept by the garbage collector 
  - _garbage_collect_ : manually start the gc
  - _list_hints_and_ids_ : list of available records id and hint
  - _runtime_exec_ : executes a runtime command given a procedure
  - _vault_exists_
  - _read_snapshot_ : read data from a given snapshot
    + get secure actor matching the client id 
    + get snapshot actor 
    + query the snapshot actor given: key, filename, path, client id
    + reload the secure actor with the data in the snapshot
  - _write_all_to_snapshot_ : write entire state of the Stronghold into a snapshot
    + all actors and their data will written into the snapshot 
    + encrypted with a key 
    + filename and path can be specified
  - _kill_stronghold_ : kill a stronghold actor or clear a cache
    + you should not kill the current active client
  - _read_secret_ : read data from a vault
  - _switch_client_ : private function used by API
- p2p api 
  + _spawn_p2p_ : spawn the p2p-network actor and swarm
  + _stop_p2p
  + _start_listening_ : start listening on the swarm to the given address
  + _stop_listening_
  + _get_swarm_info_ : peer ir, listening addresses, connection info of local peer
  + _add_peer_
  + _add_dialing_relay_ : relays are used when a peer cannot be reached directly
  + _start_relayed_listing_ : establish a connection to a relay
  + _remove_listening_relay_ 
  + _remove_dialing_relay_
  + _set_firewall_rule_
  + _remove_firewall_rules_
  + _write_remote_vault_
  + _write_to_remote_store_
  + _read_from_remote_store_
  + _list_remove_hints_and_ids_
  + _remote_runtime_exec_

### Actors
- Secure Actor
- P2P
- Registry
- Snapshot 

#### Secure Actor/Client
- Responsible for managing Vaults
- Struct 
  + keystore: hashmap of VaultId, Key
  + db: view on the vault entries
  + client_id 
  + store
- procedure calling for old crypto client
  + SLIP10Generate , generate SLIP10 seed
  + SLIP10Derive , derive a child key from a seed or parent key
  + BIP39Recover, BIP39 mnemonic sentence to create/recover a BIP39 seed
  + BIP39Generate, generate BIP39 seed and its corresponding mnemonic sentence 
  + BIP39MnemonicSentence, read a BIP39 seed and its mnemonic sentence 
  + Ed25519PublicKey, derive a public key from given private key
  + Ed25519Sign, sign given msg with given private key

#### Registry Actor 
- keeps record of all client actors bound to a unique _client-id_ 
  + SecureClient actors can be managed through their actix::Addr
  + registry can also be used to query the snapshot actor 
  + the registry actor owns/manages client actors

#### NetworkActor
- responsible for network communication 
- Requests for Secure Client
  + List ids
  + Vault: check, read, create, write to remote, check record
  + Store: read, write, delete 
  + garbage collect, clear cache 
  + call procedure

#### Snapshot Actor 
- Snapshots: key, filename, path, id, fid

## Get a GuardedVec
- DbView::get_guard -> Vault::get_guard -> Record::get_blob -> GuardedVec
- GuardedVec has single field Boxed

## Guarded 
- API 
	+ functions
		* borrow
		* borrow_mut
		* new
		* random
		* size
		* try_new
		* zero
	+ trait 
		* clone 
			- derived
		* debug
		* Eq 
		* _From<&'_ mut T>_
		* PartialEq<Guarded<T>>
		* Send 
		* StructuralEq
		* Sync
	+ Auto Trait 
		* RefUnwindSafe
		* Send 
		* Sync 
		* UnwindSafe
	+ Blanket Implementation 
		* Any 
		* Borrow
		* BorrowMut
		* DeserializeOwned
		* From
		* Into
		* Same
		* ToOwned
		* TryFrom
		* TryInto
		* VZip
- Properties 
	+ segfault when accessing without a borrow 
	+ different protections: noaccess, readonly (immutable 
		borrow), readwrite (one mutable borrow) with mprotect
	+ guard pages for overflows and large underflow
	+ canary to detect small underflows
	+ memmory is locked with mlock
	+ memory is zeroed when no longer used 
	+ guarded types can be compared in constant time
	+ guarded types cannot be printed
	+ interior data of a guarded type may not be clone
	+ GuardedVec can be (de)serialized


## Boxed 
- struct (generic)
	+ ptr to underlying protected memory 
	+ nb of elements of type T that can be stored in the pointer 
	+ current protection level of the data 
	+ number of current borrow of pointer
- methods 
	+ drop 
		* assert all refs are released 
		* assert protection with no access was set
	+ zero 
	+ new_unlocked
		* create a sodium init

## Lifecyble of Stronghold
1. Initialization of Stronghold 
	- _init_stronghold_system_
		+ init actor registry 
		+ create client actor 

2. Creating a secret 
	- _write_to_vault(location, payload, hint, options)_
		+ send to current client actor, location, payload and hint
	- client actor _write_to_vault_(self, location, hint, value)
		* create a an entry (vault_id, new_key) in keystore if location does 
			not refer to an existing vault_id
		* take key matching the vault_id (and remove it from the keystore)
		* write in the vault using a view: key, vault_id, record_id, value and hint
		* put back the key in the keystore: (vault_is, key)
	- _write(dbview, key, vaultid, recordid, data, hine)_ 
		* get vault using the vault_id, if it does not exist
		* add or update an existing record
	- add_or_update_record(vault, key, recordid , data, hint)
		* check that key in vault match the key argument
		* create a record 
			- data length
			- encrypt metadata in a DataTrasaction type 
			- encrypt data in a blob with the key 
			- revokation conditions
		* add/update the record to the vault entries hashmap (ChainId, Record)

3. Delete data 
	- _delete_data(location, should_gc)_
		+ send msg RevokeData to the current client actor
		+ if need gc send the msg GarbageCollect
	- _revoke_data(client, location)_
		+ take ownership of the key 
		+ dbview revoke_record_ 
			* in the record put a RevocationTransaction in the revoke field
		+ reinsert the key in the keystore

4. Call a procedure on data
	- _runtime_exec(stronghold, procedure)_
		+ send procedure to client actor
		+ call exec_proc with dbview
	- _exec_proc(view, key0, vaultid0, recordid0, 
	 									 key1, vaultid1, recordid1, hint, procedure)_
		+ get vault0 from vaultid0 
		+ get_guard from vault0, key0, rid0
		+ compute the procedure closure on the guard memory
			* decrypt the blob in a guarded vec
		+ write the result in vault1, record1

5. Read_snapshot
	- _read_snapshot(stronghold, clientpath, formerclientpath, keydata, filename, path)_
		+ get address from snapshot actor through the registry actor
		+ send msg ReadFromSnapshot to snapshot actor
		+ send data to secure client actor and reload 
	- _ReadFromSnapshot(snapshot, key, fname, path, clientid, formerclientid)_
		+ snapshotstate contains a hashmap 
			* clientId to 
				- Hashmap<vaultid, Pkey<Provider>>
				- dbview 
				- store
			* change the current snapshot for a new one from read_from_snapshot
	- _Snapshot::read_from_snapshot(name, path, key)_
			* deserialize the snapshot file into a SnapshotState
			* create a new snapshot actor with the generated snapshot state

6. Write snapshot 
	- _msg::WriteSnapshot_
		+ create a snapshot file
		+ set the snapshot actor state to default
	- _write_to_snapshot(snapshot, fname, path, key)_
