# GIEC
- Groupes d'experts Intergouvernemental sur l'Evolution du Climat
- Association de pays, les personnes siégeant ne font que représenter 
	leur pays
- objectifs
	+ faire une évaluation de la situation concernant les changements climatiques
		* connaissance scientifique/techniques/socio-économique
		* causes et répercussions
		* stratégies de parades
	+ se base sur le consensus scientifique par rapport aux recherches menées dans le
		monde entier
	+ objectifs de transparence 
		* sélection des documents/auteurs
		* approbation des rapports
- __le GIEC ne fait pas de recommandations mais des projections__
- les rapports du GIEC sont validés par:
	+ les gouvernements
	+ utilisateurs du rapport
	+ les rédacteurs du rapport
	+ les scientifiques
- fake news
	+ _le GIEC est politique et écrase les opinions gênantes_
		* les rapports sont bien le fruit de consensus puisque ils sont validés
			par __tout__ les pays qui ont des intérêts différents
		* de ce fait le contenu des rapports ont été constaté par tout les pays
		* les rapports ne font pas de recommandations mais seulement une observation
			de la situation
	+ _le GIEC est financé par les lobbys_
		* le budget du GIEC varie entre 5 et 8 millions d'€
		* financé par les membres de l'ONU à _prix libre_
		* 0€ par les USA depuis l'élection de Trump en 2016
		* France 1million d'€ (~15% du budget)
	+ _le GIEC choisit ses sources et exclut les travaux climato-sceptiques_
		* le GIEC prend en compte tout les travaux validés par les revues scientifiques
			(peer reviewed)
		* l'influence de l'homme fait aujourd'hui consensus au sein de la communeauté
			scientifique
- en 2007 le GIEC a reçu le prix nobel de la paix

# Effet de serre ou Forçage radiatif
- L'effet de serre est le phénomène de réchauffement de la planète
	dûe à l'augmentation de la concentration gaz à effet de serre (GES)
	par l'activité humaine
- GES __produits par l'homme__ (Nom / Formule / contribution à l'effet de serre)
	* Dioxyde de carbone / CO2 / 26% / 77%
		+ combustion d'énergies fossiles: charbon, pétrole, gaz
		+ les énergies fossiles sont tous essentiellement composés de carbone
		+ ce carbone provient de la transformation de matière végétale
		+ cette matière végétale obtient du carbone via la photosynthèse
	* Méthane / CH4 / 14%
		+ produit par la décomposition organique sans oxygène
			* dans la panse des vaches
			* rizières immergé qui se décompose sans oxygène
			* décharge de déchets quand les tas sont trop grands pour obtenir de l'O
	* Protoxyde d'azote / N2O / 8%
		+ l'utilisation d'engrais
		+ aliments pour le bétail
		+ procédés chimiques: production d'acide nitrique
	* Hexafluorure de soufre / SF6 / 1%
- Tout les GES
	* vapeur d'eau / H2O / 60%
		- __note__ la vapeur d'eau n'est généralement pas comptée dans le réchauffement
			climatique d'origine humaine car il ne reste que quelques semaines dans
			l'atmosphère avant d'être lavé par la pluie
	* Dioxyde de carbone / CO2 / 26% 
	* ozone / O3 / 8%
	* méthane et proto d'azont / CH4 et N2O / 6%
- __Forçage radiatif__
	+ le forçage radiatif est la différence entre l'énergie radiative
		reçue et l'énergie radiative émise par un système
	+ elle représente l'énergie qui fait quitter un système de son équilibre
		thermodynamique
	+ terme scientifique utilisé plus juste que l'effet de serre
		* le réchauffement d'une serre est dûe à la non convection de l'air
			chaud entre les parois et le monde extérieur, ce qui n'est pas
			semblable au phénomène du réchauffement climatique
	+ effets réchauffants
		* solar: intensité du soleil qui varie
		* Black Carbon: carbone noir (suie) provenant de la combustion se déposant
			sur la neige blanche et qui diminue l'effet albédo
		* Contrails: traînées d'avion (aérosols et vapeur d'eau) agissent comme 
			des nuages
				- à cette hauteur l'effet réchauffant des nuages l'emportent
					sur l'effet refroidissant 
		* H20 stratosphérique lié à la combustion des avions
				- pas de mouvements verticaux dans la stratosphères seulement horizontaux
				- il n'y a pas de pluie dans la stratosphère
				- la vapeur d'eau à cette hauteur peut rester quelques années
					et participe au réchauffement climatique
		* O3 (ozone) troposphérique: 
				- la couche d'ozone est située dans la stratosphère et nous protège des UVs
				- l'ozone est	un gaz à effet de serre et celui de la troposphère 
					produit un forçage radiatif positif
				- l'humain ne produit pas d'ozone directement
					* on produit les précurseurs: NOx,C0V, CH4, CO
		* CO2: l'élément principal
		* autres GES: CH4, N2O
	+ effets refroidissants
		* Aérosols (voir #Aérosols): nuages et effet occultant des rayons solaires
		* Usage des sols: la déforestation remplace du foncé par du clair (arbre/sol)
		* ozone stratosphérique: la destruction de l'ozone stratosphérique (couche d'ozone)
			par les CFC a diminué l'effet de serre
		* éruption volcaniques
			- envoie des cendres dans la stratosphères
			- stratosphère provient de stratifié (stable verticalement)
			- pas de mouvements verticaux dans la stratosphères 
			- mais des courants horizontaux puissants (jetstreams)
			- les cendres sont mélangés sur l'ensemble de la planète
			- grosse réflection du soleil

# Réchauffement climatique
1. La Terre reçoit des rayonnements solaires
	+ 30% est réfléchie par air/nuages/surface claires (glaces) 
		* aussi appelé effet __albedo__
	+ 20% est absorbée par l'atmosphère
	+ 50% est absorbé par la surface terrestre
2. La surface terrestre comme tout corps émet un rayonnement thermique
	+ la terre considéré comme un corps noir émet dans un spectre uniquement
		lié à sa température
	+ pour la terre le spectre est situé dans l'infrarouge
	+ pour le soleil le spectre du rayonnement est situé dans le spectre visible
3. le rayonnement de la terre est en partie absorbée par les GES
	+ le forçage radiatif augmente car l'homme produit de plus en plus
		de GES, notamment le CO2 (voir CO2)
4. cette énergie est ensuite diffusée sous forme de chaleur dans tout les 
	sens et notamment vers la surface de la terre
5. cette énergie réfléchie est absorbée par 
	+ 93% océan
	+ 3% fonte des glaces
	+ 3% dans les sols
	+ 1% atmosphère

# Aérosols
- __Définition__: 
		particules solides ou liquides tellement petites que leur vitesse de chute
		est considérée nulle
- émis par les activités humaines et naturels
	+ naturels
		* volcans, incendies de forêts, tempête de sable
		* aérosols naturels
			- sable
			- cristaux de sel
			- composés organiques arrachés par le vent 
	+ humaines: combustion incomplète d'hydrocarbures
		- combustion complète donne C02 et H20
		- les hydrocarbures ne sont pas purs
		- on obtient aussi
			+ NOx
			+ monoxyde de carbone
			+ carbone noir (suie)
			+ sulfure d'hydrogène
- effets directs et indirects sur le climat
	+ effet direct: occultent les rayons du soleil
		* les jours de pollution la lumière est différente
		* refroidit le climat 
	+ effet indirect: rôle dans la fabrication des nuages
		* les nuages ne peuvent se constituer que sur un noyau de condensation
			comme un aérosol
		* fabriquent plus de nuages
		* les nuages ont un effet albédo ET effet de serre
		* la résultante est un refroidissement 
- _pourquoi les aérosols ne sont pas une solution pour le réchauffement climatique?_
	+ les aérosols ont une durée de vie courte donc l'effet albédo
		est liée à la production instantanée contrairement au CO2 qui s'accumule
		dans l'atmosphère
		* les aérosols ont une durée de vie supérieur dans la stratosphère
		* la validité d'une telle solution n'est pas garantie
		* création d'une dépendance aux aérosols
	+ l'impact de l'effet de serre et des aérosols ne sont pas homothétiques
		* effet de serre est plus important aux pôles
			- cela est dûe que la majorité des GES est dûe à la vapeur d'eau
			- l'effets de serre est moins probant dans les régions humides (équateur)
			- le réchauffement climatique est 2x plus important aux pôles
		* les pôles ne reçoivent presque pas de rayonnement solaires
		* on aurait des pôles qui se réchauffent et l'équateur qui se refroidit
		* il y aurait un changement climatique malgré un non-réchauffement climatique moyen

# Influence de l'activité humaine
- Industrie (40% des GES, surtout CO2)
	+ fabrication des biens de consommations
	+ essentiellement: papeterie, ciment, acier, aluminium, chimie
	+ solutions
		* allonger durée de vie des produits
		* réduire leur consommation
- Bâtiment (20% des GES, surtout CO2)
	+ utilisation des batîments, pas leur construction
	+ chaufface, éclairage, climatisation, électronique
	+ solutions
		* isolation/rénovation thermique
- Transport (15% des GES, surtout CO2)
	+ surtout pays occidentaux
	+ l'essentiel du bilan carbone peut être réduit aux vols longs 
		courriers d'une personne
	+ produit aussi des aérosols
- Agriculture (25%, pleins de GES)
	+ Déforestation (surtout CO2)
		- Seul le fait de brûler les arbres créer du CO2
		- on ne réduit pas les puits de carobne en déforestant car
			la plupart du temps les forêts déforestés sont à l'équilibre et ne
			produise plus de bois
	+ CH4: bovins, rizières (produits lorsque les sols sont longtemps immergés)
	+ N2O: engrais, rizières (lorsque la terre est immergé/séchée régulièrement)
- où va le CO2 produit?
	+ 25% océan
	+ 50% atmosphère
	+ 25% photosynthèse

# Variation du climat naturel
- Soleil
	+ l'étoile vieillit, par exemple l'énergie reçue aujourd'hui
		est 30% moindre à celle reçue il y a des milliards d'années
	+ l'énergie reçue par le soleil suit des cycles de 11 ans caractérisé par la
		variation du nombre de tâches solaire
	+ l'orbite de la Terre se déforme sous l'attracion des grosses planètes
		* peut engendrer des ères glaciaires pour la planète
		* l'ellipse de la terre autour du soleil s'applatit et grandit suivant
			des cycles ~80 000 ans
	+ l'inclinaison de la terre sur son orbite (obliquité) varie sur des périodes
		de 40 000ans
	+ la terre n'est pas toujours au même endroit dans l'orbite lros des équinoxes
		* l'équinoxe correspond au moment où l'axe de rotation de la terre est perpendiculaire
			à la ligne traversant la terre et le soleil
		* durant l'équinoxe la longueur du jour et de la nuit est la même partout sur Terre
		* la face présentée au soleil n'est pas la même à chaque tour d'orbite, l'énergie
			reçue par régions est différente chaque année
- modification de l'atmosphère (sans intervention de l'homme)
	+ par exemple l'apparition de la photosynthèse a diminué la concentration en CO2
	+ au temps des dinausures l'atmosphère contenait de 2 à 5 fois plus de CO2
	+ volcans
		* dégagement de CO2 et SO2
		* cendres envoyés dans la stratosphères bloque les rayons du soleil
	+ dérive des continents

# Capture et Séquestration du Carbone (CSC)
- l'idée est de réduire les émissions de carbone industriel dans l'atmosphère 
	en injectant ses émissions dans le sol 
- le carbone doit être stocké à long terme
- aujourd'hui les sites de CSC captent 35 000 000 tonnes/an
	+ émissions en 2019, 37 000 000 000 tonnes
- émission de carbone négative
	+ dans ce cas on va brûler du bois et le CO2 sera injecté dans le sol
	+ comme si l'on prenait du CO2 dans l'air qu'on réinjectait dans le sol
- __Séquestration de carbone__
	+ _où?_
		* il existe 21 sites dans le monde mais pas distribué uniformément 
			géographiquement
		* réservoirs épuisés/en déclin de gaz ou de pétrole (16) 
			- possibiilité de stocker de grandes quantités
		* aquifères salins profonds (5)
			- poche d'eau salée inutilisable
			- sans impact pour faune et flore?
			- possibiilité de stocker de grandes quantités
			- potentiellement 10 000 milliards de tonnes de CO2 (300x les émissions 
				mondiales annuelles)
		* veines de charbons inexploitables car trop profond
			- encore théorique ou petite quantité
		* roches basaltiques qui réagira avec le CO2 pour former du calcaire
			- encore théorique ou petite quantité
	+ _sous quelle forme?_
		* liquide 
		* super critique (à mi-chemin entre gaz et liquide)
	+ _quels risques?_
		* les fuites
			- expérimentalement ces zones de stockage peut le faire sur le long terme
			- même dans le pire des cas les fuites seraient diffuses
		* provocation de séismes dûes à l'injection
			- technologie maîtrisée par l'industrie avec les forages et autres
- __Capture__
	+ la partie encore en recherche car non maîtrisée et énergivore
	+ aujourd'hui utilisation d'un solvant pour capter le CO2 dans l'air
		* le CO2 se dissout dans le solvant
		* le solvant va ensuite être chauffé pour relâcher le CO2 et être stocké
- __la CSC modélisée dans la transition écologique__
	+ la présence des CSC est très importante dans les modélisations (par exemple GIEC)
	+ prédiction en 2050: 250x plus de captation que la situation actuelle
	+ le déploiement actuel n'est pas à la hauteur des prédictions mais
		le CSC est toujours un grand facteur dans les modèles prédictifs
	+ pourquoi cette incohérence?
		* les modèles se plantent
		* l'impact de la CSC peut être énorme, donc on espère que le déploiement 
			va s'accélérer
- __Pourquoi n'est-elle pas utilisée par les industries des énergies fossiles?__
	+ aujourd'hui elle est surtout utilisée lorsque c'est rentable,
		par exemple lors du pompage de pétrole, la partie CO2 est réinjectée
	+ ajouter la CSC à une centrale charbon consommerait 1/4 de l'électricité produite
	+ coût de l'électricité
		* charbon avec CSC 100€/MWh
		* charbon sans CSC 50€/MWh
		* gaz avec CSC 80€/MWh
		* gaz sans CSC 50€/MWh
	+ une centrale utilisant la CSC mettrait la clef sous la porte
	+ l'industrie a intérêt à ce qu'on croit à la solution (avoir un discours, plan B...)
		mais pas qu'on la déploie
- __Comment accélrer le déploiement?__
	+ obligation de capter et séquestrer la majeure partie des émissions
	+ subvention des technos CSC
	+ taxes carbones plus élevée
