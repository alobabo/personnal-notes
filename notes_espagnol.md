#  Accent tonique
1. sur l'accent visible
2. avant-dernière syllabe si
	+ termine par une voyelle
	+ temine par un -n ou -s
3. dernière syllabe sinon
+ important de bien prononcer car cela peut changer le 
	sens d'un mot:
	> canto, je chante
	> cantó, il chanta

# Démonstratifs
- formes:
	- masculin singulier: _eso/esto/aquel_
	- masculin pluriel: _esos/estos/aquellos_
	- féminin singulier: _esa/esta/aquella_
	- féminin pluriel: _esas/estas/aquellas_
	- neutre: _esto/eso/aquello_
- usages:
	+ _este_ dans la sphère du locuteur
			> esta tarde tengo que...
	+ _eso_ distance moyenne
			> ves eso edificio antiguo?
	+ _aquel_ distance supérieure
			> no me acuerdo del argumento de aquella película
- remarque
	+ _ese_ peut avoir une conotation critique
	+ _aquel_ peut avoir une conotation laudative 

# Possessifs
- s'accordent en nombre et en genre avec le nom qu'ils accompagnent
- placés avant ou après le nom et changent de forme
	> mi perro
	> el perro mío
- formes singuliers (avant/après masculin/après féminin)
	+ mi/mio/mia
	+ tu/tuyo/tuya
	+ su/suyo/suya
	+ nuestro/nuestra (même forme avant/après)
	+ vuestro/vuestra (même forme avant/après)
	+ su/suyo/suya
- usage
	+ en général avant le nom
	+ après le nom dans des exclamations
		> dios mío!
- il n'y a pas de 3è personne du pluriel
	> Miguel y Cecilia nos presentaron su proyecto
	> Miguel et Cecilia nous ont présentés leur projet

# Indéfinis
- quelque: _algún/alguno/alguna_ 
- aucun: _ningún_
- n'importe quel: _cualquier_
- autre: _otro_
		+ jamais avec un pronom indéfini
			> la otra persona, l'autre personne
			> otra persona, une autre personne
		+ devant les numérals
			> tienen otros dos hijors
- tout les autres: _los demás_
- certain: _cierto_
- plusieurs: _varios_
- beaucoup: _mucho_
- peu: _poco_
- assez: _bastante_
- trop: _demasiado_
- suffisament de: _suficiente_
- tout/tous: _todo_
- chaque: _cada_
- plus: _más_
- moins: _menos_
- quelque chose: _algo_
- rien: _nada_
- pas du tout: _nada_
		> no me gusta nada el chocolate
- quelqu'un: _alguien_
- personne: _nadie_
# Comparatifs et Superlatifs
- Supérieur/inférieur
	- más ... que
	- menos ... que
	- comparatifs
		+ mayor
		+ menor
		+ mejor
		+ peor
		+ inferior
		+ superior
	- avec une proposition on utilise _del que_
		> Tiene más dinero del que imaginas
		> Tiene más defectos de los crees
		> Es menos tarde de lo que piensas
- égalité
	+ tan _adjectif/adverbe_ como
		> Es tan tímido como su padre
	+ tanto _nom_ como
		> Tiene tanto fantasía como cuando le conocí
	+ _verbe_ tanto como
		> No duermo tanto como tú
- superlatif
	+ el más/el menos _adj_
		> es el perro más caro de la ciudad
- superlatifs absolus
	+ muy + _adj_  /  -ísismo
		> es inteligentísimo
		> muy caro
	+
