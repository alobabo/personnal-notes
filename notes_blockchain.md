# TODO
- multichain ecosystems
- layer 2/3 chains
  + Lightning network
  + Plasma
  + Raiden
# BlockChain Principles
- __Goal__
    Record any transaction or action between parties
- __Selling features__ for application
    1. __Trustless__
        - The tech does not need third party to guarantee
        the integrity of an exchange
        - depends on the decentralized structure:
            + all miners have a copy of the transactions
            + everyone checks that every new transaction is allowed
    2. __Immutable__
        - It's really hard to falsify previous exchanges
        - all blocks depend on the past blocks
        - to falsify a block you need to modify all the following ones
				- also to avoid the _double spend_ problems
					* make a victim believes that a payment has been made then
						changing the chain to make the previous payment void
    3. __Anonymity__
        - actors of a transaction are represented by their private key
        - possible to have private content of a transaction too
- __Principles__
	1. Decentralisation
		System can work in the case where participants have limited resources
	2. Scalability
		System can deal with transactions which required computing powers is higher than
		what can offer a single node
	3. Security
		System is secure against an attacker possessing less computational power than
		the required amount for the network to process blocks
- __Algorithm__
    - Client
        + has a public and private key
            * public key to interact with other people
            * private key to access your account
        + client sends its transaction to multiples miners with a transaction fee
        + client wait for few blocks (depending on the tech) for its
        transaction to be confirmed
    - Miner
        + receives transactions and spread it to other miners
        + add transactions to the block he is mining
        + Try to validate its block which brings other nodes to a __consensus__
            * the protocol used to chose the next block needs few traits
                - should able to check the integrity of the block
                - should reward miners/forgers as incentive
                - the gains from an attack should be lower than the cost to mount
                the attack
            * _Proof of work_
                - try to find the nonce to compute a valid id for the block
                - a block has its hash id computed from:
                    * content of the block
                    * hash of the previous block
                    * nonce that has to be mined
                - usually to set the difficulty of the mining (used to set an average
                mining time) the id has to be lower than a threshold
                - If found receives some coin as incentive and spread the new block to other nodes
                - Else receives new block from other nodes and check its
                integrity then compute next block
								- attack
									+ get 51% of the blocks for a period of time (to have longest chain)
									+ the cost:
										* all the miners combined spend approximately 1$ for every 1$ in
												block rewards (else they are unprofitable)
										* attackers just need to spend more than the cost of the following
											block rewards during the time of an attack
										* gpu is not that expensive to rent, so making the longest chain is
											possible
            * _Proof of stake_
                - the next block creator is chosen from a pool of nodes
								- validators have made a deposit to the blockchain
									and are tasked to propose and vote on the next block
								- Possible criteria for validators weight during votes
									+ amount of coins possessed
									+ longevity of the coins possessed
									+ amount of coins bet for the next block
								- two types of consensus between validators
									+ _chain based_
									  * every n seconds a validator is allowed
										to create a block following an existing block
										* usually most blocks converge into a unique chain
									+ _BTF-style_ (byzantine fault)
										* validators are randomly assigned the right to propose a block
										* consensus over which block is canonical is done through a
											multi-round vote to agree on a single block
                - to mount an attack it is necessary to own a large amount of coins
                    + really expensive
                    + if the attackers do shit then trust in the coins will fall
                    with its value
                    + a person with monopoly on coins is forced to act "benevolent"
								- cost to get 51% of the chain:
										+ you have to majority of the "stakes"
								- variant _Liquid PoS_
									+ you can stake on a validator to improve its chance
										of choosing the next block
									+ staker gain parts of the alidation reward
						* Stake vs Work
							- pow transforms computational efforts into coins which makes
								it costly to create false coins (recompute the chain)
							- in pos it is easier to recover from an attack using a fork
							- cost of an attack:
								+ pow, the global computing power cost is similar to reward for a block
									you have to get 51% of the computing power (the cost depends on the
									reward of the blocks)
								+ pos, to get 51% you have to stake as much as the honnest validators
									(the cost depends on the total stakes of honnes nodes)
							- pos may be more decentralized, since being a validator is accessible to
								everyone contrary to current computing power required for pow
							- pos may concentrate wealth in the long run since rich validators
								will get more stakes and snowball
- __Common issues__
	+ scalabitiliy
		* to get scalability we could give more transactino power to nodes
		* unfortunately this leads to centralisation such as in blockchain
			where only a few have the computing power to mine coins
		* have to find a balance between scalability and decentralisation
		* a solution is to use __sharding__
			+ Ethereum
- __SmartContract__
    + programs that executes on events of the block chain
		+ pros
			- avoid the need of trusted intermediators/arbitrations/enforcement costs
			- allow to do exchanges much more complexed than just give and take
			- potentially be cheap, fast and secure
			- immutable
		+ cons
			- cannot be corrected if contains bugs or vulnerabilities
    * Ethereum accounts
        * hold a balance
        * contract code
        * data store
        * the accounts are mutable by transaction compared to Bitcoin which is a
        sum of plus and minus
    + Ethereum smart contract language is Turing complete
				* allow complex operations
				* security issues such as infinite smart contracts
    + Used to do ICO and tokens
        * _Initial Coin Offering_
            - Like an IPO (P for public), a project that needs funds
            - people can invest in the projects by buying tokens of the project
            - the tokens are usually bought with the native coin of the block chain
            like ether for ethereum
        * tokens
            - these tokens would be the dual of shares
            - tokens use depend on what the projects propose
            - it can be decision power, premium service ...
		+ Smartcontracts are executed by multiple miners but only of them
			will write the output in the ledger
- __Decentralized applications (Dapps)__
	+ software applications ditributed and replicated on multiple nodes
	+ made from one or several smart contracts which are executed on a blockchain
	+ for example bitcoin as money is a _dapp_ opposed to the management of €
		by banks and banque centrale
		* examples
			- bitcoin vs banks
			- Maker/Compound for credit
			- Hive/Voice/Memo.cash for Facebook/Twitter
			- LBRY for Youtube
	+ a dapp usually contains:
		* one or few smartcontracts executed on a blockchain
		* a user interface
			- most often a web interface between smart contracts and the software
		* a model to store data
			- usually stored on a p2p network
		* a P2P protocol for messages
			- Whisper
		* a decentralised name solver
			- Ethereum Name Service
	+ cons of centralised app
		* application data are not yours, they are stored on the centralised server
			that might use your info
		* app resilience depends only the few centralised servers
		* owners of centralised apps can do whatever they want
			- censorship
			- bans for non legitimate reason
			- abusive rules
	+ pros and cons of DApps
		+ resilience, apps shoudl not stop
		+ transparency, apps source code are available in smartcontracts
		+ avoid abuse from the app company/owner
		+ usually cheaper
		+ data are owned by users of the DApps
		- slower
		- may be expensive for complex apps
		- cannot be interrupted except by removing it from the network or
			make an update preventing the dapp from working
		- spedific kknowledge to write smart contract code
	+ What you get for free making a dapp
		* identity system
			- accounts
			- ownership
			- history/logs
		* transactions
			- direct without third-party
			- instant
			- micro (low fees)
			- conditional (can add logic to transactions)
		* cryptography
			- verify activity or non-repudiation
			- verify process integrity (dapps are open-source)
			- privacy

# Bitcoin
+ basis of blockchain
+ uses proof of work
+ method of recording state is _UTXO_ (Unspent Transaction Output)
		- a wallet is composed of all the blocks where the receiver of a
			a transaction corresponds to the wallet address
		- the wallet is an abstraction which is calculated out of the chain,
			which just locate all utxo's under an address to display an 'account balance'
		- to send money, you break one of your block (utxo) which will create another
			block with the remaining credit of your utxo and a new utxo for the receiver
		- similar to bank notes in your wallet (you have multiple and you need to break one
			to pay)
		- the global state at any given time is the set of all spendable UTXOs
		- Specificities
			+ wallets are stored by the blockchain
			+ stronger privacy since you can use multiple address for your wallet
			+ it is more difficult to do complex tasks (smart contracts) since you
				don't have a global easily accessible state
			+ transactions can easily be processed in parallel since global state
				is defragmented
			+ all transactions are naturally valid since the must come from valid
				utxo
			+ all valids utxo have to be recorded and noted (separate database)
				which is hardly scalable

# Ethereum
+ Smartcontracts framework using blockchain
+ Ethereum blockchain can be considered as a __decentralized state machine__
	* the blockchain is made to execute decentralized applications or _Dapp_
	* those dapp are made of smartcontracts
	* to execute your dapp you must pay ether
+ Ether is the currency to make operation on the Ethereum blockchain
	* people who want to execute their dapp/smartcontracts must pay ether (ETH)
	* miners/nodes are paid with ether
+ How to make an operation on Ethereum
	* you have to pay for gas to do operations
		- ethereum transfer is 21000 gas
		- addition is 3 gas
		- multiplicaiton is 5 gas
	* gas is an abstraction of computing power unit
		- each ethereum block has an upper bound on gas
		- gas is limited by the blockchain
		- allow to put a metric on computing power used
	* gas metric is _Gwei_
		- 1Gwei = 10^-18 ETH
	* for a transaction you have to pay for: _gas limit * gas price_
		- gas limit
				+ upper bound on the necessary gas for your transaction
				+ you give an estimation of the gas limit
					- if gas limit is too low, your transaction is rolled back
							but you pay the ETH used for computations
					- if gas limit is too high, extra gas will be refunded
						* putting gas limit put you at risk at depleting too much funds if your code is buggy
						* you also crowd the network
				+ prevent you from depleting your funds when having too long/infite
					trasactions due to bugs
		- price of gas
				+ you choose the price of gas, but you usually index it on the market price
				+ market price depends on the available computing power of the blockchain
					* if the nerwork is busy gas price will rise
				+ miners will prioritize transactions with a high gas price
+ __Ethereum Virtual Machine__
	* the EVM is a nearly Turing-complete state machine
		- got loops and recursivity
	* run smartcontracts under the form of bytecode
		- usually resulted from the compilation of _Solidity_ programs
	* Runtime environment to run smartcontracts
	* composed of:
		- immutable EVM code
		- machine state
			* program counter, gas available, stack, memory
		- world state, persistent memory
			+ method of recording world state: _Account model_
				* similar to a credit card and a bank account
				* world state is stored on nodes locally (not in blocks)
				* world state is a _Merkel tree_ and nodes reach an agreement
					by comparing the state root (basically an hash of the whole tree)
				* transactions are events and the Ethereum Virtual Machine computes
					the next state transition based on event and prior blockchain state
				* separation between transactions (events) and global state (outputs)
				* Specificities
					- accounts are stateful which is easier to interact with
					- bulk transactions are more efficient than in UTXO
						(can be grouped into one contract)
					- transactions all have to validated by the nodes locally since
						the chain does not contain the state (to avoid illegal transactions)
+ __Ethereum Name Service__
	* used to map account addreses to names (crypt.eth)
	* DNS for Ethereum domain names
	* For each domain the following information will be given by the _ens_
		- domain owner
		- domain mappings (eth addresses, smart contracts...)
		- past states of the domain
+ LImits of Ethereum
	* Dapps are depends on blockchain performance, essentially scalability
		We reached the limits in 2017 when cryptokittis were trending
	* it is not possible to update a smartcontract with a bug or security issue
+ Ethereum 2
	- going from POW to POS with Casper
			+ validators have to put 32 ETH as stakes to be a validator
			+ in case of inactivity your 32 ETH get decreased bit by bit
				* you keep losing unti a certain threshold then get kicked of 
					of the validators
				* in case of misbehaviour such as accepting fake transaction 
					punishment is harsher
			+ validators are responsible for a specific shard or two
				* propose and attest
	- imprevement of EVM with eWASM
	- usage of _sharding_ to improve scalability
		* __Sharding__
			+ the goal is to have the trinity properties
				* scalability, decentralization and security
				* classic pow does not get scalability
				* private chain does not get decentralization
				* multi-chain ecosystems do not get security
			+ does not work well with PoW since atkers need less computing power to
				control a shard
			+  Split ethereum state and history into independent shards
				* each shard is validated by a changing committee
					- allow parallelization of the work
				* each shard is similar to an entire chain
					- possess its local state
					- possess its local history
					- has its own validators committee
				* shards need to be synchronized to keep a coherent global behaviour
					- _cross-shard-communication_
				* there exists a main chain called _beacon chain_
					- the beacon chain allow nodes to be validators 
						+ in exchange of a stke of 32ETH
						+ there are beacon nodes (more computer intensive)
						+ and also shard validators
					- beacon chain stores solely 2 things
						+ list of validators
						+ stores attestations
					- a commitee is chosen for each shard which are responsible for
						validating blocks of the shard
						+ for a block to be valid, 2/3 of the committee vote about the validity of
							the block and leave their signature/vote on the beacon chain
							* this vote/signature is an hash of the shard state
						+ a beacon committee review the votes via a sharding manager contract
						+ the votes are called attestations 
							* vote for current beacon chain head
							* vote on which beacon block should be finalised 
							* vote on current state of the shard
							* signature of all the validators who agree with that vote
					- for a beacon node to validate the beacon chain and
						the shards, it only needs to look at the aggregated attestations
						from each committee
						+ validators signatures can be aggregated
				* each shard is tied to main ethereum shain in the form of merkle trees
			+ with random sampling
- __Modified Merkle Patricia Trie (MPT)__
	+ used to save states
	+ fusion of Patricia trie and Merkle tree
	+ Eth store state in key value databases
		* value stored in the storage is the content of the MPT leaf node
		* key is the path to a leaf node in the MPT
	+ hexary patricia tree, each node can have up to 16 branches
		* path/value of branch nodes is composed of hexadecimal value
	+ 3 types of nodes
		* https://i.stack.imgur.com/YZGxe.png
		* _branch node_ is composed of 17 items
			- 1 node value corresponding to an hexadecimal char
			- 16 branch node, each corresponding to next hexa char
		* _leaf node_ consists of 2 items: hexa char + value (e.g: account balance)
		* _extension node_ 
			* optimized branch node
			* most of the time, branch nodes only have one child
			* in this case the branch subtree is compressed into an extension node
			* extension node contains: complete path + hash of the child

# Tezos
+ Tezos new blockchain written in Ocaml with smart contracts
__Has the particularity to be self evolving with an internal protocol__
+ Resolve the issue with forks
    * Risk of having a split
    * A fork is successful if the mass of users switch from the old
    to the new software
    * In this case currency will exist in both chains and may create mistakes
    in transactions which decrease the trust in both chains
    > avoid the risks of forking which allow more flexible chain
+ Use _liquid proof of stake_ instead of proof of work
    * Proof of work encourages large pool of miners
        - Bitcoin is mainly mined by few pools which breaks the idea of decentralized protocol
        - Easier to attack and under the rule of dominant miner
        - Large pool of miners are encouraged since it means more stable income
        and growth
        - Furthermore some create "premium" transactions to increase their revenue
        (bitcoin amount of transactions is limited making it a scarce resource)
        > in POS having 51% of hash power mean having 51% of the currency which
        > is much more costly
    * POW bitcoin makes incentives between stakeholders and miners misalign
        - This misalignment brings discords for future modifications of the chain
        - miners revenue is based on mining and transaction fees
            + mining income is low since mining cost and revenue should
            come close to 0 in average
            + transaction fees depend on number of transactions
            + bitcoin has low number makes it a limited resource
            + Therefore miners want high transaction fees
        - stakeholders revenue comes from number of users and trust of the blockchain
            + if mining fees are high people won't be using blockchain
            + 51% attack should be costly which is not the case anymore
            if miner revenue is low
        > in POS miners are also stakeholders therefore interests of the actors are aligned
    * POW waste a lot of resources
        - Tons of computation with only one hash
        > does not used up resources to validate blocks but instead
        > you must show that you possess the resources to validate
+ Smartcontracts: Thezos compared to Ethereum
		* use a secure language called Michelson
				- Michelson have a specification if Coq
					+ Mi-Cho-Coq is an implementation of a Michelson interpreter in Coq
				- MIchelson smartcontracts can be formally verified
    * Ethereum is Turing complete -> may have non-terminating programs
    * the reward for validating a transaction is proportional to the number of steps
    used to exec the contract
    * malicious miners can input a block with a non-terminating contract with huge rewards
        - other miners will waste time on it -> Denial of service attack
    > Tezos limits the nb of CPU steps allowed for blocks
    > Smartcontracts are usually concise so it's not an issue
    > Since the protocol is amendable the limit can be adjusted
+ Correctness
    * Bitcoin uses C++ which is often riddled with memory vulnerabilities
        - this code is connected to Internet
        - Cryptographic lib with C/C++
        - Bug with integer overflows happened creating a huge amount of coins
    * Tezos uses Ocaml
       - functional language
           + stateless language
           + logic of the program is clearer
       - strong typing
           + no undefined behaviour
       - quite fast
       - can be extracted from Coq therefore critical protocol can be formally verified
* Pros and cons of On-Chain Governance
	+ increase coordination and fairness between users
		* if an on-chain exists it defintely is quicker than reaching a
			consensus using off-chain tools (mails, chats...)
	+ quicker decision making
	- risky because evolution can gets out of control
		* can be manipulated to introduce flaws quickly
	* for low level blockchains stability may be prefered
	* higher level protocols may be adventurous to try out new things
- __HoneyBadger__
	+ library that implements a consensus protocol 
	+ terms
		* we want to have a common subset agreement 
			- f.e agreement on a batch of transactions to be processed
		* __Common Subset Agreement__
			1. using _reliable broadcast_ every node can propose value _v_
			2. binary byzantine agreement with common coin is used to vote
				if _v_ is included or not
		* __Reliable Broadcast__
			- you want to send value _v_ to all nodes
			1. apply erasure coding to _v_ and obtain N blocks
			2. compute merkle tree with the blocks and obtain hash root _h_
			3. sender sends to each party message _M_: 
				(1 block, _h_, corresponding merkle tree branch)
			4. when a node receives _M_ it "echoes" it to everyone else
			5. when enough "echoes" has been received by a node it can recompute 
				the value _v_ from the blocks and erasure coding and check validity
				by recomputing the merkle tree
	+ protocol 
		1. every node proposes a batch of transactions which is encrypted 
			using a threshold encryption scheme
		2. Common subset agreement is used on encrypted batches
		3. nodes work together to decrypt the agreed batch of transactions
			* nodes produce their secret shares and send it to the others
	

# Ripple
+ Developed Ripple blockchain
+ Native crypto is __XRP__
+ Created to allow banks to transfer money internationnally
	* faster and cheaper than through thed centralized organisation SWIFT
+ Public and permissioned blockchain
  * anyone can join the network
  * this is permissioned because to run a node you
    need enter other nodes quorum set
  * in Stellar there are few incentives to run a node
    - transaction fees are sent to the community pool
    - no mining
    - only Stellar developpers and companies who run business on
      Stellar are interested in running a validator
+ half of the coins are hoarded by developement team using it to
	regulate the circulation of coints
	* money if deflationary because transaction fees are burnt
	* to burn coins you send them to an unreachable wallet address
+ Consensus algorithm: _Proof of Correctness_
+ Stellar reproaches to Ripple
  * governance
    - tension between shareholder interests and network users
  * technical
    - nearly closed membership make it
    similar to traditionnal Byzantine Agreement

# Stellar
+ Stellar and Ripple were co-founded by Jed McCaleb
	* many think that Stellar is a fork of Ripple
+ Stellar crypto currency is called __Lumens__ or __XLM__
+ Like Ripple was made to transfer money internationnally
  * this is actually the thrid iteration after Ripple and Stellard
+ However Stellar is aimed at _citizens_ and _organizations_ of poor countries
	* McCaleb regularly donate freen LUmens to those in need
	* Stellar calls itself a "Not-for-Profit-Organization"
+ semi-private with no public nodes
	* developers run nodes across multiple servers
	* banks and private org that use the tech can also run nodes
+ half of the coins are hoarded by developement team using it to
	regulate the circulation of coints
	* they will inflate the price of Lumens by 1% every year
	* like real-world money when they release money to inflate prices
+ Consensus algorithm: _Stellar Consensus Protocol_
+ Consensus protocol is independent from Lumens emission
	* validators are not rewarded with Lumens
+ __3 major goals__ as a payment network
	* the 3 rules
		1. Open membership
		2. Issuer-enforced finality
			- token issuer can prevent transactions from being undone or reversed
		3. Cross-issuer atomicity
			- exchange and trade are atomic with multiple issuers
	* 1 and 2 are achieved with payment solutions such as Paypal, Venmo, WechatPay...
		- but atomic transactions across these currencies is impossible
	* 2 and 3 can be enforced in closed systems but does not respect 1
	* 1 and 3 have been achieved in mined blockchains such as Ethereum but there
		are situations where transactions had to be reversed
		- token issuers had to been liable for tokens already been exchanged
			for real-world money
	+ __Stellar Consensus Protocol__
    - 4 key properties
      1. decentralised
      2. flexible trust
      3. low latency
      4. asymptotic security
    - Based on __FBA__ and __Quorums__(see below)
    - __Internet hypothesis__, the nodes people care about will be intertwined
    - Prioritize safety over liveness
      + encourage having large quorum slices to improve the chance of having
        intertwined node
      + with FBA it is easy to recover from liveness failure
        * in traditional BA, you have to add/remove nodes to recover from
          liveness failures which requires reconfiguration consensus
        * in FBA, any node can adjust its slices at any time to work around
          the issue
		- _Statement_
			+ something the network wants to agree on
				* "I propose this transaction for ledger 5000"
				* "I am ready to apply this transaction set for ledger 5000"
		- __SCP__
			+ two-phases
			1. __Nomination phase__
        _I nominate V_
				+ use federated voting to choose which values (set of statements) to
					consider for the ledger
        + each note may cast a vote for value V, but also have to mirror
          peers vote (essentially multiple votes in parallel)
        + eventually there will be a nominee value that will be confirmed
          through federated voting, it is called a _candidate_
        + the final nominated candidate is a composite of all the previously
          confirmed candidate
          * has to be deterministic since all the nodes do the same combination
          * application layer have to supply a combination method to turn multiple
            candidates into a single composite candidates
          * for lunch order, could discard one lunch
          * for transactions, will take the union of two sets of transactions
          * this is done using a federated leader selection
        + since the network is asynchronous you may get multiple final
          composite statement
          * we don't care the nomination phase is just used to reduce
            the number of nominated statements
			2. __Ballot phase__
        + a ballot is a pair <counter, value>
          * value → transaction
          * counter → id for the ballot
          * statement → message about a ballot
            - "I am prepared to commit ot ballot B"
            - "I commit to ballot B"
        + use federated voting with statements to try to reach consensus on a ballot
          * if a ballot timeout you retry with ballot <counter+1, value>
          * ballot with high counters are prioritized
        + from the pov a node, consensus is reached when a quorum accepted
          a ballot
          - the ballot value is then _externalized_, meaning you can act
            on it
          - once a ballot is confirmed commited, node know that other nodes
            have externalized the value or will eventually
        +

# NEAR
+ platforms for creating, developping and serving Dapp
+ uses sharding
	* 3 ways to validate cross-chain transactions 
		- dual validation 
			+ validators validate transactions on both receiving and sending chain
			+ does not scale well for validators 
			+ used by Quarkchain
		- trust the transaction
			+ assume that incoming transactions are valid 
			+ total number of tokens in shards are kept in check so you 
				can't create tokens
			+ theorically you can create invalid transfers between parties (stealing)
			+ used by Cosmos
		- Beacon chain with Rollback
			+ beacon chain verifies state transitions of other chains using
				a subset of validators 
			+ in case of problem chains are rolled back
			+ requires fast detection of problems and rollback should not happen
				too ofter
	* Near and Ethereum use a beacon chain with rollback
	* __NightShade__
		- all of the shards produce a chunk when put together produce a single block 
		- this block is produced with a regular cadence even wit missing chunks
		- single validator produce each block from the chunks provided 
			+ this validator is rotated through a pool of validators 
			+ the validator only accepts chunks, no transactions
		- for each shard and period a validator is assigned to produce its chunk
			+ if validator is not present, shar will be stalled for that period
			+ shard validators are also rotated and taken from the block pools
		- _hidden validators_
			+ provide additional security
			+ in charge of validating chunks 
			+ assignments of validators is done privately 
				* validator know which shard to verify
				* adversary must bribe a large number of validators
			+ number of hidden validators is random as well
		- distribution of roles
			+ if there are 100 seats/shards and 100 shards there are 10 000 validators
			+ 100 of them will be used to produce/verify chunks and blocks
			+ rest will be hidden validators
		- _fishermen_
			+ any node operator can participate without permisson 
			+ they fulfill the same job as hidden validators
			+ even if the whole hidden validator pool is corrupted they may be 
				caught by independent fishermen
		- preventing lazy validators 
			+ validators have to receive new chunk, download the new state and run
				validation on the blcok 
			+ they could choose to do nothing unless another validator submits a 
				fraud proof
			+ to prevent that, validators have to commit their decision (validity of 
				chunk) and reveal what they commited
		- preventing data hoarding 
			+ if a chunk producer is corrupted and refuse to profide sufficient
				data so hidden validators are unable to validate te chunk
			+ the chunk producer has to send out an erasure coded to other chunk producers
				* you can reconstruct the chunk with only 16% of the parties 
			+ the erasure coded part is necessary to continue the chain 
+ economic
	* every half-day is called an epoch
	* validators are paid a fixed 90% of around 5% of total supply of near token
		- the first year there is 1billion token
		- the first year a validator would get paid 45,000,000 tokens
		- rewards are distributed per epoch which is on average ~61,640
			of reward per epoch the first year
	* validators receives rewards propotional to their participation
		- validator stakes on how many seats he wants
		- rewards are proportional on how many chunks/blocks they produced compared
			to what they were expected to do
	* each time you pay for gas your NEAR tokens are burned instead of
		being redistributed to validators
		- that allows validators to be rewarded equally and not only
			the block producer
		- burning transaction fees still improve the yield of validators
	* contrary to classic blockchains, you can only store an amount of
		data proportional to your balance in NEAR tokens
		- for example, with 1 near you can store 10Kb on your account
		- as a consequence as data grows an important part of the coins will be
			locked for storage purpose
	* number of seats is proportional to the number of shards
		- seats are allocated through an auction
	* validators can receive tokens to stake via delegation	using smart contracts
		- useful to allow professional validators who might not have funds to participate
		- increase the total stake and thus the security of the protocol
	* smart contracts are rewarded
		- sustainable path for independent developer and operation
		- popoular libs can receive funds from clients using them
	* __incentives alignment__
		- users want security for assets and data
		- developers want adoption and a source of revenue

# IOTA 
- cryptocurrency for the Internet-of-Things
- the goal is to have micro-transactions using DLT
	+ the transaction fees have to be lower than transaction amount
	+ this is difficult in generic blockchain since you want to have an incentive 
		for block validators/miners
	+ blockchain categorize participants into different roles
		* transactions issuers
		* transaction approvers
		* you have to find a stable win/win relationship between them
- instead of blockchain use a _DAG_ called the __tangle__ 
	+ _Directed Acyclic Graph_
- __Vocabulary__
	+ a _site_ is a node of the tangle
		* a site contain transactions 
	+ a _node_ is an entity that issue and validate transactions
	+ _approvals of transactions_ are represented by direct edges between sites
	+ indirect approval is represented by indirect edges between two sites
	+ _tips_ unapproved transactions in the tangle graph
	+ _site height_ length of the longest path to the genesis site
	+ _site depth_ length of the longest reverse-oriented path to some tip
	+ _transaction cumulative weight_ its weight + Σ (weight of transactions it approves)
	+ _transaction score_ its weight + Σ (weight of transaction approved by it)
	+ _orphaned transaction_ transaction which will not be approved 
		by following transactions (in)directly
	+ _abra_ low level assembly which is ran by the nodes (abraVM)
	+ _qupla_ qubic high level programing language, translates to abra 
	+ _qubic_ protocol for quorum-based computation for smart contracts 
	+ _bee_ node components 
- _HOW TO add a transaction t_
	+ t must approve of two previous transactions
		* this work is considered as transaction fee
		* issuing transaction contributes to the network security
	+ t can be added to the tangle with direct edges to the approved transactions
- _HOW TO approve_
	* node chooses two transactions as it wish
	* node checks if the two transactions are not conflicting 
		+ does not approve conflicting transactions
	* then it can issue its transaction 
- _HOW TO issue a transaction_
	* find a nonce that solve a crypto puzzle similar to bitcoin
- _genesis transaction_
	+ this is the root transaction that created all the tokens
		* genesis transaction sent all the tokens to several _founder_ addresses
		* no tokens will be created in the future
	+ all transactions directly/indirectly approve the genesis transaction 
- _Weight of transactions_ 
	+ every transaction has a weight (positive integer) attached to it 
		* weight only assume values 3^n 
	+ weight of a transaction is proportional to the amount of work invested to it
		by its issuing node 
		* related to the quality of the nonce found
		* in bitcoin it could be how small the nounce is
	+ heavy weighted transaction are more important than light ones
		* when there are multiple sub-tangles, heavy-weighted one are prioritized
  + _cumulative weigh_
		* weight of a transaction plus the sum of thei weights of transactions 
			that (in)directly approve of it
- Stability of the system
	+ ideally we want the number of tips to converge on a value with time
		* converging to infinity would mean we'd leave many unapproved transactions
	+ number of pending tips depends of the meantime required for a tip to be approved
		* depends on tips selection strategy
		* during low load (few tips) it is related to the rate of incoming transactions 
		* if a transaction is not approved for a long time, it can be promoted by
			approving it with an empty transaction
- How fast does cumulative weight grow?
	+ an old transaction will have its cumulative weigh grow by λ the rate 
		of incoming transaction
		* we assume that all transactions have weight of 1
	+ 3 phases 
		1. does not grow, wait for a transaction to approve of it
		2. transition phase where and more approve of it (in)directly 
		3. the transaction is so old that almost all new transactions approve
			of it indirectly
- Attacker scenarios
	+ double spend 
		* similar to blockchain
		1. attacker pay a merchant, merchant sends the goods when the transaction
			has sufficient cumulative weight 
		2. attacker issues a double-spending transaction 
		3. attacker make it so the relevant transaction is the double-spend one
			- attacker create with many small transactions that approve the double-spend 
			transactions without approving the original transaction (in)directly 
				* using strong computing power
				* using plethora of Sybil identities 
			- attacker can use all of its computing power to give the double-spend 
				transaction a big weight
				* a solution to this is to put a limit on maximal weight of a transaction
				* is not sufficient if attackers just add lots of small transactions
		4. attacker hopes that the dishonnest subtangle outpace the orginal transaction
	+ double spend + subtangle prepared in advance 
		* prepare a subtangle with the doible spend transactions in advance locally
		* since attacker does it locally, it only consider its own transactions and 
			is not affected by network latency 
		* attacker is able to make a parasitic chain with lots of transactions which
			allow its chain to have high score and big height
		* MCMC tip selection algorithm:
			1. select sites with cumulative weight around W 
				- choose W such as it's not too close to the tips and not too far either
			2. make a parametrised walk with multiple particles starting from those sites 
				- walkers will prefer sites with big cumulative weight
				- parameters will choose how probable a node choose a site with less cumulative
					weight
				- note that this selection algorithm is local you don't have to explore the 
					tangle to choose next site
			3. Select tips that were reached by the particles 
				- reject the first particles since fast walk might be due to lazy tips
					which always approves of old transactions 
				- particles which are too slow may due to a subtangle produced 
					by an attacker with a chain of small transactions
				- there are more chances to stay in the main tangle rather than 
					parasitic tangle due to bigger cumulative weight
				- parasitic chains have less computing power therefore are lighter too
		+ double spend + try to keep two equivalent subtangles 
			* really hard to balance for the attacker 
			* if MCMC algorithm is well parametrized it should not work
		+ quantum computer 
			* theorically iota is more robust to strong hashing power 
			* the crypto puzzle is quite weak in iota so the improvement 
				using a quantum computer is not as big as in blockchain 
			* you also don't get improvement from the other steps to issue a transaction
# Governance 
- 5 aspects 
	+ how membership is granted
		* token membership 1token = 1 vote
		* endorsement from existing memeber 
		* automatic process for regular users
	+ how power within the organization is allocated
		* voting system
		* randomised lottery to choose executives for limited time 
		* based on contribution/reputation
	+ how preferences are aggregated to produce decisions
		* quadratic voting, conviction voting, futarchy
	+ how consensus is signalled
		* usually majority based
		* result can be a hybrid of multiple proposals depending on voting result 
	+ how created value is allocated
		* increase of token value 
		* pay dividends to member 
# Models of governance
+ models of governance directly affects the longevity of a blockchain
+ 2 critical components of governance
	1. Incentives
		- each group in a system have their own incentives
		- those incentives are not 100% aligned with the other groups
		- groups will propose changes over time that will be advantageous for them
	2. Mechanisms of coordination
		- since each group has its own incentives, the ability to coordinate
			is critical in how much they affect changes
		- coordinated group have bigger impact on decisions
		- if one group coordinate better than others it create power imbalances
			in their favor
+ Bitcoin
	- Actors of bitcoins and their incentives
		+ _developers_, increase value of bitcoins, social recognition,
			control over bitcoin future decisions
		+ _miners_, increase value of bitcoins, mined block rewards,
			transactions fees
		+ _users_, increase value of bitcoins, utilities of
			bitcoin (store of value, uncesorable transactions ...)
	- Coordination between actors
		+ off-chains, Bitcoin Improvement Proposals and mailing list
	- Results
		+ Similar to government
			* developers got legislative power by submitting pull requests
			* miners got judiciary power by adopting or not updates
			* nodes then enforce miners policy
		+ Asymmetries in incentives
			* developers only economic incentives is to raise the value of bitcoins
				which is something that cannot be really controlled
				- hence they got little incentives to work on bitcoins
				- there are no new developers hoping on the project, most
					would prefer starting a new project
				- __power gets concentrated into a small group of early developers__
				- early devs are usually more conservative
				- risk of developers getting bribed, since they got lot of power
					but weak economic incentives
		+ Asymmetry in coordination
			* miners communice easily since they are a small and concentrated group
			* mining is an economy of scale therefore coordination is greatly favorable
				to miners
			* __systems encourage miners to group up to gain economic advantage__
+ Ethereum
	- Mostly similar to bitcoin
	- By transferring to PoS
		* entry wall is much lower to be a validator/miner
		* __flatten the distinction between miners and users__
		* this reduce centralization risk that Bitcoin experiences
	- Developers remain the same
		* less conservative than bitcoin
			+ Ethereum was created to oppose Bitcoin rigid environment
			+ Vitalik is widely trusted
	- Weaknesses
		* over reliance in Vitalik (he is trying to take a step back)
		* limited incentives for core development
+ Tezos
	- anyone can submit a change to the governance structure in
		the form of a code update
		* on-chain vote occurs to see if any update goes to testing network
		* a confirmation vote takes place after a period to decide
			if updates go live
	- __powers over decisions transfer from developers/miners to users__
		* since all users vote
	- developers get rewarded through _inflation funding_
		* if users deem an update will increase the value of XTZ of _n_ %
		* Thezos can crowdfund a reward for the developers
		* users will generally give up to _n_ % of their XTZ
		* __developers get economic incentive through inflatino funding__
	- In general power is more evenly ditributed between developers/miners/users
+ Dfinity
	- Dfinity takes a step further than Tezos
		* it is possible to make retroactive changes to the ledger
		* it is possible to __rollback or edit the ledger and the rules of governance__
	- super flexible it allows smooth rollback with an on-chain coordination
		* it was possible with Ethereum too buth with friction and offchain coordination
+ Other governance systems
	- __Futarchy__
		* proposed in 2000 by Robin Hanson
		* slogan: "vote values, but bet beliefs"
		* protocol
			1. every user is polled once a year on how satisfied they are
				"Etes vous-satisfaits de la fréquence des frites à la cantine?"
			2. creation and publication of a proposal
				"Frites à disposition tout les mercredis et jeudis"
			3. split into 2 markets one with the "no" and one with the "yes"
			4. implement the policy with the best result at the end of a trial period
				+ revert all trades in the losing market
				+ market winner is decided by the value of asset at the end of trial
					(for example coin value)
				+ value of assets is decided by how promising each scenario is
					to users
			5. wait for maturity and measure success metric
			6. reward everyone on the winning market
		* pros and cons
			+ user are asked only on their satisfaction
				* avoid the "voter apathy" problem where individuals do not have
					enough incentive to study the impacts of candidates proposals
					that may be potentially harmful to him
			+ as the system evolve predictions will get better
				* individuals who are bad at predicting will be weed out
				* similar to capitalism for the production of private goods
					but here it applies to public goods
			+ reduces irrational social influences of the governance process
				* since there are economic incentives to win, it may encourage
					to focus more on proposals than personalities
			+ elegantly combines public participation and professional analysis
				* individual experts and analysis firms influence the decision
					by buying or sellign on the market and profit from
					good results
				* allows anyone to participate in the market and rise if succesful
			- a powerful entity or coalition can influence the market
				towards a particular result
			- markets are "volatile" since it's mostly based on copying what your
				neighbours do. This behaviour can be exploited
			- prediction market can be uncorrellated to the policy tested
				* prediction market can have a negligible impact compared to the
					volatility of the market
			- it is difficult to gauge satisfaction with a numerical metric
			- participation in the prediction markets may be low
				* competition between prediction markets is zero-sum
				* it is really ard to earn profit in the long-term
			- ultimately prediction markets is based on how you think on how
				the others will act and not on which is the best scenario
	- __Liquid democracy__
		+ system where everyone can vote for each bills
		+ you are allowed to
			* vote
			* delegate your vote
		+ mid point between direct democracy (France) and representative democracy (US)
			* it is different from representative decmocry since
				the duration terms are not fixed and can end at anytime
		+ Pros and cons
			+ anyone can be a representative
			+ people are not forced to make choice on proposals they don't understand
			+ delegates can be removed quickly if they don't follow their "promises"
			- you can buy votes
			- you cannot guarantee both anonymity and integrity of the votes
	- __Quadratic voting__
		+ a system where one buy votes, each additional vote is double the price
			* people purchase votes directly proportionally to the strength of
				their preferences
		+ variant
			* each voter have a budget vote which allow them to chose
				which proposal they want to back u
		+ to have a strong influence on the vote is costly which abides by
			the rules "it is too costly to rig the system"
		+ one person = one vote is susceptible to sybil attacks, hence
			the default of one coin = one vote of PoS

# FLP Impossibility Problem
- FLP because published by Fischer Lynch and Paternson in 1985
- Model
	+ asynchronous
		* weak model but not unrealistic
	+ fail-stop model
		* when nodes fail they stop to work correctly
		* more restricted than Byzantine
		* Impossibility result is threfore stronger
	+ reliable message system
		* TCP is a reliable protocol for message passing, this model is realistic
- _No consensus protocol is totally correct in spite of one (crash) fault_
	+ _totally correct consensus_
		* Validity, Agreement and Termination
- Formal Model
	+ N > 2
	+ message: (p, m)
		* p the destination processor
		* m contents of a message
		* messages are stored in a multiset (set where you can have copies of values)
	+ nodes can:
		* send(p,m)
			- place m in storage of p
		* receive(p)
			- returns θ if storage is empty
			- returns arbitrarily any m of θ if there are messages in storage
			- message delivery is non-deterministic
			- infinetly calling receive(p) will eventually give all received messages
	+ nodes have configuration which is its internal state
	+ non-faulty processes take infinitely many steps in a run
		- until reaching θ
		- _admissible run_
			* at most one process is faulty
			* every message is eventually delivered
		- _deciding run_
			* some processes decide according to the properties of a consensus
	+ a consensus protocol is _totally correct_ if every admissible run is
		a deciding run
	+ impossibilty result: there is some admissible run which is not a deciding run
	+ an algorithm solve consensus if for any initial state and any executions it will
		satisfy validity, agreement and termination
- __Common Coins__
	+ due to FLP result _deterministic_ protocol cannot achieve total correctness
	+ we can use randomised protocols to avoid being stuck 
		* some steps are randomly taken, which prevent repeating identical situation
			where you may get stuck
		* termination is probabilistic, number of rounds is not bounded
	+ ensure termination when all correct processes obtain the same result 
		of their coin toss
		* nowadays with threshold crypto we have almost perfect probability 
			that nodes will get the same coin result
	+ only accept a result if the value of a coin matches the majority

# Distributed System
- from Lamport: _a distributed system is one in which the failure of a computer
	you did not even know existed can render your own computer unusable_
- key words: nodes, partial knowledge, uncertainity
- Consider multiple models for your system
	+ __Timing Model__, notion of time between nodes
		* Synchronous Model
			- a node knows exactly how much time it takes to:
				+ process
				+ send a message
		* Asynchronous Model
			- no timing guarantees to process or send a message
		* Semi-synchronous
			- a mix of both
	+ __Interprocess Communication__, how is information passed by nodes
		* Message passing
		* Shared memory
	+ __Failure Modes__, how nodes behave when something goes wrong
		* Crash-stop
			- node is gone
			- algorithm can keep running even with missing nodes
		* Crash-recovery, node can recover
			- nodes can recover
			- need some sort of storage or information sharing to recover
			- tell details about performance of the algorithm
		* Omission Faults
			- algorithm tolerate faults from nodes
		* Arbitrary Failures Mode (Byzantine)
			- Everything can go wrong
			- Nodes can lie to each other
- Properties
	+ __Liveness__
		* good thing will eventually happen
			- process will eventually deliver a message
			- consensus will eventually happen
	+ __Safety__
		* nothing bad can happen
			- messages won't be invented from nowhere
			- blockchain won't fork
- __Consensus__
	+ Paradigm of agreement problems
	+ useful when nodes have to agree on a common action
	+ properties
		* __Termination__, correct process eventually decides on some value
		* __Validation__, if a correct process decides _v_ then _v_ was proposed by
			some process
		* __Agreement____, no two correct processes decide differently
		* __Uniform Agreement__, no two processes (correct or not) decide differently
		* __Quorums__
			+ how many correct processes need to be alive for the algorithm to work
		* __Consistency__
			+ atomic consistency (linearizability)
				- is it consistent if we linearize concurrent events
				- from an external point of view
			+ sequential consistency
				- just your view has to be consistent
				- events you're implicated in
			+ causal consistency
				- events orders are consistent logically (receive msg before answering)
	+ FLP Impossibility
			+ Termination, Validition and Agreement cannot be achieved at the same
				time in an asynchronous setup
			+ can be solved in synchronous systems
				* in synchronous systems __crash can be detected__ just by
					checking timeouts
	+ Consensus can be achieved with extra assumptions
		* "perfect failure detector"
			- external process
			- information about suspicious processes
			- completeness: crashed processes are detected
			- accuracy: correct process are never suspected
		* eventually accurate failure detector
			- strong completeness: eventually every process that crashes is permanently
				suspected by every correct process
			- eventual weak accuracy: there is a time after which some correct process
				is never suspected by the correct processes
		* how do you setup your failure detectors timeout?
		 
# CAP Theorem (Brewer theorem)
- _in a distributed storage you cannot have consistency, avaibility
	and partition tolerance at the same time_
	+ __consistency__, each read will return last write or an error
	+ __availability__, every requiest receives a non-error response (no guarantee to
		get the last write)
	+ __partition-tolerance__, system continue to operate despite an arbitrary numbers
		of message being dropped or delayed between nodes
- you cannot escape form network failure so partition-tolerance has to
	be supported, therefore you usually choose between consistency or avaibility
- in truth you never have a perfect partition tolerance and you have to maximize
	C, A or P depending on the use case 
	+ in wide area systems you should not forfeit P 
	+ NoSQL focuses on A first, C 2nd 
	+ ACID (atomiticy, consistency, isolation and durability) focuses on C 1st
- you can even have systems that changes of mode depending on the situation
	+ for example have a partition mode when detecting an inconsistency
		with a neighbour 
	+ in partition-mode you could restrict the possible operations, restriciting 
		availability during a limited time 
	+ the idea is to resynchronize the partitions to have consistency
	+ at the end of the mode, node could go back to their initial state
		where availability is maxed out

# Consensus algorithm
- Comparisons criteria
  + __Decentralized Control__
  + __Low Latency__
    * how long before a transaction is finalize on the public ledger
    * include the time for the chain to stabilize (in bitcoin it is recommended to
      wait few blocks before considering a transaction finalized)
  + __Flexible Trust__, users can trust any combinations of parties they see fit
  + __Asymptotic Security__
    * a paradigm to quantify the cost of adversaries breaking a crypto scheme
    * informally, an asymptotic secure scheme is one that's been conditionally
      proven to be harder than polynomial for the attaker to break
    * in blockchain it may refer to computing power
      - POW is not asymptotic because it is linear (you have to get 51%)
      - SCP is asymptotically secure because you have to insert malicious nodes
        in multiple nodes quorus
  + _Table Comparisons_
   | Decentralized Control | Low Latency | Flexible Trust | Asymptotic Security
   POW | ✓ | | |
   POS | ✓ | maybe | | maybe
   Byzantine agreement | | ✓ | ✓ | ✓
   Tendermint | ✓ | ✓ | | ✓
   SCP | ✓ | ✓ | ✓ | ✓
## Proof of Work
+ based on resources used to solve a problem
+ for a double spend you should work a considerable amount
+ cost of an attack (in 2019):
	* 823,000$ / hour for Bitcoin
	* 93,000$ / hour for Ethereum
	* under 20,000$ / hour for smaller blockchains
+ criteria
	* decentralization: medium
		- anyone is allowed to be a validator
		- but ther is an entry barrier due to the cost of mining
		- this weaken the decentralisation since you usually get mining pools
			which dominates the validator nodes
	* latency: big weakness
		- blocks are small (bigger blocks will increase entry price for validating)
		- low latency allows miners to auction validation of transactions (which
			benefit miners)
		- block speed has been set by the technology
	* flexible trust:
		- no verification required to be a validator
	* security:
		- for a double spend you require 51% of the computing power
+ weakness:
	* consume massive amount of energy
+ examples
	* Bitcoin
	* Ethereum 1
	* Litecoin
	* Dogecoin
## Proof of Stake
  + 2 version: one with stakes and one just your wallet
  + validators
    * validators have to stake coins to be eligible
    * you get rewards by forging and attesting a block
    * you lose your stake if:
      - you're not accessible (offline, or not reachable)
      - attest/forge a wrong block
      - attest blocks on multiple chains (which encourage forks)
      - work on the wrong chain (shorter chain)
  + block forging
    * among the validators one is chosen randomly
    * proportional to the amount of coins you got
    * how long coins have been staked may be taken into account too
    * block has to be attested by other validators
    * a node should not be chosen as a forger multiple times in a row
  + criteria
    * decentralization: good
      - cost to be a validator is the minimum stake
      - there also pool of stakers to reduce the entry barrier
    * latency: good
      - in theory bottleneck is the message passing between nodes which
        is extremely fast
      - you still have to wait for few block to have finality (since a longer
        chain may appear)
      - can scale well when combined with sharding
    * flexible trust
    * security
      - security is based on the fact that to have enough power you have
        to have a lot of coins
      - therefore making a double spend attack also harms yourself since
        you have a lot of coins
  + weakness
    * _Nothing at stake_: validators are encouraged to mine all the fork chains
      - it does not cost anything to attest a block
      - miners are not rewarded when working for a smaller chain
      - they should attest all potential chains to guarantee a reward
      - potentially facilitate double spend since all forks are encouraged
      - you may be punished if you validate on multiple chains (lose your stakes)
  + __Delegated Proof of Stake__
    * nodes vote their stake to elect representatives for
      a conventional BA
		* PoS is democrative while DPoS is representative democratic
## Byzantine Agreement Systems (BA)
  + based on voting
	+ safety and forms of liveness if systems contains 3f + 1 nodes
		* f is number of byzantine nodes
		* 66% of nodes need to be honnest
		* example:
			- Byzantine general problems is not solvable with 3 nodes
				+ loyal node cannot discern from lies from truth
			- works with 3 loyal generals and 1 traitor (4 nodes)
    * safety -> validity and agreement
    * liveness -> termination 
### Practical Byzantine Fault Tolerant Protocol (pBFT)
Provides state machine replication in a BFT context.
- Used by __Hyperledger Fabric__ and __Zilliqa__
- Rounds/Views of a pBFT
	1. Choose a node as a leader 
	2. Client sends a request to the leader 
	3. Leader broadcast the requests to all the nodes 
	4. Nodes executes request and send output to the client 
	5. Client awaits $f+1$ identical outputs from different nodes 
- Supermajority of nodes can detect a faulty leader node
### Federated Byzantine Agreement__ (FBA)
  	- instead of having a whole system agreement, you only seek
  		agreement of nodes that you trust
  	- __Quorums__
  		* a _quorum set_ is a set of nodes that you trust
  		* a _threshold_ is a minimum number of nodes in one nodes set that must agree
  		* a _quorum slices_ a subset of a quorum set that reach the threshold
  		* a _quorum_ a non-empty set of nodes that contains a quorum slice of each node
        - from paper: a non-empty set _S_ of nodes emcompassing at least one
          quorum slice of each non-faulty member
  		* a _blocking set_ any set of nodes in a quorum set which can prevent a consesus
  			for the node
  			- if a threshold is 3/4 then any set of 2 nodes is blocking
      * node v1 and v2 are _intertwined_ when every quorum containing v1 and
        every quorum containing v2 intersect in at least one non-faulty node
      * _FBA ensures agreement only between intertwined nodes_
      * set of nodes I is _intact_ if I is a uniformly non-faulty quorum such
        that every two members of I are intertwined even if every node outside I
        is faulty
        - SCP guarantees liveness under eventual synchrony for intact sets
        - I is impervious to non-intact nodes
  		* Protocol
        0. a node N begins by casting a vote and broadcast msg _A_:
          _Node: N, Quorum slices: Q, Value: V_
          + implies that it has never voted against V and never will
        - Node M receives _A_ from N and has to go through
          this 4 steps
  			1. I don't know anything about _A_ and have no opinion
  			2. I _vote_ for _A_, it's valid but I don't know if it's safe
  				* A is valid and consistent with my previous votes
          * implies it will never vote ~A
  			3. I _accept_ _A_ because my quorum slice support it, but I don't know
  				Happens if either:
  				* every node in my quorum slice voted for or accepted A
  				* my blocking set accepted A
  						- in this situation you're either stuck if you don't agree
  							with blocking set
  						- or you change your opinion and join your blocking set
  				if it's safe to act on it yet
          * no node in network of M can accept ~A
          * it's possible to accept another statement B even if node M
            voted for A if M sees a blocking set voting for B
  			4. I _confirm_ _A_ and _it's safe to act on it_
  				* happens if one of its quorum slice accepted _A_
          * rest of the network will also eventually confirm A
          * if A is confirmed no other node will confirm another value
      * protocol is safe (assuming enough intersections) but does not guarantee liveness
  	- Advantage over BAS
  		* even if a node adds millon of malicious nodes, they won't have an effect
  			until he convinces other nodes to include the bad nodes in their quorum set
  		* this is possible in traditionnal BAS
  		* BA is a particular case of FBA where everyone has the same quorum set
  	- Setting up quorums
  		* having enough reliable nodes in each node quorum set to be able
  			to reach a consensus (liveness)
  		* having enough quorums intersections to avoid forks (safety)
  			- quroum intersection iff any pair of quorums intersect in at least one node
          + meaning that we can't have fork, since the intersecting node
            will prevent any diverging value
  			- quroum intersection is necessary for safety but not enough to guarantee
  				safety (if intersecting node is byzantine)
  		* you need balance between too few or too much intersection
  			- too much and you may not have liveness with many byzantine nodes
- __Proof of Authority__
	+ works very well for a private blockchain
	+ only a selected pool of nodes are allowed to validate and add 
		transactions to the ledger
	+ nodes does not even need new blocks since forgers are trusted
	+ chain can be rollback
		* nodes have no benefits in acting maliciously
		* they can get blacklisted from validator nodes
	+ really efficient and does not require a lot resources to work
	+ allows company that do not trust each other but benefit from cooperation
		* for example consortium of banks
- __Proof of Capacity/Space__
	+ used by _BurstCoin_
	+ here nodes are favored to choose the next block depending on how much 
		memory capacity they have
	+ solutions to block hash are too difficult to compute in real-time therefore
		nodes compute as much potential solutions as its hardrive can store
	+ 2 phases 
		1. Plotting 
			* node plot its memory with possible solutions 
			* a nonce contain 4096 scoops (scoops are pair of hashes)
			* the more nonce you can store the higher the chance to forge the next block
		2. Mining
			* you calculate a scoop number between 0 and 4096, n
			* from all your nonce you select the scoop n 
			* among those scoops with number n you choose the scoop with the 
				best _deadline_ 
			* deadline is the time allowed between actual block and next block
			* if you're deadline is sooner than other nodes then you can forge the next
				block when the deadline is reached
	+ Pros and Cons 
		+ it's a more energy efficient PoW 
		- could lead to another race for better hardware 
		- attackers could ppl memory space to plot solutions (malware mining)
- __Proof of Weight/Importance__
	+ Variant of PoS where you don't increase your chance based 
		on how much coins you have at instant 
	+ you increase your chance depending on how much you contributed to the chain
	+ avoid the coin hoarding behaviour of PoS
- __Proof of Burn__
	+ Variant of PoS where you increase your chance to be choosen as next forger 
		depending on amount of coins you burned
- __Directed Acyclic Graphs__

- __Proof of work__
	+ node which fullfil a task the fastest get to choose
- __Leader__
	+ a leader node gets to choose for everyone 
	+ cons
		- susceptible to ddos on leader (single weak point)
		- not fair because the leader gets to choose everything
			* choose transactions, orders, timestamps
- __Economy based__ 
	+ economic rationality drives consensus
	+ the consensus algorithm tries to simulate e
	+ nodes bet on which block will become be part of the chain
		* if they are right they are rewarded
		* if they are wrong the get fined
	+ nodes naturally converges into a choice and we get a consensus 
	+ can get stuck
		* if no nodes choose a side by fear 
		* attackers collude to have a perfect split which encourages forks
	+ no accurate mathematical models or proofs yet
# Improving speed of blockchain
- increase block size
  + may be a short term solution
    * exponential growth cannot be handled with linear scaling
    * increasing 2x the block size just increase transaction speed by 2x
  + __bigger block size can encourage centralization__
    * if we keep increasing block size, having to store the chain will
      require tons of hardware and only big groups could afford to run a node
    * the entry barrier will be too high and we'd get more centralization
- shorten block time
  + it works in theory and also brings faster finality
  + it's also a solution that scale linearly
  + main issue is the increase _stale rate_
    * the amount of blocks which are mined but not added to the chain
    * happen when you find a block while another one is being propagated
  + __greater stale rate encourages mining pool and centralization__
    - with increase _stale rate_ you lose computing power and lower your chance
      to earn a block reward
      * in Bitcoin it takes, 6.5s for a new block to reach 50% of nodes and
        40s to reach 95% and the mean delay is 12.6s
      * therefore if block time decrease the ratio from delay to mining time
        increase bigger chance to create a stale block
    - stale rate is almost 0% in a mining pool, therefore by mining in a pool
      you improve your chance to mine the next block
	+ __GHOST protocol__
		* Greedy Heaviest Observed SubTree
		* protocol allowing shorter block time while avoiding centralization 
		* used by Ethereum 1
		* protocol that use the stale blocks due to the fast block time of certain chains
		  - having a lot of stale blocks favour centralisation since mining pool 
		  	can receive new blocks produced internally faster than the rest of 
		  	the network
		* stale block is block is a valid block which was produced but won't be 
		* part of the main chain
		* Ghost includes stales blocks/uncles in the calculation of which chain has 
		  highest cumulative difficulty
		* honest node travers the entire tree, at each depth block from the main 
		  chain corresponds to the heaviest subtree underneath
		* adding the uncles and nephews make the chain becomes a tree with a main
		  chain with peripheric uncles
		* uncle blocks and nephew (child of uncles) also receive rewards
- __SPECTRE__
	+ Serialized of Proof of work Events: Confirming Transactions via Recursive Elections
		* same creator as Ghost
	+ use DAG to parallelize chains 
	+ creating a block 
		* find a set B of blocks with 0 in degree
		* computes hashes _h < D_
		* create b with h and includes B in the header (edges to the block)
		* broadcasts b
	+ consensus over block ordering 
		* order between two blocks (x, y)
		* each block z of the DAG vote -1/0/1 
			- -1 if z has x in its past and not y
			- 1 if z has y in its past and not y
			- follow majority if z if before both 
			- if z is after both vote as if the dag only contained its past nodes
		* for every pair of blocks the protcol is done which gives a matrix 
			of preference orderings
	+ few characteristics 
		* vote in favour of visible blocks
			- honest miners build on honest blocks
		* majority amplification
			- honest blocks will side with earlier honest blocks
		* referencing recent blocks is beneficial
			- hidden chain lose out on vote and cannot get majority
			- building on recent honest blocks gives more votes
		* votes from the past counter pre-mining attacks
			- concurrent chain creation will mean that honest blocks will outnumber
				attacker blocks
	+ transaction finality
		* transactions without conflicts are more reliable the deeper they are in the DAG

# UTXO 
- Unspent Transaction Output 
- instead of just keeping an account balance you keep 
	track of all the transactions concerning an address
- each token is uniquely identifiable
- __Pros and cons__
	+ in case of double spend you can identify legitimate transfers from
		actual double spend
	+ useful if consensus is solved through voting 
		* voter may need additional info to choose a truth
	+ you can mark IOTA tokens to have a "meaning"
		* reserved for types of digital asset
	+ easy detection of repeated transactions or conflicts
	- you store more info

# Layer-2 chains 
- instead of putting all activity on the chain directly, you do most of
	the work off-chain in a layer 2 protocol
	* a smartcontract makes the transition between core and 2nd layer protocol
		1. process deposits and withdrawals
		2. check that all the off-chain work follow the rules
- 3 categories: state channels, plasma and rollups
	+ __state channels__
		- protocol 
			1. create an initial state of the channel through L1 blockchain 
				 between n parties
				> channel initial state: Alice = 50$ and Coffee_machine = 0$
			2. do an unlimited of transaction off-chain 
				> Alice takes coffee every day for a month
				> current state: Alice = 30$ and coffee-machine = 20$
				- each new state is signed by both parties 
			3. close the channel and commit the final result to the L1 chain
				- only the last new state signed by both parties is commited
		- possible issue: party may decide to close channel early to avoid disadvantageous				last transaction 
			+ to avoid that a SC is generally setup at creation of the channel as a
				"judge" so that channel closes correctly
		- to avoid creating channels to everyone in the network, you can have a P2P 
			network and transactions pass multiple channels through intermediate nodes 
			to reach the destination account
		- pros and cons 
			+ great privacy: all transactions are done between the defined entities
			+ instant finality: each state update can be considered final
			- need 100% availability from participants: otherwise other parties may
				use your absence to close channel early 
			- defined set of participants: the judge contract need to know all the 
				participants to lock a state
			* made for situation with many updates over long time: the inital creation 
				is costly but all the intermediate transactions are almost free
	+ __side chains__
		- separate chain attached to its parent chain/main chain using a two-way peg
			* you can move assets between side and main chains
		- user send their coins to a specific address where the coins are locked
			* when those coins are locked a confirmation is transmitted between chains 
			* these coins are then materialized in side chains where they can be used
			* reverse happen to return assets to main chain
		- a federation is a group that serves as intermediate between main and
			side chains 
			* determines when and how coins are passed between the chains
		- side chains are independent and have their own consensus, governance...
			* if side chain is hacked the damage are contained into the side chain
		- pros and cons 
			+ sidechains are permanent: you can join a sidechain at any time, you
				don't need to create your own
			+ allow crypto to interact with one another
			+ allow to test different types of governance 
			- need a lot of initial investment
			- federation is needed for sidechains
			- may have lower security
	+ __rollups__
		- execute transactions outside L1 but data are posted in L1
		- rollups usually contain some core SCs to verify proofs and transfer money
		- 2 types of proofs:
			+ Optimistic rollups with fraud proofs
				* we assume that data commited is valid
				* a user can challenge the data with fraud proofs 
				* L1 will redo the computations to judge the challenge
				* parties who was wrong or behaved maliciously will be stashed some crypto
				* pros and cons 
					+ simple and fast if no bad behaviour
					- to able fraud proofs, solidification time may be long
			+ ZK rollsups with validity proofs 
				* each batch of data commited come with a validity proof
					+ the proof is zero-knowledge
				* pros and cons 
					+ faster withdrawal time 
					- much more complex
						- we don't have EVM ZK-compatible yet, SCs has to be rewritten
						- nodes creating the validity proofs need to have computation power



# Securiy
- How do we model attacking nodes?
  + Normal-case, everyone is good but are uncoordinated
  + Byzantine fault tolerance, a percentage of miners are attackers
  + Economic, there is an attacker with a budget of X$ which can use
    however he wants
    * can buy hardware
    * bribe nodes
- Security
  + 51% attack, when an actor is able to forge the next block more than 50%
    of the time
    * allows double-spend where the attacker can present a longer
      chain at any time
    * relevant on chains where longest chain is main chain is applied
  + Sybil attack, attacker flood the network with false identities
    * relevant in consensus based on voting

# Economy
- Blockchain is an extension of Internet in the sense that it can  
  transfer values on top of data
- What is money?
	* Token representing the time and effort of a person
		+ you get paid for you time and effort dedicated to a task
	* Fiat money is a social coordination ame in a constrained environment
		+ Monopoly bills are money when playing monopoly
		+ $ is money within the US borders
	* Money needs certain attributes 
		+ gold been has been the dominant store of money for thousands of years
		+ can't be easy to produce money
			- gold/silver is used as money since it's hard to reproduce
		+ fairness 
			- gold can be found more or less all around the planet
		+ money is constituted of 2 parts
			1. the subjective: the social coordination
			2. the objective: the technology/vehicle 
				* determine what is the money
				* paper & ink, gold, distributed ledger
			- DLTs are competitive both on the subjective and objective parts
- Bitcoin and Ether are the based money in the internet world
	* base asset/money or M0 is money which don't have dependency 
		+ you don't need an intermediate to trade with it
			- such as your bank account
		+ gold was, cash is base money

# Identity online
- Benefits of IOTA identity
	+ for me
		* prove who am I
		* own my own data
		* easily use my data to services from a single source
	+ for companies 
		* same as an individual with an org id 
		* reduce data responsibility by not storing them and using customer id
		* use device or asset ids to optimize processes
- Decentralized identities 
	+ data sharing: what do I share, to whom?
	+ indepedent auditing:
	+ building trust: the data I share is trustworthy
- Data sharing: verifiable credentials 
	+ 3 parties: issuer/holder/verifier
		* ex: bachelor degree 
		* issuer is university, holder (me), verifier: a company that wants to hire me
- 
