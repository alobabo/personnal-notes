######################## Side-Channel ######################
#Differential power analysis
- Goal 
  - Teaching material about
  - Present Simple Power Analysis (SPA)
  - Present Differential Power Analysis (DPA)
- _SPA_
    attack which allows one to guess the info flow of an execution
  - directly get a power consumption trace of an execution
  - branch and special operands may have peculiar power consumption signature
  - it is possible to deduce the sequence of instructions that was 
      executed and retrieve secrets
  - Defense
    + branchless
    + hard-wire hardware implementation of crypto algorithms have small variations
        of power consumptions
- _DPA_
    statistical attack allowing one to guess secret values by analysing the correlation
    power consumption and public outputs
  - for example the same secret key is used to compute multiple cyphertexts
  - by having the power consumption trace of each cyphertexts we can retrieve the key
  - focus on power consumption correlation between the data being computed
    + these are generally weak and can be confused with noise
  - attack using statistics to get secret values used for _m_ executions
  - we compute and trace a DPA funciton for which 
    + if our guess of a secret is true then a peak will be osberved
    + otherwise the DPA will stay close to 0
  - Defense
    + reduce signal size
      * balancing hamming weight
      * chosing less costly operation
      * shielding the device
      * however DPA can still be used if we have an ∞ observations
    + introduce noise
      * can also be used with randomize execution timing and order

# maskVerif: automated analysis of software and hardware higher-order masked implementations
- Contributions
  + automated tool for proving different notions of security of 
      masked implementations for software and hardware models
  + intermediate representation called MaskIR, capturing threshold probing model, 
      bounded moment model and threshold probing model with glitches
  + efficient implementation with experimental results
- Masking
  * transform every secret variable _x_ into n shares
  * t_0, ..., t_(n-1) are random values
  * t_n is chosen such that t_0 * ... * t_n = x, with * a specific law
  * correctness, sum of output shares is equal to applying 
  the function without masking
  * incompleteness, each output share is computed by at most t input share
  * uniformity, output sharing must be uniformly distributed if the input sharing is
- Security notions
  + example with two inputs but scale to n inputs
  + threshold probing security
    * for every two tuples of inputs (s,s') and (u,u') and for every observation O
    * we have L(μs, μs') = L(μu, μu')
    * with μ the uniform distribution over all tuple of inputs (x0, x1...xn) 
        such that s = x0 + x1 + ... + xn
  + non-interference
    * if x ~I y ∧ x' ~I' y'   (equivalent on indexes of I)
    * then L(x,x') = L(y,y')
  + strong non-interference
    * like NI
    * but we differentiate between internal and output observations
    * if x ~I y ∧ x' ~I' y'   (with I and I' smaller than size of internal observations)
    * then L(x,x') = L(y,y')
    * is used for compositional reasoning
  + increasingly stronger definition
    strong NI ⇒ NI ⇒ threshold probing security
- Leakage models
  + threshold probing model
    * adversary chooses a set of intermediary variables of his choice
    * leakage is the joint distribution of the intermediate values
    * secure if leakage do not reveal anything on secret
    * convenient and simple, useful for software implementations
    * does not capture glitches attack
  + robust threshold probing model
    * robust to glitches, hardware model
    * glitches may break locality of leakage
        → leakage may depend of the present state of the computation
    * observation of a gadget (within the same combinatorial logic) 
        reveals all its inputs
  + robust Probing Model with Glitches
    * adversary can select modified implementations on which 
        the observations will be made
  + the two last models are equivalent


######################## Information-Flow #######################
# Nonmalleable Information Flow Control 
- Goal 
  * introduction of __transparent endorsement__
  * which is the dual of robust declassification
  * intoduce nonmalleable information flow
- Contribution
  * identify transparent endorsement and nonmalleable information flow
  * introduce a the NMIFC language which provably enforces
      robust declassification, transparent endorsement and nonmalleable info flow
  * formalise the three security property as 4-safety proerty
  * implemention of NMIFC with Flame a lib of limited flow information
# Faceted Secure Multi Execution
- Goal
  * Multi Facets is an optimization of SME
  * however termination sensitive is not possible with MF
  * SME can remove termiantion leaks using the  irght scheduler
  * Fuse the two approaches to have the best of both world
- Contributions
  * FSME combination of SME and MF, two SME threads with MF executions
  * Multef a framework providing MF, SME and FSME execs
  * soundness proof of Multef in Coq
  * Implem and bench on Multef

# Multiple Facets for Dynamic Information Flow
- Goal
  * Static and dynamic analysis are in theory good for NI,
      however in practice you may reject a lot of secure programs
  * Secure Multi Exec guarantees NI but requires 2^n process
      if the number of code block protected is n
  * Here is presented Mutiple facets value which has the guarantees
      of SME combined with the efficiency of single executions
    + faceted values that simulate multi-exec
        + most of the time faceted values are equal so single exec
        - faceted values can be numerous 
- Comparing different strategies
  + Naive
    * skip the execution of code/branch dependent on secrets
    * this strategy may reveal private data due to intricate implicit flows
  + Non-Sensitive Upgrade
    * halt executions when updating public value in secret context
    * The issue here is taht we get stuck
  + Permissive-Upgrade
    * rather thatn stopping the execution say the public value updated in
        private context has an unknown value
    * accepts more program than NSU
    * you still get stuck if an unknown value is reused
- Contributions
  + Description of faceted value as a dynamic info-flow analysis
  + proof that it achieves termination-insensitive NI
  + proof that it simulates 
      + Non Sensitive Upgrade
      + Permissive Upgrade
      + Secure Multi Execution
  + implementation and compare perf with SME in Firefox
      + better results when principals number increase (complexity of the multi-exec)
  + have a conservative 1-safety property to prove NI a 2-safety property
  - Weaker that SME for termination

# Dynamic vs Static Flow-Sensitive Security Analysis
- Goal
  + Tradeoffs between static and dynamic flow analysis
- Contributions
  + impossibility of sound purely dynamic info-flow monitor, of Hunt and Sands definition 
      for flow-sensitive analysis
  + framework for hybrid mechanisms
  + soundness of the framework termination-insensitive NI

- Ideas
  + Flow sensitive analysis: security levels of variables can change 
  + Static is good because no runtime cost -> Denning style
  + dynamic is more permissive than static -> Sabelfeld style
  + for every program typable in a flow-sensitive type sys, there
      exists an equivalent program typable in a flow-insensitive type sys
    * only apply to compiled code (to do the transformation)
  + NI is not a safety property
    * safety property is a property which holds on a single execution
    * NI is a "2-safety" property, we need to compare two executions
  + here what is enforces is an overapprox of NI

- attacker model
  - public outputs
  - public part of initial memories

- monitors
  + monitor certain steps of execution
  + monitors when detecting an unsafe configuration can either 
    * stop an execution
    * supress outputs
    * output default value in public channel
  + monitors are purely dynamic:
    * does not look ahead of current configuration  (look ahead)
    * does not inspect branches that are not taken  (look aside)
  + sound to non-interference
    - progress-insensitive
      * given two low-equivalent initial memories
      * we obtain two traces generated by the monitored executions (only low outputs)
      * traces are either:
        - the same
        - one of them is prefix of the other 
  + permissive
    * at least as permissive as Hunt-Sands type systems
    * if a command _c_ typecheck, then an unmonitored and monitored execution
        of _c_ results in the same configurations modulo auxiliary commands of the monitor
    * auxiliary commands help the info-flow analysis but do not change outputs
  + it's impossible for purely dynamic monitors to have soundness and permissive
    * counter example with "dead" branch that leak secret
    * due to dynamic nature we do not check the dead branch

- hybrid framework
  + monitor configuration evolves with commands
  + monitor reacts to internal events α and produce γ 
    * equal to α if it is safe
    * different if the monitor intervenes
  + this internal events change the configuration of the monitor to complement
      its dynamic only behaviour
  + soundness is proved, for any command and configuration the execution will be safe
  + proved that the hybrid is at least as permissive as the type system


# Noninterference Through Secure Multi-Execution
- Goal
    + Do non-interferent executions of programs
- Contributions
    + Secure multi-execution for enforcement of non-interference
    + prove soundness and precision for secure multi-execution
    + Implementation on a JS engine with benchmarks and analysis

- Secure multi-execution
    + Execute a program multiple times with different security levels
        * execution at L have the H input set at _Undef_
        * only outputs with the same level as the execution are kept
        * concatenate all outputs for 
    + Non-interference
        * Here low executions do not have any way to use H variables so the property
            is naturally enforced
        * termination and timing sensitive
            - for any given number of step _n_ the public outputs agree for 2executions 
                if public inputs agree
    + Execution
        * Serialized works but multiplies times by the number of security level
        * parallel 
            - fast but may have lock if H exec needs L input
            - memory demanding
    + Precision of Secure multi execution
        * Number of pair (P, I) (program and input) such that
            execution of SME gives same observables that classic execution of (P,I)
        * This is true for at least all non-interferent programs 

# Dynamic Leakage - A need for new quantitative information flow measure
- Goal
  + give a leakage measure for a single execution and not for a whole program
- Contributions
  + present multiple definition of info leakage as a difference of knowledge about 
      secrets before and after observing outputs
  + show that they are not suitable for dynamic leakage
  + present belief tracking and how it fares for dynamic leakage

- Quantitative information can be seen as the decrease of an attacker uncertainty after
    observing some public outputs
  * The usual quantitative info-flow measure concern whole program
  * In the case where we have a distribution π on the secret inputs,
      we want to measure the leakage when observing a certain output
    + _Info flow as Shannon entropy_
      - Also called attacker uncertainty
      - Leakage is the difference between the amount of information known on secret before
          and after observing outputs
      - determine the quantitative info-flow using Shannon entropy
        * H = Σ - pi log(pi)
        * H is maximum with an uniform distribution
        * H is equal 0 if we have an event with a probability of 100%
      - the leakage of a program is defined by
        * L = H(π) - H(π|Outputs)
        * difference of entropy on secrets without and with knowledge of the outputs
      - not fit for a single execution because the result may be negative which is counter-intuitive
    + _Info flow as Rényi's min entropy_
      - vulnerability of a program is the probability to guess the secret at the first try
      - best strategy for the attacker is to pick the most probable secret
      - can be computed with or without observing outputs of an execution
      - min-entropy is defined
        * S = -log(V) 
        * V is the vulnerability
      - Leakage is defined as before 
        * L = S(π) - S(π|Outputs)
      - same as before may give a negative number for a single execution
    + _Channel capacity_
      - A measure calculated by taking the maximum leakage of a program given any distribution 
          of secrets π
      - Useful when we do not have knowledge on the input distribution

- _Belief Tracking_
  * how accurate is the attacker compared to the actual secret
  * Distance between two distributions
    + D(p->p') = Σ_s p'(s) log (p'(s)/p(s))
    + relative entropy between two distributions
  * Leakage
    + L = D(π -> p_s) - D(p_(S|o) -> p_s)
    + p_s a concrete distribution of secret (100% for 1 value and 0 for the rest)
    + Compares the distance with the actual secret before and after observing output



# Cryptographic Enforcement of Language-Based information erasure
[Link](http://people.seas.harvard.edu/~chong/pubs/csf15_crypto_erasure.pdf)
[Date](2015)
- __Motivation__
    + have an information-flow based language that can work with insecure storage
    + to guarantee secure storage, it stores crypted data and only erases the secret key 
        to erase these data
- __Contributions__
    + semantic for information erasure
        * we consider that secrets are stored in the initial memory _m_
        * attacker has access to source code
        * security levels are either a constant or a variable which value evolves when
            its conditional var is set
            --- this allows to modelize info erasure
            --- highlty secure variables are those that shall be erased
        * def:
            1. _attacker knowledge_, set of initial memories that can be possible 
            based on the attacker observations of the executions
            2. _indistinguishable memories_, set of inital memories that an attacker,
            should be able to see, based on program erasure policy
        * program is secure if 2. ⊆ 1.
    + flow-sensitive type sytem, that provably ensures info erasure for an 
        imperative language with cryptographic primitives
    + proof of soundness of the type system
        * the non-terminating configurations are not treated, _termination-insensitive security_
        * _relabeling judgement_ to compare strictness of erasure policies
            > p ≤ q if enforcement of policy q on some data implies enforcement of policy p on the same data
        * two difficulties:
            1. non-interference like properties, however output channel can display data from from 
            higher security levels depending if the conditionals are set or not
            2. erasure policies are dynamic whereas the type system is a sound and accurate static approx 
            of these policies
            --- requires strong invariants on branch and loops, when they might change a variable value
    + an implementation called KEYRASE

# Declassification: Dimensions and Princples
- __Motivation__
    + Declassification policies are hard to define and often incomplete
        1. Compares multiples existing policies and try relate them to another 
        balancing pros and cons.
        The classification done is over 4 axes: what? who? where and when
        2. Present the common principles used in declassification

- __Classification__
    + What
        * Partial release: what part of the information can be released
            - which part of the information
            - amount of information 
        * partial release can be expressed with equivalence relations (Partiel Equivalence Relation)
            - if parity of the secret is released then to our attacker all 
                even numbers are equivalent (same for odd)
                > ∀m,n m Parity n → s(m) Id s(n)
                m and n have same parity, s models the attacker observations after computation
            - introduced by Cohen
        * equivalent approaches
            - delimited release (Sabelfeld and Myers)
                + Some expressions can be declassified
                + If the two states vary only on secret values AND declassified expr
                    are equal then the observable effects are the same in both executions
                + in terms of PER, if the set E of declassified expressions are 
                    equivalent on states _s_ and _t_ and low memories are equal
                    then the low observable are equal in both executions
            - relaxed noninterference (Li and Zdancewic)
                + labels with lambda-term expressing how data can be leaked
                + labels are ordered in a lattice based on the amount of information leaked
    + Who
        * who release the information is important too
        * Label with ownership 
        * Robust declassification
            - if passive attackers cannot distinguish between two memories 
            neither can the active ones
        * Label with robust decla
            - no change in the attacker controlled code extract additional info on secrets
    + where
        * level locality, from which lvl to which lvl can info be released
            - intransitive noninterference
                - usually if L1 ≤ L2 ≤ L3 you can also have info from L3 to L1
                - here you can go from H to Declassified, Decl to low and L to H but you 
                    cannot go directly from H to L
        * code locatity, where is the info realeased in the code
                - make sure the info realease only happens 
    + when
        * time-complexity leak
            - attacker can only learn secret after polynomial tim eof computation
        * probabilitic approach
            - attacker can only learn secret with a probability under ε
        * relative
            - when/in which conditions a variable may be downgraded

- __Release Principles__
    + Semantic consistency
        - The security of a program is invariant under semantics-preserving transformation 
            of declassification-free subprograms
        - can be extended with info-release if release does not leak more
    + Conservativity
        - programs with no declassifications are non-interferent
        - program is secure if
            * it's either non-interferent
            * ot it contains declassification and satisfies the info release policy
    + Monotonicity 
        - adding declassifications to a secure program cannot render it insecure
    + Non-occlusion
        - declassification cannot produce other information leak
    + Trailing attacks
        - appending an insecure programs with fresh variables 
            to a secure terminating program should result in an insecure program


################################### Secure compilation #########################################

#What you get is what you C
- __Motivation__
    + Developpers have to find way around to keep side-effect of their code
        avoiding compiler optimizations
        * obfuscating branchless code to avoid opt
        * erasure safety
        * list of different tricks found
           - secretGrind, taint tracking tool to see duplicated and leaked data
           - volatile 
           - memset_s
    + This tricks lose in efficiency as compilers get more and more smart
        A new version of a compiler can understand and remove the side effects instructions
    + The compilers should provide tools to control side-effects with minimal overhead
- __Contributions__
    + nice biblio
    + builtin implementations of functions controlling side effects in Clang
        - one for branchless constant time selection
        - annonated __zero_on_return__ function to zero everything 
            * registers
            * stack
            * signal handling with saved state
            * asynchron API
            * Multiple versions:
                - Function based,
                    the erasure routine tracks the register and the stack slot that 
                    are used by the annotated function
                - Stack based
                    only the stack is tracked and all the registers are
                    erased before returning
                - Call-Graph based
                    A call graph is created during compilation tracking stack and register usage
                    The erasure is then implemented statically in the code without trancking code
                    whereas the other approches track usage dynamically

#The Correctness Security Gap in Compiler Optimizations
Even if compiler optimizations are proven to be sound or devoid of bugd, they 

can still cause security issue.
- __Optimizations vulnerability examples__
- Dead Store Elimination
    > int password = 0xc0de;
    > ... // use the password
    > password = 0x0; //scrub memory (remove from persistent data)
    + DSE will remove the last assignment since the variable _password_ won't
        be used anymore
    + eventhough this step is necessary to wipe it from memory
- Code motion
    + opt which changes code order
    + for cache miss, loop hoisting ...
        > int pwd = 0;
        > if (secret)
        >   pwd = 0xc0de;
        becomes
        > int pwd = 0xc0de;
        > if (!secret)
        >   pwd = 0;
    + this can happen if the non secret branch is _hotter_
    + the code will be present in public exec too
- Inlining
    + inlining cause two stack frame to fuse into one
    + it causes to increase the longevity of local variables
    + hence a local secret can be accessible from the calling function which
        was not foreseen
- Undefined behaviour
    > int size = n * sizeof(int);
    > if (size < n)     //check if the interger size has overflowed
    >   exit(1);
    + undefined behaviour has no specifications for compilers, hence the 
        overflow check presented could be removed
- Common Subexpression Elimination
    > if (secret) {
    >   key = k[0] * 15 + 3
    >   key += k[1] * 15 + 3
    >   key += k[2] * 15 + 3
    > } else {
    >   key = 2 * 15 + 3
    >   key += 2 * 15 + 3
    >   key += 2 * 15 + 3
    > }
    becomes
    > if (secret) {
    >   key = k[0] * 15 + 3
    >   key += k[1] * 15 + 3
    >   key += k[2] * 15 + 3
    > } else {
    >   tmp = 2 * 15 + 3
    >   key = 3 * tmp
    > }
    + open hidden channels, for timing attack
- Strength reduction
    + how processor intensive an instruction is
    + can replace classic arithmetic by a bitwise operation
    + can also open vulnerability for timing attacks
- __Analysis of the correctness-security gap__
    + the source abstract machine, abstract machine at the source lvl
        * abstract machine has a memory model with innacuracies
            1. structure of the machine
            - C memory model is based on Harvard architecture
            - which assumes separation between code and data
            - machines used today are closer to bon Neumann architecture
            - instructions and data both resides in memory
            - which is the core of numerous exploits
                2. undef value
            - formal models do not authorize undef value for computations
            - however in realitiy there is no undef and old memory is used
                3. persistence assumtions
            - formals models assumes that stack and memory contents are not 
                persistent
            - a block that has been deallocated can't be accessed again
                4. abstractions which ignore details not relevant to correctness
            - load and store at processor level
            - time and power consumption
- __Refinements of the source machine__
    + refine abstract machine to take in account details important for security
        guarantee
    + persistence of data
    + time and power consumption

#Securing Compiler Transformations
This paper focus on preservation of security guarantees 
and especially information leakage which is really close to our subject
- Issues
    + Compiler Correctness is now well-known issue and multiple solutions exist
    + Now we're interested in securing security problematics in compiler transformations
    + Especially here we talk about information leakage 
- Contributions
    + Explanations on why it is much harder to validate erasure than correctness 
        * make a proof with DSE as an example
        * Something like correctness is Ptim
        * Erasure is PSPACE-HARD for finite state programs
        * Undecidable for general programs
    + propose a secure algorithm for DSE
        * the algorithm relies on taint-tracking
        * gives a semantic of the taint-tracking analysis
        * and use it with the new algorithm to prove it secure
    + Propose an interesting theorem to easily prove the security of 
        some transformations
        * The theorem states that correctness is enough for information-flow
            subject to few conditions
            - _R_ refinement relation for correctness
            - _S_ and _T_ source and transformed programs
            - _=L_ agrees on low level vars
                > R(s_0, t_0) /\ R(s_f, t_f)
                >            =>
                > t_0 =L t_f <=> s_0 =L s_f
            - If the transformation does not touch high variables then the transfo is secure
        * useful for constant propagation...

####################### Secure refinement ######################
# Preserving Information Flow Properties under Refinement
- Issues
  + in stepwise development process we start from 
    * an abstract specification, what we want to do
    * to a concrete system, how we do it
  + Safety and liveness properties are preserved under such refinements
  + However information-flow property are not preserved under refinements

- contibrutions
  + present refinement operators that preserver the perfect security property
  + show that these operators are optimal

- System
  + attacker only observes trace of events
    * e1.e2.e3 ...
    * events have domain of security, for example D = {L, H}
  + information flow
    * perfect security property
      - an observer of level L who knows the spec of a 
      system (set of possible finite traces) cannot deduce any 
      information about occurences of events with domain H
      - t ∈ Tr ∧ t|L ∈ Tr
        + Tr set of all traces of the system (spec)
        + t a trace with at least one event with domain H
        + t|L projection of t on L
        + both may be executions of the system
  + to prove information flow properties it's easier to have more local conditions
    * but it's easier to understand the info flow properties with simple systems
    * enhance the systems with system states in the trace of executions

- Specifications and Refinement
  + C is a refinement of A if, TrC ⊆ TrA
  + safety and liveness properties can be modeled by a set of properties P
  + P is refined by C from A if:  P ⊆ TrA ⇒ P ⊆ TrC
- Contributions
 
# Maintaining Information Flow Security under Refinement and Transformation
- contributions
  * schema to specify secure information flow properties
  * refinement that preserver such poperties
  * definition of secure transfomation that preserve such properties
- transforamtion vs refinement
  * refinement
    + stepwise development by removing underspecifications from spec to spec
    + possible traces of transformed is included in the possible traces of the source
  * transformation
    + computable function mapping spec to spec
    + a refinement is a transformation
- secure information flow 
  * specifications are a set of possible traces
  * secure information flow is composed of:
    + flow policy, like there should not be flow from H to L
    + secure information flow predicate, like every knowledge should contain at least one trace
    without a high level event
      More precisely each knwloedge or low level equivalence set can be closed by a criteria
  * unpredictability
    + a program has secure information flow if there is enough unpredictability 
    from the attacker after an observation
    + this unpredictability may be reduced due to refinement which removes some possible traces
- limited notion of refinement in STAIRS
  * a specification is a set of obligations
  * an obligation is a set of traces
  * for all source obligation there is a transformed obligation that is smaller
  * for all trace obligation there is a source obligation that is bigger
  * traces of an obligation offer underspecification
  * obligation provides unpredictability, therefore an implement has to 
  fulfuill all obligations of a spec
  * also called property refinement or behavioral refinement
- transformation
  * an abstract trace may correspond to multiple concrete trace
   → different manner to implement the abstract
  * an abstract event may be decomposed into multiple concrete events
   → must handle issues at the concrete level
  * there may be concrete traces which have no equivalent at the abstract level
   → some concrete events does not exist at the abstract
####################### Safe compilation #######################
#Beyond Good and Evil
Introduce new notion of security 
- Secure Compartmentalizing Compilation (SCC)

    1. Gives a formal definition of SCC
    * separate a program in multiple components
    * __fully defined__, a component C is fully defined in respect to interfaces Bis if
        for any components Bs respecting interface Bis, C behaviour cannot go undefined/wrong
        1. for every partitioning between compromised and uncompromised component Cs are fully defined in respect to Bis 
        2. If the a variant of uncompromosed components Cs and Ds show distinguishable behaviours at _low level_ for compromised components Bs, 
        then there exists compromised components As which see distinguishable behaviours between Cs and Ds at _high level_

        2. Compares it to formal abstration
    + show how from formal abstraction, we can reach SCC
    + _structured full abstraction + separate compilation = SCC_
    + full abstraction limitations
        1. only applicable to safe languages
        2. attacker model is too _open_
        3. statically know which components are trusted/untrusted
    + solutions
        1. new def of full abstraction for unsafe languages
        2. _structured full abstraction_
        3. _separate compilation_

        3. Gives an implementation of SCC on a simple imperative language

#Lightweight Verification of Separate Compilation
Talk on correctness of separate compilation.
1. Intro
    - CompCert ensures correctness of whole programs compilation.
    - The next step is to prove _compositional compiler correctness_
    - However the technique found in the litterature aim for flexible
        form of correctness trading off with severe complications in 
        proof efforts

        2. Contributions
    - Proving a restricted form _compositional compiler correctness_
        with minimal proof effort
    - Compositional correctness level A: Same compiler with same options
    - Compositional correctness level A: Same compiler with different options/optim

        3. High level overview
    + CompCert
        - Prove end-to-end correctness with per-pass correctness and transitivity
        - How to prove per-pass correctness?
        - if states _s_ and _t_ are bound by a simulation _R_ then 
            the observables during the executions from _s_ and _t_ match
    + Level A
        - Need to prove for every pass C of the compilation process that:
            > C(p1 + p2 + ...) = C(p1) + C(p2) + ...    with C meaning transformation
        - Need to make sure that analysis are
            1. Correct in whole program setup
            2. Monotone -> Conservative in fragmented setup
    + Level B
        - We focus on optimizations in RTL, for each optimisation C we need 
            to prove:
            > C(s.rtl) = t.rtl
            > S = u1.rtl + ... + s.rtl + ...         u1 and ... are arbitrary other modules
            > T = u1.rtl + ... + t.rtl + ...
            > --------------------------------
            > Behav(S) ⊇ Behav(T)
        - Output of compilation C must refine the behaviour of the source s.rtl 
            in a context with other external modules
        - To prove that every optimisation respects the above mentioned:
            1. change the simulation _R_ of CompCert into _R'_ where
            > R' = | R   when executing s and t
            >      | Same instructions preserve R' since other modules are identical


####################### Miscellaneous #######################
#Build System Rules and Algorithms
1. Term
    - DAG, directed acyclic graph
    - Alpha system (global DAG)
        + Make, SCons, ANT...
        + O(n) time, n number of files
    - Beta system
        + O(log2n), n number of files

            2. Making of a good build system
    * Scalability
        - the build time should be proportional to the number of modifications done
    * Correctness
        - given a change A, applying A -> build -> remove A  should give the same state
    * Usability
        - There should be an unique way to build the system
        - You should not ask yourself:
            + Should I start for a clean build?
            + Should I build from top dir?
            + Is a fast build enough?

                3. Alpha build system
                When updating:
                1. Update the DAG
                2. Build the program
                Why is it linear time at best?
                __update the dag__
    - Because for phase 1. you must ask every dependency "Do I need to update you?"
    - Since we have no information about the modifications that have been made, every dependency must be asked

        __build__
    - Even is _update_ was not O(n), the way _make_ store the DAG forces linear time
    - it create fragments of dependency, however we still have O(n) operation since 
        we don't know each dependency contains our target file

        __Corretness?__
    + file persistence
    + if you modify the name of the program you build for instance from "program" -> "foo"
    + you have no way to know if a file "program" still exists and if other files use it
    + no data persistence algorithm 

        4. Beta build system
        1. Input file change list, info on all the file change (delete,create,modif)
        2. Build partial DAG, build partial DAG with step1
        3. Update, build with partial dag

    * Consequences
        1. O(n) with n the number of modifications, for the change file list
        2. Since we build the partial DAG from modified source file, the partial DAG is minimal
        and consistent, we start from the files which we know have been modified compared to Make
        the algorithm is:
        - foreach modif file    (O(1), usually you have only fewx modif (debatable)) 
            - add file
            - find its dependency with on-disk DAG (O(log(n) it the on-disk DAG is well constructed)
            - call recursive                       (O(log(n) since you recreate a minimal DAG )
                = O(log^2(n))
                3. rebuild following partial DAG

    * Data persistence?
        - Needs to add persistent fragments throughout builds other than user-written output files
        - Those will be update automatically and it is easy to notice difference in output files from  a build to another

            5. Example of a build system using these algo
- Tup, scales much better than make
- Immplementation choices are not discussed

#Frama-C
Frama-C is a platform for analysis which main goal is too be highly modular

Frama-C is:
- static analysys platform
    + proposes natively mutiple tool for static analysys
- semantic extractor 
    + value range of a program
    + program slicing 
    + can navigate the dataflow of the program
- formal verifier
    + spec can be written with ACSL (ANSI/ISO C Spec Language)
    + ACSL annotations in C programs can be interpreted by Frama-C
- highly modular platform
    + easy to add new analysys
- collaborative platform
    + it's possible to make analysy works together
    + ACSL notations to exchange info between analyzers
    + ACSL is the common ground between all plug-ins

        __Analysys steps__
        1. Preprocessing
    - gcc -C -E -I.
    - -C, to keep comments during preprocessing
    - -E, to preprocess only
    - -I, include current dir for headers
        2. Merge source files
    - parses, type-checks and links the source code
        3. Normalizing the source code
    - code transformation for easier code analysys

        File merging
- Merge foo.c and bar.c into total.c
    > frama-c -print foo.c bar.c -ocode total.c

#Deep Specifications Certified Abstraction Layers
1. Problem
    - Complex systems are written using numerous layers of abstractions.
    - Each of them defines an API hiding the implementation detail.
    - You're supposed to be able to build on the layer just by understanding its interface.
    - However it is not always the case when the api is not well defined or does not capture all the
        subtle effects of the implementation

        2. Solution
    - __deep specification__, should capture the precise functionality of the underlying implementation
    - formally specify the interface of a layer, and prove it correct in regard of the implementation M
    - use a certified compilation toolchain to ensure that the high level spec matches the binary behavior
    - __certified abstraction layer__, (L1, M, L2)
        + M built on L1 implements the layer L2
        + the primitives of L2 are implemented by M(L1)
    - __implementation independence__
        + any implementation M1 and M2 of the same deep specification L should have contextually 
            equivalent behaviors
    - L1 and L2 can be considered as abstract machine and M the transformation from one to the other
    - refinement ⊆
        + for all program P, M(P)@L1 ⊆ P@L2
        + behavior of M(P) built on L1 refines P built on L2
    - composition, if (L1, M, L2) and (L2, N, L3) then (L1, M∘N, L3)
    - Combine layers into one with multiple primitives


1. Create multiple layers with each a primitive, we will combine them into a single layer afterwards
2. For each of this layer, write a specification about the primitive
3. Write an implementation of those primitive using the primitives of the underlay
4. Prove that the implementation respects the spec,  bisimulation or forward simulation is enough
5. Combine the multiple layers

#The meaning of memory safety
1. Problem
    - Unsafe languages like C have been plagued with memory corruption
        bug/vulnerabilities
    - To avoid such issues guidelines or enforcements are proposed against 
        every problematic behaviour
        + uninitialized pointer
        + dangling pointer
        + use of freed memory
        + array out of bounds
        + ...
    - We know which practice to avoid to have a memory safe program but we
        don't have a clear definition of what is a "memory safe" program

        2. Contributions
        1. Formalize local reasoning for memory safety which show that code do not
        affect unreachable memory
        - Inspire from "memory safe" languages like Java to define what is memory safety
        - On a simple language formalize "frame rules" which define how some commands
        only have a local impact on the memory and the rest of the state is left
        untouched
        - Convert these "frame rules" to rules which can be used with separation 
        logic. Also gives a definition of Non-interference and integrity

        2. Explain how we can relax the model to approach real systems and point
        the trade-off involved (like sacrificing secrecy to keep integrity)
        + in the abstract model pointers are "unreachable" from each other
        + uninitialized memory allow an attacker to have an access to the 
        whole memory if pointers leave in the residual memory
        + quantity of allocated memory is reachable even to a "local" program
        by querying new area of memory and see how which memory region was given
        + dangling pointers may be reused later to access memory regions which 
        should not be reachable
        + state of the memory can be learned by forcing the memory to overflow

        3. Show how this definition can be applied to a real implementation

##################### Secure Refinement ##################### 

